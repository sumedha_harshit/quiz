import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Home from './Pages/Home';
import Login from './Pages/Login';
import SignUp from './Pages/SignUp';
import Verification from './Pages/Verification';
import GameZone from './Pages/GameZone';
import Question from './Pages/Question';
import Winner from './Pages/Winner';
import Looser from './Pages/Looser';
import ForgotPassword from './Pages/ForgotPassword';
import ChangePassword from './Pages/ChangePassword';
import ReferFriend from './Pages/ReferFriend';
import Network from './Pages/Network';
import Wallet from './Pages/Wallet';
import FinishTransfer from './Pages/FinishTransfer';
import Withdraw from './Pages/Withdraw';
import AuthLoadingScreen from './Pages/AuthLoading';
import Passcode from './Pages/Passcode';
import BankDetails from './Pages/BankDetails';
import PendingPayment from './Pages/PendingPayment';
import SuccessPayment from './Pages/SuccessPayment';
import QuizScore from './Pages/QuizScore';
import PrivacyPolicy from './Pages/PrivacyPolicy';
import MySetting from './Pages/MySetting';
import ActiveUsers from './Pages/ActiveUsers';
import Support from './Pages/Support';
import ContactSupport from './Pages/ContactSupport';
import News from './Pages/News';
import NewsFeed from './Pages/NewsFeed';
import NewsFeedDetails from './Pages/NewsFeedDetails';
import Challenge from './Pages/Challenge';
import SearchUser from './Pages/SearchUser';
import UsersChallenge from './Pages/UsersChallenge';
import ChallengeWinner from './Pages/ChallengeWinner';
import WinnerDetails from './Pages/WinnerDetails';
import SavedChallenge from './Pages/SavedChallenge';
import RequestedChallenge from './Pages/RequestedChallenge';
import StartSoonChallenge from './Pages/StartSoonChallenge';

const MainNavigator = createStackNavigator({
  AuthLoadingScreen: { screen: AuthLoadingScreen },
  Login: { screen: Login },
  GameZone: { screen: GameZone },
  Withdraw: { screen: Withdraw },
  Wallet: { screen: Wallet },
  FinishTransfer: { screen: FinishTransfer },
  Network: { screen: Network },
  ReferFriend: { screen: ReferFriend },
  Looser: { screen: Looser },
  Winner: { screen: Winner },
  SignUp: { screen: SignUp },
  Home: { screen: Home },
  Verification: { screen: Verification },
  Question: { screen: Question },
  ChangePassword: { screen: ChangePassword },
  ForgotPassword: { screen: ForgotPassword },
  Passcode : { screen: Passcode },
  BankDetails: { screen: BankDetails },
  PendingPayment: { screen: PendingPayment },
  SuccessPayment: { screen: SuccessPayment },
  QuizScore: { screen: QuizScore },
  PrivacyPolicy: { screen: PrivacyPolicy },
  MySetting: { screen: MySetting },
  ActiveUsers: { screen: ActiveUsers },
  Support: { screen: Support },
  ContactSupport: { screen: ContactSupport },
  News: { screen: News },
  NewsFeed: { screen: NewsFeed },
  Challenge: { screen: Challenge },
  SearchUser: { screen: SearchUser },
  UsersChallenge: { screen: UsersChallenge },
  ChallengeWinner: { screen: ChallengeWinner },
  WinnerDetails: { screen: WinnerDetails },
  SavedChallenge : { screen: SavedChallenge },
  RequestedChallenge : { screen: RequestedChallenge },
  NewsFeedDetails:{screen : NewsFeedDetails},
  StartSoonChallenge:{screen : StartSoonChallenge}
});

const AppNavigator = createAppContainer(MainNavigator);

export default AppNavigator;
