import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, BackHandler } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import { CustomButton, ValidationMessage, Loader } from '@components';
import SplashScreen from 'react-native-splash-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _ from 'lodash';


class ForgotPassword extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      MobileNumber: '',
      MobilePlaceholder:'Mobile number*'
    };
  }
  handleUsernameChange = text => {
    this.setState({ MobileNumber: text });
  };

  goLogin = () => {
    this.props.navigation.navigate(Constants.Screen.LoginScreen);
  };

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  componentWillUnmount()
   { 
     this.backHandler.remove(); 
   }
  
  handleBackButton = () => {
      this.props.navigation.navigate(Constants.Screen.LoginScreen);
      return true;
  };

  handleSendOTP = () => {
    const { MobileNumber, password } = this.state;
    const { setError } = this.props;
    let errorMsg = false;
    if (MobileNumber == '' || _.trim(MobileNumber).length == 0) {
      this.setState({ MobileNumber: '' });
      this.MobileNumber.focus();
      setError(Languages.en.UserNameErrorMsg);
      errorMsg = true;
    }
    else if (_.trim(MobileNumber).length != 10) {
      this.MobileNumber.focus();
      setError(Languages.en.mobileNumberLen);
      errorMsg = true;
    }
    else {
      this.setState({ MobileNumber: _.trim(MobileNumber) });
    }
    if (errorMsg == true) {
      return
    }
    this.props.sendOtpForgotPassword(_.trim(MobileNumber), (d) => this.responsSendOtp(d));
  };

  responsSendOtp(Data){
    if(Data.Result == true)
    alert(Data.Message);
    this.props.navigation.navigate(Constants.Screen.VerificationScreen, {"action": "forgot", "mobilenumber": this.state.MobileNumber});
  }

  render() {
    const { validationMessage, clearError, loading } = this.props;
    const { MobilePlaceholder, MobileNumber } = this.state;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollView}
          showsVerticalScrollIndicator={false}>
          <View style={styles.LoginInner}>
            <View style={styles.LoginIcon}>
              <Image source={Icons.ForgotIcon} style={styles.userImage} />
            </View>
            <View style={styles.LoginTextView}>
              <Text style={styles.LoginText}>{Languages.en.ForgotPassword}</Text>
            </View>
            <View style={styles.LoginContentView}>
              <Text style={styles.LoginContent}>
                {Languages.en.ForgotContent}
              </Text>
            </View>
          </View>
          <View style={styles.LoginInner}>
            {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearError} />) : (<View />)}
            <View style={styles.LoginInputs}>
              <View style={styles.inputView}>
                <TextInput
                  ref={input => {
                    this.MobileNumber = input;
                  }}
                  onSubmitEditing={() => {
                    this.handleSendOTP();
                  }}
                  returnKeyType={'done'}
                  style={CommonStyle.inputStyle}
                  onChangeText={this.handleUsernameChange}
                  autoCorrect={false}
                  blurOnSubmit={false}
                  keyboardType="phone-pad"
                  underlineColorAndroid="transparent"
                  placeholderTextColor={Colors.Black}
                  placeholder={MobileNumber == "" ? MobilePlaceholder :''}
                  selectionColor={Colors.Black}
                  onFocus={()=>{this.setState({MobilePlaceholder:''})}}
                  onBlur={()=>{this.setState({MobilePlaceholder:"Mobile number*"})}}
                />
              </View>
              <View style={styles.LoginButton}>
                <CustomButton
                  buttonStyle={CommonStyle.buttonStyle}
                  titleStyle={CommonStyle.buttonTitleStyle}
                  title={Languages.en.Continue}
                  onPress={() => this.handleSendOTP()}
                />
              </View>
            </View>
              <View style={styles.LoginSignup}>
                <Text style={styles.LoginSignupText}>
                  {Languages.en.AlreadyHaveAccount}{' '}
                </Text>
                <TouchableOpacity onPress={() => this.goLogin()}>
                  <Text style={styles.LoginSignupTextSec}>
                    {Languages.en.Login}
                  </Text>
                </TouchableOpacity>
              </View>
          </View>
        </KeyboardAwareScrollView>
        <Loader
            modalVisible={loading}
        />
      </View>
    );
  }
}

ForgotPassword.propTypes = {
  setError: PropTypes.func,
  clearError: PropTypes.func,
  validationMessage: PropTypes.string,
  sendOtpForgotPassword: PropTypes.func,
  loading: PropTypes.bool,
};
export default ForgotPassword;
