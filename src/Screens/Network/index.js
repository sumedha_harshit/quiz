import React, { Component } from 'react';
import { View, Text, Alert, Linking, Share, TouchableOpacity, FlatList, Image, Dimensions, Clipboard, BackHandler  } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import * as URL from '../../redux/actions/WebApiList';
import CommonStyles from '../../app/common/CommonStyle';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CustomButton, HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import { BannerView } from 'react-native-fbads';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';

class Network extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={Languages.en.Network} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
            <HeaderLeft navigation={navigation} fromhome={true} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      url: process.env.REACT_APP_API_URL+URL.SHARE_REFERRAL_CODE+'?referalCode='+global.ReferralCode,
    }
  }

  componentDidMount(){
    this.props.getAllLevel(global.UserId);
    this.showInterstitial();
  }

  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.log(error));
}

  componentWillMount() {
    //Ads
    AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

    AdMobInterstitial.addEventListener('adLoaded', () =>
        console.log('AdMobInterstitial adLoaded'),
    );
    AdMobInterstitial.addEventListener('adFailedToLoad', error =>
        console.warn(error),
    );
    AdMobInterstitial.addEventListener('adOpened', () =>
        console.log('AdMobInterstitial => adOpened'),
    );
    AdMobInterstitial.addEventListener('adClosed', () => {
        console.log('AdMobInterstitial => adClosed');
        //AdMobInterstitial.requestAd().catch(error => console.warn(error));
    });
    AdMobInterstitial.addEventListener('adLeftApplication', () =>
        console.log('AdMobInterstitial => adLeftApplication'),
    );

    AdMobInterstitial.requestAd().catch(error => console.log(error));
    
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  componentWillUnmount()
   { 
     this.backHandler.remove(); 
   }
  
  handleBackButton = () => {
      this.props.navigation.navigate(Constants.Screen.GameZone);
      return true;
  };

  renderItem = ({ item, index }) => {
    return (
      <View style={[CommonStyle.row, styles.listItemsAll]}>
        <Text style={styles.listItems}>{item.Title}</Text>
        <Text style={[styles.listItems, styles.boldText]}>{item.Activeuser}</Text>
        <Text style={styles.listItems}>{Constants.Symbol.rupee} {item.Amount}</Text>
        {index == 0?
        <TouchableOpacity style={styles.arrowButton} onPress={()=>this.checkActiveUser(item.Activeuser, item.Title, item.Level)}>
          {Svgs.forwardIcon}
        </TouchableOpacity>
        :
        <View style={styles.arrowButton}></View>
        }
      </View>
    )
  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  checkActiveUser = (activeuser, title, levelid) => {
    //  alert(activeuser)
    if(activeuser > 0){
      this.props.navigation.navigate(Constants.Screen.ActiveUsers, { level : title, levelid: levelid});
    }
  }

  addListHeader = () => {
    return (
      <View style={styles.LevelHeaderView}>
        <View style={styles.NetworkIncomeView}>
            <View style={styles.NetworkTextView}>
              <Text style={styles.NetworkText}>Network Income</Text>
            </View>
            <View style={styles.RefreshIconView}>
                <TouchableOpacity onPress={()=>this.refreshLevel()}>
                  {Svgs.RefreshIcon}
                </TouchableOpacity>
            </View>
        </View>
        <View style={styles.listHeader}>
          <Text style={styles.listHeaderItem}>Level</Text>
            <Text style={styles.listHeaderItem}>Active</Text>
          <Text style={styles.listHeaderItem}>Amount</Text>
        </View>
      </View>
    )
  }


  refreshLevel(){
    this.props.getAllLevel(global.UserId);
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyle.emptyViewMain}>
        <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
      </View>
    )
  }

  dynamicEventLink = async () => {
    fetch(`https://tinyurl.com/api-create.php?url=${this.state.url}`, {
      method: 'GET'
    })
    .then((response) => response.text())
    //Response to text
    
    .then((responseJson) => {
      Linking.openURL(`whatsapp://send?text=${responseJson} Your friend has invited you to try QZGuru app. QZGuru is the perfect quiz game mobile application for having fun and learning new things simultaneously. Use referral code ${global.ReferralCode} and get  Bonus amount on registration.`);
    })
    //If response is not in text then in error
    .catch((error) => {
        alert("Error -> " + JSON.stringify(error));
        console.error(error);
    });
  }

  render() {
    const { loading, AllLevelData } = this.props;
    const { url } = this.state;
    return (
      <View style={styles.LoginMain}>
        <View style={styles.SubHeaderView}>
            <Text style={styles.SubHeaderText}>{Languages.en.NetworkPageSubHeading} {AllLevelData.MinimumQuiz} quiz today.</Text>
        </View>
        <View style={styles.questionImageSection}>
          <BannerView
                placementId="892991524504160_917721055364540"
                type="large"
                onPress={() => console.log('click')}
                onLoad={() => console.log('loaded')}
                onError={err => console.log('error', err)}
            />
        </View>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
          <View style={styles.LoginInner}>
            <View style={styles.LoginTextView}>
              <TouchableOpacity onPress={() => {
                  this.dynamicEventLink();
                  // Clipboard.setString(global.ReferralCode);
                  // alert('Referral Code Copied!');
                }}>
                <Text style={styles.LoginText}>{ global.ReferralCode ? global.ReferralCode :''}</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.LoginContentView}>
              <Text style={styles.LoginContent}>
                Share your code with your friends
              </Text>
              <Text style={styles.LoginContent}>
                and get bonus points
              </Text>
            </View>
          </View>
          <View style={[styles.tabSection]}>
            <FlatList
              data={AllLevelData.levelEarnings}
              keyExtractor={this.keyExtractor} 
              renderItem={this.renderItem} 
              ListHeaderComponent={this.addListHeader}
              showsVerticalScrollIndicator={false}
              column={1}
              stickyHeaderIndices={[0]}
              style={styles.NetworkFlatlist}
              ListEmptyComponent={this.emptyView}
            />
          </View>
          <Text style={styles.NetworkUpdateText}>{Languages.en.UpdateText}</Text>
        </KeyboardAwareScrollView>
        <Loader modalVisible={loading} />
        <BannerView
            placementId="892991524504160_892998361170143"
            type="standard"
            onPress={() => console.log('click')}
            onLoad={() => console.log('loaded')}
            onError={err => console.log('error', err)}
        />
      </View>
    );
  }
}

Network.propTypes = {
  getAllLevel: PropTypes.func,
  loading: PropTypes.bool,
  AllLevelData: PropTypes.array
};
export default Network;
