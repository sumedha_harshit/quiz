
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcomeText: {
    fontSize: 20,
    color: "black"
  },
  LoginMain: {
    width: '100%',
    height: '100%',
    flex: 1
  },
  LoginInner: {
    width: '100%',
    //height:'50%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    //border
    paddingTop: 30,
    paddingBottom:20
  },
  userImage:
  {
    height: wp('40%'),
    width: wp('40%'),

  },
  LoginIcon: {
    height: 150,
    width: 150,
    borderColor: Colors.LightPurple,
    borderWidth: 20,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',

  },
  LoginTextView: {
    //paddingTop: 20,
    paddingBottom: 10,

  },
  LoginContentView: {
    width: wp('70%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  LoginText: {
    fontSize: Constants.FontSize.large,
    color: Colors.ThemeBlue,
    borderWidth: 1,
    borderColor: Colors.WhiteGray,
    fontWeight: 'bold',
    padding: 10,
    width: 'auto',
    minWidth: 200,
    textAlign: 'center',
    borderRadius: 10
  },
  LoginContent: {
    fontSize: Constants.FontSize.small,
    color: Colors.BlackOne,
    textAlign: 'center',
    lineHeight: 20
  },
  LoginInputs: {
    width: '100%',
    flexDirection: 'row',
    paddingTop: 5,
    justifyContent: 'center',
    alignItems: 'center'
    // borderWidth: 1, 
    // borderColor: Colors.WhiteGray,
    // shadowColor:Colors.WhiteGray,
    //borderRadius:10
  },
  ImageIcon: {
    margin: 10,
    height: 45,
    width: 45
  },
  inputView: {
  },
  LoginButton: {
    width: '100%',
    flexDirection: 'column',
    marginTop: 15
  },
  LoginBottom: {
    width: '100%',
    flexDirection: 'row',
    paddingTop: 25
  },
  LoginSignup: {
    width: '50%',
    flexDirection: 'row',
    // borderColor:"#000",
    // borderWidth:1
  },
  LoginForgot: {
    width: '50%',
    flexDirection: 'row',
    //  borderColor:"#000",
    //  borderWidth:1,
    alignItems: 'flex-end',
    justifyContent: 'flex-end'

  },
  LoginSignupTextSec: {
    // textAlign:'right',
    color: Colors.ThemeBlue,
    fontSize: Constants.FontSize.tiny,
  },
  LoginForgotText: {
    // textAlign:'right',
    color: Colors.ThemeBlue,
    fontSize: Constants.FontSize.tiny,
  },
  LoginSignupText: {
    color: Colors.BlackOne,
    fontSize: Constants.FontSize.tiny,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20,
    paddingTop: 60,
  },
  scene: {
    flex: 1
  },
  tabSection: {
    //height: 'auto',
    width:'100%',
    //marginTop: 20
  },
  listHeader: {
    // justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#fff',
    padding: 12,
    width: '100%',
    flexDirection:'row',
  },
  listHeaderItem: {
    textAlign: 'center',
    fontWeight: '600',
    fontSize: 16,
    width: '30%',
    letterSpacing: 1,
    fontWeight:'bold'
  },
  listItemsAll: {
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#f0f0f0',
    padding: 12,
    width: '100%',
    borderWidth: 0,
    borderBottomWidth: 1
  },
  listItems: {
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: '600',
    fontSize: 15,
     width: '30%',
  },
  NetworkIncomeView:{
    width:'100%',
    backgroundColor:Colors.ThemeBlue,
    // paddingTop:5,
    // paddingBottom:5,
    paddingRight:15,
    paddingLeft:15,
    flexDirection:'row',
    // flex:1
  },
  LevelHeaderView:{
    width:'100%',
  },
  NetworkText:{
    color: Colors.White,
    fontSize: Constants.FontSize.seventeen,
    textAlign:'left'
  },
  RefreshIcon:{
    borderWidth:1,
    borderColor:"red",
    justifyContent:'center',
    alignItems:'center',
    width:'100%',
    marginTop:20
  },
  NetworkTextView:{
    alignItems:'flex-start',
    justifyContent:'center',
    width:'50%'
  },
  RefreshIconView:{
    alignItems:'flex-end',
    justifyContent:'center',
    width:'50%',
    paddingTop:10
  },
  NetworkFlatlist:{
    flex:1
    // height:'75%',
  },
  boldText:{
    fontWeight:'bold'
  },
  NetworkUpdateText:{
    fontSize: Constants.FontSize.medium,
    color:Colors.ThemeBlue,
    fontWeight:'bold',
    padding: 10
  },
  arrowButton:{
    width:'10%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  SubHeaderView:{
    backgroundColor:Colors.ThemeBlue,
    padding:10,

  },
  SubHeaderText:{
    color:Colors.White,
    fontSize:Constants.FontSize.medium
  },
  questionImageSection: {
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: '#000',
    width: '100%',
    padding: 15,
    alignItems:'center'
},
})