
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height: '100%',
     flex: 1,
  },
  NewsSection:{
    flexDirection:'column',
     width: '95%',
     marginLeft:10,
    // justifyContent:'center',
    //  alignItems:'center',
  },
  ChallengeView:{
    width:'100%',
    backgroundColor:Colors.LightPurple,
    padding:10,
    marginTop:10,
    height:60,
     justifyContent:'center',
    //marginBottom:10,
    // position:'absolute'
  },
  ChallengeText:{
    color:Colors.Black,
    fontSize:Constants.FontSize.big,
    letterSpacing:1
  },
  DeleteView:{
    borderWidth:3,
    borderColor:Colors.White,
    borderRadius:20,
    width:31,
    height:31,
    position:'absolute',
    alignItems:'flex-end',
    alignSelf:'flex-end',
    zIndex:99,
    backgroundColor:Colors.White,
  },
  SavedMainView:{
    //position:'absolute'
    marginTop:5
  }
})