import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl, TouchableOpacity, Dimensions  } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';
import HTML from 'react-native-render-html';
import * as rssParser from 'react-native-rss-parser';
import _ from "lodash";
imageWidth = 130;
import { Spinner } from '../../components/Spinner';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';
// import { WebView } from 'react-native-webview';

class NewsFeed extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={navigation.state.params.item.Title} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      PageTitle:'',
      url:'',
      newsData:[],
      dataLength:0,
      loadingMore:true,
      start:0,
      end:5,
      pageUrl:''
    }
  }

  componentDidMount() {
      let data=this.props.navigation.getParam("item");
      this.setState({PageTitle: data.Title, pageUrl: data.Url});
      //return fetch('https://www.bhaskar.com/rss-feed/1125/')
      // return fetch(data.Url)
      // .then((response) => response.text())
      // .then((responseData) => rssParser.parse(responseData))
      // .then((rss) => {
      
      //   this.setState({newsFeed: rss, dataLength: rss.items.length, newsData: rss.items.slice(this.state.start,this.state.end)});
       
      // });
      this.props.GetNewsFeed(data.Url, 1);
      this.showInterstitial();
  }

  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.log(error));
}

  //Render footer (Loader)
  renderFooter = () => {
    return (
        <View style={styles.footerLoadingView}>
            <Spinner />
        </View>
    );
  }

  componentWillMount() {
    //Ads
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

        AdMobInterstitial.addEventListener('adLoaded', () =>
            console.log('AdMobInterstitial adLoaded'),
        );
        AdMobInterstitial.addEventListener('adFailedToLoad', error =>
            console.warn(error),
        );
        AdMobInterstitial.addEventListener('adOpened', () =>
            console.log('AdMobInterstitial => adOpened'),
        );
        AdMobInterstitial.addEventListener('adClosed', () => {
            console.log('AdMobInterstitial => adClosed');
            //AdMobInterstitial.requestAd().catch(error => console.warn(error));
        });
        AdMobInterstitial.addEventListener('adLeftApplication', () =>
            console.log('AdMobInterstitial => adLeftApplication'),
        );

        AdMobInterstitial.requestAd().catch(error => console.log(error));

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.News);
    return true;
  };

  renderItem = ({ item, index }) => {
   
      return (
        <View style={styles.newsFeedView}>
            <TouchableOpacity style={styles.TitleView} onPress={()=>{this.props.navigation.navigate(Constants.Screen.NewsFeedDetails,{CategoryTitle:this.state.PageTitle, url: this.state.pageUrl, title: item.Title})}}>
              <View style={styles.ImageNewsView}>
                <HTML html={item.Img} imagesMaxWidth={120} />
              </View>
              <View style={styles.TitleNewsView}>
                <Text style={styles.TitleText}>{item.Title}</Text>
              </View>
            </TouchableOpacity>
           
            {
               index % 3 == 0 ?
              <BannerView
                placementId="892991524504160_913088409161138"
                type="rectangle"
                onPress={() => console.log('click')}
                onLoad={() => console.log('loaded')}
                onError={err => console.log('error', err)}
              />
              :
              <View />
            }
        </View>

      )
   
    
  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  refreshLevel() {
    //this.props.getUsersScore(this.state.QuizId, global.UserId);
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
        <View style={CommonStyle.emptyViewMain}>
          <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
        </View>
    )
  }

  // loadMoreData =()=>{
  //   if(this.state.newsData.length <= this.state.dataLength){
	
  //     let arr = this.state.newsData;
    
  //     arr.push(...this.state.newsFeed.items.slice(this.state.start,this.state.end));
      
  //     this.setState({newsData: arr});
  //   }
  // }
  // Called when Pull to refresh called
  onStockRefresh = () => {
    this.props.getPaginationNewsFeed(true);
  }

  render() {
    const { loading, NewsFeed, loadingMore } = this.props;
    // alert(JSON.stringify(NewsFeed));
    // const {newsFeed, index, newsData, dataLength} = this.state;
    
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
         
          <View style={styles.NewsSection}>
           
            <FlatList
              data={NewsFeed}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderItem}
              extraData={this.state}
              showsVerticalScrollIndicator={false}
              column={1}
              style={styles.NewsFeedFlatlist}
              ListEmptyComponent={this.emptyView}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={this.onStockRefresh}
                  automaticallyAdjustContentInsets={false}
                  colors={[Colors.ThemeBlue]}
                  progressBackgroundColor={Colors.White}
                />
              }
              onEndReached={({ distanceFromEnd }) => {
                if (!loadingMore) {
                    this.props.getPaginationNewsFeed(false);
                }
              }}
              onEndReachedThreshold={1}
              ListFooterComponent={loadingMore && this.renderFooter}
            />
           
          </View>
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

NewsFeed.propTypes = {
  GetNewsFeed: PropTypes.func,
  loading: PropTypes.bool,
  NewsFeed: PropTypes.array,
  pageCount: PropTypes.number,
  loadingMore: PropTypes.bool,
  getPaginationNewsFeed: PropTypes.func
};
export default NewsFeed;
