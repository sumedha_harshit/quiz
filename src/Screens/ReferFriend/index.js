import React, { Component } from 'react';
import { Alert, View, Text, TouchableOpacity, Linking, Image, Clipboard, BackHandler } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import * as URL from '../../redux/actions/WebApiList';
import CommonStyles from '../../app/common/CommonStyle';
import { CustomButton, ValidationMessage, Loader, HeaderBackground, HeaderLeft, HeaderRight } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _ from 'lodash';
import { BannerView } from 'react-native-fbads';
import firebase from 'react-native-firebase';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';

class ReferFriend extends Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            headerBackground: (
                <HeaderBackground title={Languages.en.ReferFriendHeader} />
            ),
            headerTintColor: Colors.White,
            headerLeft:
                <View style={CommonStyles.headerRightView}>
                    <HeaderLeft navigation={navigation} fromhome={true} sidemenu={true} />
                </View>
            ,
            headerRight:
                <View style={CommonStyles.headerRightView}>
                    <HeaderRight navigation={navigation} />
                </View>
        }
    };

  constructor(props) {
    super(props);
    this.state = {
      confirmPassword: '',
      password: '',
      url: process.env.REACT_APP_API_URL+URL.SHARE_REFERRAL_CODE+'?referalCode='+global.ReferralCode
    };
  }


   dynamicEventLink = async () => {
      fetch(`https://tinyurl.com/api-create.php?url=${this.state.url}`, {
        method: 'GET'
      })
      .then((response) => response.text())
      //Response to text
      .then((responseJson) => {
        // Share.share({
        //   message: "click on the link " + responseJson,
        // })
        // .then(result => console.log(result))
        // .catch(errorMsg => console.log(errorMsg));
        Linking.openURL(`whatsapp://send?text=${responseJson} Your friend has invited you to try QZGuru app. QZGuru is the perfect quiz game mobile application for having fun and learning new things simultaneously. Use referral code ${global.ReferralCode} and get  Bonus amount on registration.`);
      })
      //If response is not in text then in error
      .catch((error) => {
          //Error 
          alert("Error -> " + JSON.stringify(error));
          console.error(error);
      });
    }

  handleUsernameChange = text => {
    this.setState({ confirmPassword: text });
  };

  handlePasswordChange = text => {
    this.setState({ password: text });
  };

  goLogin = () => {
    this.props.navigation.navigate(Constants.Screen.LoginScreen);
  };
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  componentDidMount() {
    this.showInterstitial();
  }

  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.log(error));
  }

  componentWillUnmount()
   { 
     //Ads
     AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
     AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

     AdMobInterstitial.addEventListener('adLoaded', () =>
         console.log('AdMobInterstitial adLoaded'),
     );
     AdMobInterstitial.addEventListener('adFailedToLoad', error =>
         console.warn(error),
     );
     AdMobInterstitial.addEventListener('adOpened', () =>
         console.log('AdMobInterstitial => adOpened'),
     );
     AdMobInterstitial.addEventListener('adClosed', () => {
         console.log('AdMobInterstitial => adClosed');
         //AdMobInterstitial.requestAd().catch(error => console.warn(error));
     });
     AdMobInterstitial.addEventListener('adLeftApplication', () =>
         console.log('AdMobInterstitial => adLeftApplication'),
     );

     AdMobInterstitial.requestAd().catch(error => console.log(error));

     this.backHandler.remove();
   }
  
  handleBackButton = () => {
      this.props.navigation.navigate(Constants.Screen.GameZone);
      return true;
  };
 
  render() {
    const { validationMessage, clearError, loading } = this.props;
    const { url } = this.state;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
          <View style={styles.LoginInner}>
            <View style={styles.LoginIcon}>
              <Image source={Icons.LoginUserIcon} style={styles.userImage} />
            </View>
            <View style={styles.LoginTextView}>
            <TouchableOpacity onPress={() => {
                this.dynamicEventLink();
                // Clipboard.setString(global.ReferralCode);
                // alert('Referral Code Copied!');
              }}>
                <Text style={styles.LoginText}>{global.ReferralCode ? global.ReferralCode :''}</Text>
            </TouchableOpacity>
            </View>
            <View style={styles.LoginContentView}>
              <Text style={styles.LoginContent}>
                {Languages.en.ReferFriend}
              </Text>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <Loader
            modalVisible={loading}
        />
        <BannerView
            placementId="892991524504160_892998361170143"
            type="standard"
            onPress={() => console.log('click')}
            onLoad={() => console.log('loaded')}
            onError={err => console.log('error', err)}
        />
      </View>
    );
  }
}

ReferFriend.propTypes = {
  setError: PropTypes.func,
  clearError: PropTypes.func,
  validationMessage: PropTypes.string,
  requestLogin: PropTypes.func,
  loading: PropTypes.bool,
};
export default ReferFriend;
