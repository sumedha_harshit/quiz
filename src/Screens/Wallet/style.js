import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({

  LoginMain: {
    width: '100%',
    height: '100%',
    flex: 1,
    padding: 10
  },
  TopView: {
    flexDirection: 'column',
    width: '100%',
    padding: 10,
    borderWidth: 1,
    borderColor: Colors.WhiteGray,
    marginTop: 10
  },
  BalanceText: {
    paddingBottom: 10,
    color: Colors.BlackOne,
    textAlign:'left'
  },
  MoneyText: {
    paddingBottom: 10,
    textAlign:'left',
    color: Colors.ThemeBlue,
    fontWeight: 'bold'
  },
  ContentText: {
    // paddingBottom:10,
    color: Colors.BlackThree
  },
  BottomView: {
    paddingTop: 25,

  },
  TransHeading: {
    color: Colors.BlackOne,
    //paddingTop:10,
    paddingBottom: 20
  },
  walletBalance:{
    width:'100%',
    flexDirection:'row',
   
  },
  walletBalanceInner:{
    width:'33.33%',
  },
  walletFlatlist:{
    height:'52%'
  },
  PointsView:{
    width:'75%'
  },
  BtnView:{
    width:'25%',
    justifyContent:'center'
  },
  redeemBtn:{
    height: 35,
    width: '100%',
    backgroundColor: Colors.ThemeBlue,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.Silver,
    borderWidth: 1,
  },
  redeemBtnDisable:{
    height: 35,
    width: '100%',
    backgroundColor: Colors.WhiteGray,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.Silver,
    borderWidth: 1,
  }
})