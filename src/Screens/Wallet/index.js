import React, { Component } from 'react';
import { Alert, View, Text, TouchableOpacity, FlatList, Image, Dimensions, RefreshControl, TextInput, BackHandler} from 'react-native';
import { Icons, Colors, Languages, Constants, CommonStyle } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import { CustomButton, HeaderBackground, HeaderLeft, HeaderRight, WalletComponent, Loader, ValidationMessage } from '@components';
import Modal from "react-native-modal";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _ from 'lodash';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';
import { BannerView } from 'react-native-fbads';

class Wallet extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={"Wallet"} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyle.headerRightView}>
          <HeaderLeft navigation={navigation} fromhome={true} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyle.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      points:0,
      ModalVisibility:false,
      PointsRedeem:0
    }
  }

  componentWillMount(){
    this.getAllWalletData();

     //Ads
     AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    //  AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
     AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');
 
     AdMobInterstitial.addEventListener('adLoaded', () =>
       console.log('AdMobInterstitial adLoaded'),
     );
     AdMobInterstitial.addEventListener('adFailedToLoad', error =>
       console.warn(error),
     );
     AdMobInterstitial.addEventListener('adOpened', () =>
       console.log('AdMobInterstitial => adOpened'),
     );
     AdMobInterstitial.addEventListener('adClosed', () => {
       console.log('AdMobInterstitial => adClosed');
       //AdMobInterstitial.requestAd().catch(error => console.warn(error));
     });
     AdMobInterstitial.addEventListener('adLeftApplication', () =>
       console.log('AdMobInterstitial => adLeftApplication'),
     );
 
     AdMobInterstitial.requestAd().catch(error => console.log(error));
    this.showInterstitial();
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.log(error));
  }


  //Key Extractor For FlatList
  _keyExtractor = (item, index) => index.toString();

  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyle.emptyViewMain}>
        <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.TransactionDataNotFound}</Text>
      </View>
    )
  }

  //Render Item for FlatList
  _renderWalletComponent = ({ item }) => (
    <WalletComponent item={item} />
  );

  handleRedeem(){
    this.setState({ModalVisibility: true});
  }

  toggleModal(){
    this.setState({ModalVisibility: !this.state.ModalVisibility});
  }

  componentDidMount() {
    this.didBlurSubscription = this.props.navigation.addListener(
        'didFocus',
        payload => {
          this.getAllWalletData();
        }
    );
    this.showInterstitial();
  }
  
  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.log(error));
}

  getAllWalletData = ()=>{
    this.props.getWalletData(global.UserId);
    setTimeout(()=>{
      this.setState({ points: this.props.WalletData.TotalPoins, PointsRedeem: this.props.WalletData.TotalPoins});
    },1000)
    
  }

  handleBackButton = () => {
      this.props.navigation.navigate(Constants.Screen.GameZone);
      return true;
  };

  componentWillUnmount() {
    this.didBlurSubscription.remove();
    this.backHandler.remove(); 

  }
  
  redeemPoints(){
   
    const { PointsRedeem, points } = this.state;
    if(_.trim(PointsRedeem).length == 0){
      Alert.alert("","Please enter points");
    }
    else if(parseFloat(PointsRedeem) > parseFloat(points)){
      this.props.setWalletError("You don't have enough points to redeem !! Please check your wallet.")
      // Alert.alert("","You don't have enough points to redeem !! Please check your wallet.");
    }
    else{
      this.setState({ModalVisibility: false});
      this.props.navigation.navigate(Constants.Screen.FinishTransfer, { type: "redeem", PointsRedeem: PointsRedeem });
      //this.props.redeemPoints(PointsRedeem, (d)=>this.responseRedeemPoints(d));
    }
  }

  // responseRedeemPoints(data){
  //   alert(JSON.stringify(data));
  //   this.props.getWalletData(global.UserId);
  // }

  render() {
    const { WalletData, loading, clearWalletError, validationMessage} = this.props;
    const { points } = this.state;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}
          refreshControl={
            <RefreshControl
                refreshing={false}
                onRefresh={()=>{this.getAllWalletData()}}
                automaticallyAdjustContentInsets={false}
                colors={[Colors.ThemeBlue]}
                progressBackgroundColor={Colors.White}
            />
            }
          >
         <View style={styles.TopView} >
          <View style={styles.walletBalance}>
            <View style={styles.walletBalanceInner}>
              <Text style={[styles.BalanceText, CommonStyle.fs13]}>{Languages.en.Yourbalance}</Text>
              <Text style={[styles.MoneyText, CommonStyle.fs21]}>{Constants.Symbol.rupee} {WalletData.CurrentBalance ? WalletData.CurrentBalance : '00.00'}</Text>
            </View>
            <View style={styles.walletBalanceInner}>
              <Text style={[styles.BalanceText, CommonStyle.fs13]}>{Languages.en.MonthlyEarning}</Text>
              <Text style={[styles.MoneyText, CommonStyle.fs21]}>{Constants.Symbol.rupee} {WalletData.MothlyIncome ? WalletData.MothlyIncome : '00.00'}</Text>
            </View>
            <View style={styles.walletBalanceInner}>
              <Text style={[styles.BalanceText, CommonStyle.fs13]}>{Languages.en.TotalWithdraw}</Text>
              <Text style={[styles.MoneyText, CommonStyle.fs21]}>{Constants.Symbol.rupee} {WalletData.TotalWithdraw ? WalletData.TotalWithdraw : '00.00'}</Text>
            </View>
          </View>
          <View>
            {/* <Text style={[styles.ContentText, CommonStyle.fs11]}>{Languages.en.MinimunMoneyWidhdraw} {Constants.Symbol.rupee} 50</Text> */}
          </View>
        </View>
        <View style={styles.TopView} >
          <View style={styles.walletBalance}>
            <View style={styles.PointsView}>
              <Text style={[styles.BalanceText, CommonStyle.fs13]}>{Languages.en.YourPoints}</Text>
              <Text style={[styles.MoneyText, CommonStyle.fs21]}>{WalletData.TotalPoins ? WalletData.TotalPoins : points}</Text>
            </View>
            <View style={styles.BtnView}>
              <CustomButton
                  buttonStyle={points == 0 ? styles.redeemBtnDisable : styles.redeemBtn }
                  titleStyle={CommonStyle.buttonTitleStyle}
                  title={Languages.en.Redeem}
                  btndisable={points == 0 ? true : false}
                  onPress={() => { this.handleRedeem() }}
              />
            </View>
          </View>
        </View>
        <View style={styles.BottomView}>
          <Text style={[styles.TransHeading, CommonStyle.fs19]}>{Languages.en.Transactionlist}</Text>
          <FlatList
            data={WalletData.TransactionModels}
            keyExtractor={this._keyExtractor}
            extraData={this.props}
            renderItem={this._renderWalletComponent}
            showsVerticalScrollIndicator={false}
            numColumns={1}
            style={styles.walletFlatlist}
            refreshControl={
            <RefreshControl
                refreshing={false}
                onRefresh={()=>this.props.getWalletData(global.UserId)}
                automaticallyAdjustContentInsets={false}
                colors={[Colors.ThemeBlue]}
                progressBackgroundColor={Colors.White}
            />
            }
            ListEmptyComponent={this.emptyView}
          />

        </View>
        <Modal style={CommonStyle.modalContainerTimer} isVisible={this.state.ModalVisibility}>
            <View style={CommonStyle.modalView}>
                <View style={CommonStyle.modalHeaderViewTimer}>
                    <View style={CommonStyle.headingModal}><Text style={CommonStyle.modalHeaderTitleTimer}>Enter Points to Redeem</Text></View>
                    <View style={CommonStyle.closeIconModal}>
                        <TouchableOpacity style={CommonStyle.modalCloseViewTimer} onPress={() => this.toggleModal()}>
                            <Image source={Icons.CloseModalImage} style={CommonStyle.modalCloseIcon} resizeMode={'stretch'} />
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={CommonStyle.modalSubViewTimer}>
                    <View style={CommonStyle.InputView}>
                    {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearWalletError} />) : (<View />)}
                            <TextInput
                                ref={(input) => { this.PointsRedeem = input; }}
                                returnKeyType={'done'}
                                style={CommonStyle.inputStyle}
                                onChangeText={(text) => { this.setState({ PointsRedeem: text }) }}
                                autoCorrect={false}
                                blurOnSubmit={false}
                                underlineColorAndroid="transparent"
                                placeholderTextColor={Colors.WhiteGray}
                                placeholder="Points"
                                keyboardType="number-pad"
                                selectionColor={Colors.WhiteGray}
                                value={this.state.PointsRedeem ? this.state.PointsRedeem.toString() :'' }
                            />
                    </View>
                    <CustomButton
                        buttonStyle={CommonStyle.buttonStyleTimer}
                        titleStyle={CommonStyle.buttonTitleStyle}
                        title="Redeem"
                        onPress={() => { this.redeemPoints() }}
                    />
                </View>
            </View>
        </Modal>
        </KeyboardAwareScrollView>
        <Loader modalVisible={loading}  />
        <BannerView
                    placementId="892991524504160_892998361170143"
                    type="standard"
                    onPress={() => console.log('click')}
                    onLoad={() => console.log('loaded')}
                    onError={err => console.log('error', err)}
                />
      </View>
    );
  }
}

Wallet.propTypes = {
  getWalletData: PropTypes.func,
  WalletData: PropTypes.object,
  loading: PropTypes.bool,
  validationMessage: PropTypes.string,
  clearWalletError: PropTypes.func,
  setWalletError: PropTypes.func
  // redeemPoints: PropTypes.func
};
export default Wallet;
