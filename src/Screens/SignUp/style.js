
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcomeText:{
      fontSize:20,
      color: "black"
  },
  LoginMain:{
    width:'100%',
    height:'100%',
    flex:1,
    padding:20
    // paddingTop:20,
    // paddingBottom:20,
    // paddingRight:20, 
    // paddingLeft:20
  },
  LoginInner:{
    width:'100%',
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'column'
  },
  LoginInnersec:{
    width:'100%',
    // height:'60%',
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'column',
    paddingTop:10
  },
  userImage:
  {
    height:90,
    width:90,
    
  },
  LoginIcon:{
    height:150,
    width:150,
    borderColor:Colors.LightPurple,
    borderWidth:30,
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
    
  },
  LoginTextView:{
    paddingTop:20,
    paddingBottom:10
  },
  LoginContentView:{
    width:'70%',
    alignItems:'center',
    justifyContent:'center',
    marginBottom:10
  },
  LoginText:{
    fontSize:Constants.FontSize.large,
    color:Colors.Black,
  },
  LoginContent:{
    fontSize:Constants.FontSize.tiny,
    color:Colors.BlackOne,
    textAlign:'center',
    lineHeight:20
  },
  LoginInputs:{
    width:'100%',
    flexDirection:'column',
    padding: 20,
    borderWidth: 1, 
    borderColor: Colors.WhiteGray,
    // shadowColor:Colors.WhiteGray,
    borderRadius:10,
    // height: '90%'
  },
  inputView:{
  },
  LoginButton:{
    width:'100%',
    flexDirection:'column',
    marginTop:10
  },
  LoginBottom:{
    width:'100%',
    flexDirection:'row',
    paddingTop:15
  },
  LoginSignup:{
    width:'100%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center'
    // borderColor:"#000",
    // borderWidth:1
  },
  LoginSignupTextSec:{
    // textAlign:'right',
    color:Colors.ThemeBlue,
    fontSize:Constants.FontSize.tiny,
  },
  LoginForgotText:{
    // textAlign:'right',
    color:Colors.ThemeBlue,
    fontSize:Constants.FontSize.tiny,
  },
  LoginSignupText:{
    color:Colors.BlackOne,
    fontSize:Constants.FontSize.tiny,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20,
    paddingTop: 60,
  },
})