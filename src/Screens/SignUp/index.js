import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, BackHandler } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import { CustomButton, ValidationMessage, Loader } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _, { iteratee } from 'lodash';
import DeviceInfo from 'react-native-device-info';
import publicIP from 'react-native-public-ip';

let deviceId = DeviceInfo.getUniqueId();
class SignUp extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            header: null
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            Username: '',
            UserEmail: '',
            MobileNumber: '',
            password: '',
            referralCode: '',
            NamePlaceholder: 'Name*', 
            MobilePlaceholder:'Mobile number*', 
            PasswordPlaceholder:'Password*', 
            CodePlaceholder:'Referral Code'
        }
    }

    componentDidMount() {
        publicIP()
        .then(ip => {
            this.props.getReferralCode(ip, (d)=>this.setRefferalCode(d))
        })
        
        //this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    setRefferalCode =(data)=>{
        // alert(data.Data);
        this.setState({referralCode: data.Data})
    }

    handleNameChange = (text) => {
        this.setState({ Username: text })
    }

    handleUserEmailChange = (text) => {
        this.setState({ UserEmail: text })
    }

    handleMobilenumberChange = (text) => {
        this.setState({ MobileNumber: text })
    }

    handlePasswordChange = (text) => {
        this.setState({ password: text })
    }

    handleReferralCodeChange = (text) => {
        this.setState({ referralCode: text })
    }

    goLogin = () => {
        this.props.navigation.navigate(Constants.Screen.LoginScreen);
    }

    componentWillMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      componentWillUnmount()
       { 
         this.backHandler.remove(); 
       }
      
      handleBackButton = () => {
          this.props.navigation.navigate(Constants.Screen.LoginScreen);
          return true;
      };

    handleRegister = () => {
        const { Username, UserEmail, MobileNumber, password, referralCode } = this.state;
        const { setError } = this.props;
        let errorMsg = false;
        if (_.trim(MobileNumber).length == 0 && _.trim(password).length == 0 && _.trim(Username).length == 0) {
            this.setState({ MobileNumber: '', password: '', Username: '' });
            this.Username.focus();
            setError(Languages.en.AllFieldsMandatory);
            errorMsg = true;
        }
        else if (Username == '' || _.trim(Username).length == 0) {
            this.setState({ Username: '' });
            this.Username.focus();
            setError(Languages.en.NameErrorMsg);
            errorMsg = true;
        }
        // else if (UserEmail == '' || _.trim(UserEmail).length == 0) {
        //     this.setState({ UserEmail: '' });
        //     this.UserEmail.focus();
        //     setError(Languages.en.UserEmailErrorMsg);
        //     errorMsg = true;
        // }
        else if (MobileNumber == '' || _.trim(MobileNumber).length == 0) {
            this.setState({ MobileNumber: '' });
            this.MobileNumber.focus();
            setError(Languages.en.UserNameErrorMsg);
            errorMsg = true;
        }
        else if (_.trim(MobileNumber).length != 10) {
            this.MobileNumber.focus();
            setError(Languages.en.mobileNumberLen);
            errorMsg = true;
        }
       
        else if (password == '' || _.trim(password).length == 0) {
            this.setState({ password: '' });
            this.password.focus();
            setError(Languages.en.PasswordErrorMsg);
            errorMsg = true;
        }
        else {
            this.setState({ MobileNumber: _.trim(MobileNumber), Username: _.trim(Username), referralCode: _.trim(referralCode) });
        }
        if (errorMsg == true) {
            return
        }
        // this.props.requestSignup(_.trim(MobileNumber), password, _.trim(Username), _.trim(UserEmail), _.trim(referralCode), () => this.props.navigation.navigate(Constants.Screen.VerificationScreen, { mobilenumber: this.state.MobileNumber }))
        this.props.requestSignup(_.trim(MobileNumber), password, _.trim(Username), UserEmail, _.trim(referralCode), deviceId, (d) => this.props.navigation.navigate(Constants.Screen.PasscodeScreen, { UserDataId : d.id, mobilenumber : this.state.MobileNumber, type: "signup" }))
    }

   

    render() {
        const { validationMessage, clearError, loading } = this.props;
        const { NamePlaceholder, MobilePlaceholder, PasswordPlaceholder, CodePlaceholder,  Username, MobileNumber, password, referralCode } = this.state;
        return (
            <View style={styles.LoginMain}>
             <KeyboardAwareScrollView
                contentContainerStyle={CommonStyle.flexGroWScrollView}
                showsVerticalScrollIndicator={false}>
                <View style={styles.LoginInner}>
                    <View style={styles.LoginIcon}>
                        <Image source={Icons.SignupUserIcon} style={styles.userImage} />
                    </View>
                    <View style={styles.LoginTextView}>
                        <Text style={styles.LoginText}>{Languages.en.Register}</Text>
                    </View>
                    <View style={styles.LoginContentView}>
                        <Text style={styles.LoginContent}>{Languages.en.LoginContent}</Text>
                    </View>
                    {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearError} />) : (<View />)}

                </View>
                <View style={styles.LoginInnersec}>

                    <View style={styles.LoginInputs}>

                        
                            <View style={styles.inputView}>
                                <TextInput
                                    ref={(input) => { this.Username = input; }}
                                    onSubmitEditing={(event) => this.MobileNumber.focus()}
                                    returnKeyType={'next'}
                                    style={CommonStyle.inputStyle}
                                    onChangeText={this.handleNameChange}
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    placeholder={Username == "" ? NamePlaceholder :''}
                                    placeholderTextColor={Colors.Black}
                                    selectionColor={Colors.Black}
                                    onFocus={()=>{this.setState({NamePlaceholder:''})}}
                                    onBlur={()=>{this.setState({NamePlaceholder:"Name*"})}}
                                />
                            </View>
                            {/* <View style={styles.inputView}>
                                <TextInput
                                    ref={(input) => { this.UserEmail = input; }}
                                    onSubmitEditing={(event) => this.MobileNumber.focus()}
                                    returnKeyType={'next'}
                                    //onBlur={() => this.onBlur(Languages.en.Password)}
                                    //onFocus={() => { this.checkFocus(), this.onFocus(Languages.en.Password) }}
                                    style={CommonStyle.inputStyle}
                                    onChangeText={this.handleUserEmailChange}
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    placeholder="Email*"
                                    placeholderTextColor={Colors.Black}
                                    selectionColor={Colors.Black}                                />
                            </View> */}
                            <View style={styles.inputView}>
                                <TextInput
                                    ref={(input) => { this.MobileNumber = input; }}
                                    onSubmitEditing={() => { this.password.focus() }}
                                    returnKeyType={'next'}
                                    // onBlur={() => this.onBlur(Languages.en.UserName)}
                                    // onFocus={() => this.onFocus(Languages.en.UserName)}
                                    style={CommonStyle.inputStyle}
                                    onChangeText={this.handleMobilenumberChange}
                                    autoCorrect={false}
                                    blurOnSubmit={false}
                                    keyboardType="phone-pad"
                                    underlineColorAndroid="transparent"
                                    placeholder={MobileNumber == "" ? MobilePlaceholder:''}
                                    placeholderTextColor={Colors.Black}
                                    selectionColor={Colors.Black}
                                    onFocus={()=>{this.setState({MobilePlaceholder:''})}}
                                    onBlur={()=>{this.setState({MobilePlaceholder:"Mobile number*"})}}  
                                                                  />

                            </View>
                            <View style={styles.inputView}>
                                <TextInput
                                    ref={(input) => { this.password = input; }}
                                    onSubmitEditing={() => { this.referralCode.focus() }}
                                    returnKeyType={'next'}
                                    //onBlur={() => this.onBlur(Languages.en.Password)}
                                    //onFocus={() => { this.checkFocus(), this.onFocus(Languages.en.Password) }}
                                    style={CommonStyle.inputStyle}
                                    secureTextEntry
                                    onChangeText={this.handlePasswordChange}
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    placeholder={password == "" ? PasswordPlaceholder:''}
                                    placeholderTextColor={Colors.Black}
                                    selectionColor={Colors.Black}
                                    onFocus={()=>{this.setState({PasswordPlaceholder:''})}}
                                    onBlur={()=>{this.setState({PasswordPlaceholder:"Password*"})}} 
                                    
                                                                    />
                            </View>
                            <View style={styles.inputView}>
                                <TextInput
                                    ref={(input) => { this.referralCode = input; }}
                                    onSubmitEditing={(event) => this.handleRegister()}
                                    returnKeyType={'done'}
                                    //onBlur={() => this.onBlur(Languages.en.Password)}
                                    //onFocus={() => { this.checkFocus(), this.onFocus(Languages.en.Password) }}
                                    style={CommonStyle.inputStyle}
                                    onChangeText={this.handleReferralCodeChange}
                                    autoCorrect={false}
                                    underlineColorAndroid="transparent"
                                    placeholder={referralCode =="" ? CodePlaceholder: ''}
                                    placeholderTextColor={Colors.Black}
                                    selectionColor={Colors.Black} 
                                    onFocus={()=>{this.setState({CodePlaceholder:''})}}
                                    onBlur={()=>{this.setState({CodePlaceholder:"Referral Code"})}} 
                                    value={referralCode}
                                    // editable={referralCode ? false: true}
                                    />
                            </View>
                        <View style={styles.LoginButton}>
                            <CustomButton
                                buttonStyle={CommonStyle.buttonStyle}
                                titleStyle={CommonStyle.buttonTitleStyle}
                                title={Languages.en.Continue}
                                onPress={() => { this.handleRegister() }}
                            />
                        </View>
                    </View>

                    <View style={styles.LoginBottom}>
                        <View style={styles.LoginSignup}>
                            <Text style={styles.LoginSignupText}>{Languages.en.AlreadyHaveAccount} </Text><TouchableOpacity onPress={() => { this.goLogin() }}><Text style={styles.LoginSignupTextSec}>{Languages.en.SignInNow}</Text></TouchableOpacity>
                        </View>
                    </View>
                </View>
                
                <Loader
                    modalVisible={loading}
                />
             </KeyboardAwareScrollView>
            </View>
        );
    }
}


SignUp.propTypes = {
    clearError: PropTypes.func,
    setError: PropTypes.func,
    requestSignup: PropTypes.func,
    validationMessage: PropTypes.string,
    loading: PropTypes.bool,
    getReferralCode: PropTypes.func
};

export default SignUp;