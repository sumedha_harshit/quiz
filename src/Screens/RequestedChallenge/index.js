import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl, TouchableOpacity, Image } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';

class RequestedChallenge extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={Languages.en.RequestedChallenge} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
    
    }
  }

  componentDidMount() {
    this.didBlurSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.props.GetRequestedChallengeList();
      }
  );
  }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.Challenge);
    return true;
  };

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity style={styles.ChallengeView} onPress={()=>this.props.navigation.navigate(Constants.Screen.UsersChallenge, {challengeId: item.ChallangeId})}>
          <Text style={styles.ChallengeText}>{item.Name} Invited to join challenge</Text>
          <Text style={styles.ChallengeDateText}>{item.StartDateTime}</Text>
      </TouchableOpacity>
    )
  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  refreshLevel() {
    this.props.GetRequestedChallengeList();
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyle.emptyViewMain}>
        <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
      </View>
    )
  }


  render() {
    const { loading, RequestedChallengeList } = this.props;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
          <View style={[styles.NewsSection]}>
          <FlatList
                  data={RequestedChallengeList}
                  keyExtractor={this.keyExtractor}
                  renderItem={this.renderItem}
                  showsVerticalScrollIndicator={false}
                  column={1}
                  ListEmptyComponent={this.emptyView}
                  refreshControl={
                    <RefreshControl
                      refreshing={false}
                      onRefresh={() => this.refreshLevel()}
                      automaticallyAdjustContentInsets={false}
                      colors={[Colors.ThemeBlue]}
                      progressBackgroundColor={Colors.White}
                    />
                  }
                />
            
          </View>
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
        <BannerView
              placementId="892991524504160_892998361170143"
              type="standard"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
      </View>
    );
  }
}

RequestedChallenge.propTypes = {
  GetRequestedChallengeList: PropTypes.func,
  loading: PropTypes.bool,
  RequestedChallengeList: PropTypes.array
};
export default RequestedChallenge;
