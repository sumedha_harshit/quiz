
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
export default StyleSheet.create({
    mt5: {
        marginTop: 5
    },
    otpCard: {
        marginTop: 20,
        padding: 20,
        height: 170,
        width: "100%",
        alignItems: "center",
        backgroundColor: '#fff',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },

    borderStyleHighLighted: {
        borderColor: Colors.ThemeBlue,
    },

    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 2,
    },
    pageIcon: {
        height: 85,
        width: 85
    },
    pageIconSection: {
        height: 140,
        width: 140,
        borderColor: Colors.LightPurple,
        borderWidth: 30,
        borderRadius: 100,
        alignItems: 'center',
        justifyContent: 'center',
    },
    underlineStyleHighLighted: {
        borderColor: Colors.ThemeBlue,
    },
    resendText: {
        color: Colors.ThemeBlue,
        textAlign: "center",
        fontWeight: "bold",
        marginTop: 20
    }
})