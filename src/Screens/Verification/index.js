import React, { Component } from 'react';
import { Alert, View, Text, TouchableOpacity, TextInput, Image } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import loginStyle from '../Login/style';
import PropTypes from 'prop-types';
import { CustomButton, Loader,ValidationMessage } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import OTPInputView from '@twotalltotems/react-native-otp-input'

class Verification extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      code: "",
      resending: false,
      time: 30,
      mobilenumber: "",
      action: ""
    };
  }

  componentDidMount() {
    const mobilenumber = this.props.navigation.getParam("mobilenumber");
    const action = this.props.navigation.getParam("action");
    this.setState({ mobilenumber: mobilenumber, action: action });
    this.interval = setInterval(
      () => this.setState((prevState) => ({ time: prevState.time - 1 })),
      1000
    );
  }

  componentDidUpdate() {
    if (this.state.time === 0) {
      clearInterval(this.interval);
    }
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  restartTimer() {
    this.interval = setInterval(
      () => this.setState((prevState) => ({ time: prevState.time - 1 })),
      1000
    );
  }

  resendOTP = () => {
    this.setState({ time: 30 });
    this.restartTimer();
  }

  verifyOTP() {
    const { mobilenumber, code, action } = this.state;
    if (code != "") {
      this.props.verifyOTP(mobilenumber, code, action, ()=>this.responsVerifyOTP());
    }
  }


  responsVerifyOTP(){
    const {action, mobilenumber} = this.state;
    if (action == "signup") {
      Alert.alert("","You have successfully registered!");
      this.props.navigation.navigate(Constants.Screen.LoginScreen);
    } else if (action == "forgot") {
      this.props.navigation.navigate(Constants.Screen.ChangePassword, {'mobileNumber': mobilenumber});
    }
  }

  goLogin = () => {
    this.props.navigation.navigate(Constants.Screen.LoginScreen);
  };

  render() {
    const { code, resending, time, mobilenumber } = this.state;
    const { loading, validationMessage, clearError} = this.props;
    return (
      <View style={[loginStyle.LoginMain, styles.mainView]}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollView}
          showsVerticalScrollIndicator={false}>
          <View style={loginStyle.LoginInner}>
            <View style={styles.pageIconSection}>
              <Image
                source={Icons.VerificationIcon}
                style={styles.pageIcon}
              />
            </View>
            <View style={loginStyle.LoginTextView}>
              <Text style={loginStyle.LoginText}>{Languages.en.VerificationHeading}</Text>
            </View>
            <View style={loginStyle.LoginContentView}>
              <Text style={loginStyle.LoginContent}>
                {Languages.en.VerificationContent}
              </Text>
              <Text style={loginStyle.LoginContent}>
                {mobilenumber}
              </Text>
            </View>
          </View>
          {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearError} />) : (<View />)}

          <View style={styles.otpCard}>

            <OTPInputView
              style={{ width: '100%', height: "28%", marginBottom: 30}}
              pinCount={4}
              autoFocusOnLoad={true}
              code={this.state.code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
              onCodeChanged={code => { this.setState({ code }) }}
              codeInputFieldStyle={styles.underlineStyleBase}
              codeInputHighlightStyle={styles.underlineStyleHighLighted}
            // onCodeFilled={(code => {
            //   alert(code)
            // })}
            />
            <View style={loginStyle.LoginButton}>
              <CustomButton
                btndisable={code.length == 4 ? false : true}
                onPress={()=>this.verifyOTP()}
                buttonStyle={code.length == 4 ? CommonStyle.buttonStyle : CommonStyle.disabledButtonStyle}
                titleStyle={CommonStyle.buttonTitleStyle}
                title={Languages.en.Continue}
              />
            </View>
          </View>
          {time == 0 ?
            <TouchableOpacity onPress={() => this.resendOTP()}>
              <Text style={styles.resendText}>
                Resend OTP
            </Text>
            </TouchableOpacity>
            :
            <Text style={styles.resendText}>
              {Languages.en.Resend} 0:{time}
            </Text>
          }
        </KeyboardAwareScrollView>
        <Loader
          modalVisible={loading}
        />
      </View>
    );
  }
}

Verification.propTypes = {
  // type: PropTypes.string,
  verifyOTP: PropTypes.func,
  loading: PropTypes.bool,
  clearError: PropTypes.func,
  validationMessage: PropTypes.string
};

export default Verification;
