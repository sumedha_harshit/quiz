import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler,RefreshControl } from 'react-native';
import { Icons, Colors, Svgs, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import { BannerView } from 'react-native-fbads';

class ActiveUsers extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
    
        return {
          headerBackground: (
            <HeaderBackground title={navigation.state.params.level+ " " +Languages.en.ActiveUsers} />
          ),
          headerTintColor: Colors.White,
          headerLeft:
            <View style={CommonStyles.headerRightView}>
                <HeaderLeft navigation={navigation}  sidemenu={true} />
            </View>
          ,
          headerRight:
            <View style={CommonStyles.headerRightView}>
              <HeaderRight navigation={navigation} />
            </View>
        }
      };
    
      constructor(props) {
        super(props);
        this.state = {
            levelId:""
        }
      }
    
      componentWillMount() {
        const level = this.props.navigation.getParam("levelid");
        //alert(level);
        this.props.getAactiveUser(global.UserId, level);
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      refreshActiveUsers(){
        this.props.getAactiveUser(global.UserId, level);
      }

      componentWillUnmount()
       { 
         this.backHandler.remove(); 
       }
      
      handleBackButton = () => {
          this.props.navigation.navigate(Constants.Screen.Network);
          return true;
      };
    
      renderItem = ({ item }) => {
        return (
            <View style={styles.innerView}>
                <View style={styles.innerLeftView}>
                    {Svgs.SuccessCheck}
                </View>
                <View style={styles.innerRightView}>
                    <Text style={styles.LableText}>{item.Name}</Text>
                    <Text style={styles.LableValue}>{item.PhoneNumber}</Text>
                </View>
            </View>
        )
      }
    
      keyExtractor = (item, index) => {
        return (index.toString());
      }
    
        //Used to show When Data is empty
        emptyView = () => {
            return (
            <View style={CommonStyle.emptyViewMain}>
                <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
            </View>
            )
        }

      render() {
          const { ActiveUserData, loading } =this.props;
        return (
          <View style={styles.container}>
            <FlatList
                data={ActiveUserData}
                keyExtractor={this.keyExtractor} 
                renderItem={this.renderItem} 
                showsVerticalScrollIndicator={false}
                column={1}
                style={styles.NetworkFlatlist}
                ListEmptyComponent={this.emptyView}
                refreshControl={
                    <RefreshControl
                        refreshing={false}
                        onRefresh={()=>this.refreshActiveUsers()}
                        automaticallyAdjustContentInsets={false}
                        colors={[Colors.ThemeBlue]}
                        progressBackgroundColor={Colors.White}
                    />
                    }
            />
            <Loader modalVisible={loading} />
            <BannerView
                placementId="892991524504160_892998361170143"
                type="standard"
                onPress={() => console.log('click')}
                onLoad={() => console.log('loaded')}
                onError={err => console.log('error', err)}
            />
          </View>
        );
      }
    }
    

ActiveUsers.propTypes = {
    loading: PropTypes.bool,
    getAactiveUser: PropTypes.func,
    ActiveUserData: PropTypes.array
};
export default ActiveUsers;