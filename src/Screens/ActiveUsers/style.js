
import { StyleSheet } from 'react-native';
import { Colors } from '@common';
import Constants from '../../app/common/Constants';

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingLeft:20,
    paddingRight:20,
    paddingTop:20,
    paddingBottom:20
  },
  welcomeText:{
      fontSize:20
  },
  innerView:{
    width:'100%',
    flexDirection:'row',
    borderBottomColor:Colors.WhiteGray,
    borderBottomWidth:1,
    paddingTop:15,
    paddingBottom:15
  },
  innerLeftView:{
    width:'15%',

  },
  innerRightView:{
    width:'85%'
  },
  LableText:{
    fontSize:Constants.FontSize.medium,
    //fontWeight:'bold'
  },
  LableValue:{
    fontSize:Constants.FontSize.thirteen,
    //color:Colors.ThemeBlue,
    //fontWeight:'bold'
  },
  NetworkFlatlist:{
    height:'95%',
  },
})