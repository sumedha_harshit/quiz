import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, Platform, BackHandler } from 'react-native';
import { Icons, Colors, CommonStyle, Languages } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, ValidationMessage } from '@components';
import _ from 'lodash';
import Constants from '../../app/common/Constants';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';

class BankDetails extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerBackground: (
                <HeaderBackground title={Languages.en.BankDetails} />
            ),
            headerTintColor: Colors.White,
            headerLeft:
                <View style={CommonStyles.headerRightView}>
                    <HeaderLeft navigation={navigation} />
                </View>
            ,
            headerRight:
                <View style={CommonStyles.headerRightView}>
                    <HeaderRight navigation={navigation} />
                </View>
        }
    };
    constructor(props) {
        super(props);
        this.state = {
            accountNo: '',
            ifscCode: '',
            bankname: '',
            accountHolderName: '',
            accountNoConfirm: '',
        }
    }

    componentDidMount() {
        const withdrawMethod = this.props.navigation.getParam("withdrawMethod");
        const withdrawalAmount = this.props.navigation.getParam("amount");
        const propsData = this.props.navigation.getParam("data");
        this.setState({
            accountHolderName: propsData.NameInAccount,
            accountNo: propsData.AccountNumber,
            ifscCode: propsData.IFSCCode,
            withdrawMethod: withdrawMethod,
            withdrawalAmount: withdrawalAmount,
            bankname: propsData.Bank
        })
    }

    componentWillMount() {
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    handleBackButton = () => {
        this.props.navigation.navigate(Constants.Screen.Withdraw);
        return true;
    };

    handleSubmit() {
        const { accountNo, ifscCode, bankname, accountHolderName, accountNoConfirm } = this.state;
        const { setWithdrawError } = this.props;
        let errorMsg = false;
        if (_.trim(accountNo).length == 0 && _.trim(ifscCode).length == 0 && _.trim(bankname).length == 0 && _.trim(accountHolderName).length == 0 && _.trim(accountNoConfirm).length == 0) {
            this.setState({ accountNo: '', ifscCode: '', bankname: '', accountHolderName: '', accountNoConfirm: '' });
            this.accountHolderName.focus();
            setWithdrawError(Languages.en.AllFieldsMandatory);
            errorMsg = true;
        }
        else if (_.trim(accountHolderName).length == 0) {
            //this.setState({ accountNo: '' });
            this.accountHolderName.focus();
            setWithdrawError(Languages.en.AccountHolderNameErrorMsg);
            errorMsg = true;
        }
        else if (_.trim(accountNo).length == 0) {
            //this.setState({ accountNo: '' });
            this.accountNo.focus();
            setWithdrawError(Languages.en.AccountNoErrorMsg);
            errorMsg = true;
        }
        else if (_.isNumber(accountNo) == true) {
            this.accountNo.focus();
            setWithdrawError(Languages.en.ValidAccountNoErrorMsg);
            errorMsg = true;
        }
        else if (_.trim(accountNo).length < 11 && _.trim(accountNo).length > 16) {
            //this.setState({ accountNo: '' });
            this.accountNo.focus();
            setWithdrawError(Languages.en.AccountNoLengthErrorMsg);
            errorMsg = true;
        }
        // else if(_.trim(accountNoConfirm).length == 0){
        //     this.accountNoConfirm.focus();
        //     setWithdrawError(Languages.en.AccountNoConfrimErrorMsg);
        //     errorMsg = true;
        // }
        // else if(_.trim(accountNo) != _.trim(accountNoConfirm)){
        //     this.accountNoConfirm.focus();
        //     setWithdrawError(Languages.en.ConfrimAccountErrorMsg);
        //     errorMsg = true;
        // }
        else if (_.trim(ifscCode).length == 0) {
            this.setState({ ifscCode: '' });
            this.ifscCode.focus();
            setWithdrawError(Languages.en.IFSCcodeErrorMsg);
            errorMsg = true;
        }
        // else if(ifscCode.matches("/^[A-Za-z]{4}\d{7}$/")){
        //     //this.setState({ ifscCode: '' });
        //     this.ifscCode.focus();
        //     setWithdrawError(Languages.en.IFSCcodeValidErrorMsg);
        //     errorMsg = true;
        // }
        else if (_.trim(bankname).length == 0) {
            this.setState({ bankname: '' });
            this.bankname.focus();
            setWithdrawError(Languages.en.BanknameErrorMsg);
            errorMsg = true;
        }
        else {
            this.setState({ accountNo: _.trim(accountNo), ifscCode: _.trim(ifscCode), bankname: _.trim(bankname), accountHolderName: _.trim(accountHolderName) });
        }
        if (errorMsg == true) {
            return
        }

        let bankTransferData = {
            "accountHolderName": _.trim(accountHolderName),
            "accountNo": _.trim(accountNo),
            "ifscCode": _.trim(ifscCode),
            "withdrawMethod": this.state.withdrawMethod,
            "withdrawalAmount": this.state.withdrawalAmount,
            "bankName": _.trim(bankname),

        }

        this.props.navigation.navigate(Constants.Screen.FinishTransfer, { data: bankTransferData, type: "Banktransfer" });
    }

    render() {
        const { validationMessage, clearWithdrawError } = this.props;
        return (
            <View style={styles.mainView}>
                <KeyboardAwareScrollView
                    showsVerticalScrollIndicator={false}>
                    <View>
                        <Text style={[CommonStyles.whiteText, CommonStyles.fs18]}>{Languages.en.accountdetails}</Text>
                    </View>
                    <View style={styles.otpSection}>
                        {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearWithdrawError} />) : (<View />)}

                        <View style={styles.inputView}>
                            <Text style={[styles.label, CommonStyles.fs14]}>{Languages.en.accountHolderName}*</Text>
                            <TextInput
                                ref={input => {
                                    this.accountHolderName = input;
                                }}
                                onSubmitEditing={() => {
                                    this.accountNo.focus();
                                }}
                                returnKeyType={'next'}
                                style={[CommonStyle.inputStyle, { backgroundColor: Colors.White }]}
                                onChangeText={(text) => this.setState({ accountHolderName: text })}
                                autoCorrect={false}
                                blurOnSubmit={false}
                                underlineColorAndroid="transparent"
                                placeholderTextColor={Colors.Black}
                                placeholder=""
                                value={this.state.accountHolderName ? this.state.accountHolderName : ''}
                                selectionColor={Colors.Black}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <Text style={[styles.label, CommonStyles.fs14]}>{Languages.en.accountNumber}*</Text>
                            <TextInput
                                ref={input => {
                                    this.accountNo = input;
                                }}
                                onSubmitEditing={() => {
                                    this.accountNoConfirm.focus();
                                }}
                                returnKeyType={'next'}
                                style={[CommonStyle.inputStyle, { backgroundColor: Colors.White }]}
                                onChangeText={(text) => this.setState({ accountNo: text })}
                                autoCorrect={false}
                                blurOnSubmit={false}
                                keyboardType="phone-pad"
                                underlineColorAndroid="transparent"
                                placeholderTextColor={Colors.Black}
                                placeholder=""
                                value={this.state.accountNo ? this.state.accountNo : ''}
                                selectionColor={Colors.Black}
                            />
                        </View>
                        {/* <View style={styles.inputView}>
                        <Text style={[styles.label, CommonStyles.fs14]}>{Languages.en.confirmAccountNumber}*</Text>
                        <TextInput
                            ref={input => {
                                this.accountNoConfirm = input;
                            }}
                            onSubmitEditing={() => {
                                this.ifscCode.focus();
                            }}
                            returnKeyType={'next'}
                            style={[CommonStyle.inputStyle,{backgroundColor:Colors.White}]}
                            onChangeText={(text)=>this.setState({accountNoConfirm: text})}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            keyboardType="phone-pad"
                            underlineColorAndroid="transparent"
                            placeholderTextColor={Colors.Black}
                            placeholder=""
                            value={this.state.accountNoConfirm ? this.state.accountNoConfirm : ''}
                            selectionColor={Colors.Black}
                        />
                    </View> */}
                        <View style={styles.inputView}>
                            <Text style={[styles.label, CommonStyles.fs14]}>{Languages.en.IFSCcode}*</Text>
                            <TextInput
                                ref={input => {
                                    this.ifscCode = input;
                                }}
                                onSubmitEditing={() => {
                                    this.password.focus();
                                }}
                                returnKeyType={'next'}
                                style={[CommonStyle.inputStyle, { backgroundColor: Colors.White }]}
                                onChangeText={(text) => this.setState({ ifscCode: text })}
                                autoCorrect={false}
                                blurOnSubmit={false}
                                underlineColorAndroid="transparent"
                                placeholderTextColor={Colors.Black}
                                placeholder=""
                                value={this.state.ifscCode ? this.state.ifscCode : ''}
                                selectionColor={Colors.Black}
                            />
                        </View>
                        <View style={styles.inputView}>
                            <Text style={[styles.label, CommonStyles.fs14]}>Bank Name*</Text>
                            <TextInput
                                ref={input => {
                                    this.bankname = input;
                                }}
                                onSubmitEditing={() => {
                                    this.handleSubmit();
                                }}
                                returnKeyType={'done'}
                                style={[CommonStyle.inputStyle, { backgroundColor: Colors.White }]}
                                onChangeText={(text) => this.setState({ bankname: text })}
                                autoCorrect={false}
                                blurOnSubmit={false}
                                underlineColorAndroid="transparent"
                                placeholderTextColor={Colors.Black}
                                placeholder=""
                                value={this.state.bankname ? this.state.bankname : ''}
                                selectionColor={Colors.Black}
                            />
                        </View>

                    </View>
                </KeyboardAwareScrollView>
                <TouchableOpacity style={styles.confirmButtonSection} onPress={() => this.handleSubmit()}>
                    <Text style={styles.confirmButton}>
                        {Languages.en.Proceed}
                    </Text>
                </TouchableOpacity>
                <BannerView
                    placementId="892991524504160_892998361170143"
                    type="standard"
                    onPress={() => console.log('click')}
                    onLoad={() => console.log('loaded')}
                    onError={err => console.log('error', err)}
                />
            </View>

        )
    }
}
BankDetails.propTypes = {
    setWithdrawError: PropTypes.func,
    clearWithdrawError: PropTypes.func,
    validationMessage: PropTypes.string,
};

export default BankDetails;