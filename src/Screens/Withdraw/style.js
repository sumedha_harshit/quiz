import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({

  LoginMain: {
    width: '100%',
    height: '100%',
    flex: 1,
    padding: 15,
  },
  TopView: {
    flexDirection: 'column',
    width: '100%',
    marginTop: 10
  },
  BalanceText: {
    paddingBottom: 10,
    color: Colors.Black,
  },
  MoneyText: {
    paddingBottom: 10,
    color: Colors.ThemeBlue,
    fontWeight: 'bold'
  },
  ContentText: {
    color: Colors.ThemeBlue
  },
  BottomView: {
    paddingTop: 25,
    flex:1
  },
  LoginButton:{
    width:'100%',
    flexDirection:'column',
    marginTop:10,
    
  },
  TransHeading: {
    color: Colors.Black,
    paddingBottom: 20,
  },


  //component
  ListView:{
    padding:10,
    borderWidth:1,
    borderColor:Colors.WhiteGray,
    flexDirection:'column',
    width:'100%',
    marginBottom:10
  },
  ImageView:{
    width:'auto',
    // alignItems:'center',
    justifyContent:'center',
    paddingRight:15
  },
  ImageIcon:{
    height:25, width:25
  },
  ListContentView:{
    width:'auto',
    alignItems:'flex-start',
    justifyContent:'center',
  },
  EarningText:{
    color:Colors.BlackOne,
    fontWeight:'bold'
  },
  AmountView:{
    width:'100%',
    //alignItems:'center',
    flexDirection:'column',
    alignItems:'flex-end',
    justifyContent:'center'
  },
  AmountText:{
    textAlign:'right',
    color:Colors.ThemeBlue,
    fontWeight:'bold',

  },
  CheckboxView:
  {
      width:'10%',
      justifyContent:'center',
  },
  ListViewInner:{
      width:'100%',
      flexDirection:'row',
      paddingBottom:10,
      paddingTop:10
  },
  ContentText: {
    color:Colors.ThemeBlue
  },
  selectedMethod:{
    borderColor:Colors.ThemeBlue
  },
  infoStyle:{
    height:16,
    
  }

  
})