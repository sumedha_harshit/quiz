import React, { Component } from 'react';
import { View, Text, TextInput, Platform, Image, Dimensions, RefreshControl, TouchableOpacity, BackHandler } from 'react-native';
import { Icons, Colors, Languages, Constants, CommonStyle, fontAwesomeIcon, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { CustomButton, HeaderBackground, HeaderLeft, HeaderRight, Checkbox, ValidationMessage } from '@components';
import _ from "lodash";
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';
import { BannerView } from 'react-native-fbads';

class Withdraw extends Component {

  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={"Withdraw"} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyle.headerRightView}>
          <HeaderLeft navigation={navigation} fromhome={true} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyle.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      amount: 0,
      walletAmount:0,
      withdrawMethod:'Bank'
    }
    
  }

  componentWillMount(){

    //Ads
    AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    // AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
    AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

    AdMobInterstitial.addEventListener('adLoaded', () =>
      console.log('AdMobInterstitial adLoaded'),
    );
    AdMobInterstitial.addEventListener('adFailedToLoad', error =>
      console.warn(error),
    );
    AdMobInterstitial.addEventListener('adOpened', () =>
      console.log('AdMobInterstitial => adOpened'),
    );
    AdMobInterstitial.addEventListener('adClosed', () => {
      console.log('AdMobInterstitial => adClosed');
      //AdMobInterstitial.requestAd().catch(error => console.warn(error));
    });
    AdMobInterstitial.addEventListener('adLeftApplication', () =>
      console.log('AdMobInterstitial => adLeftApplication'),
    );

    AdMobInterstitial.requestAd().catch(error => console.log(error));
    this.showInterstitial();
    
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    this.props.getBankDetails((d)=>this.responseBankDetails(d));
  }

  componentDidMount() {
    this.didBlurSubscription = this.props.navigation.addListener(
        'didFocus',
        payload => {
          this.props.getBankDetails((d)=>this.responseBankDetails(d));
        }
    );
  }

  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.log(error));
  }

  handleBackButton = () => {
      this.props.navigation.navigate(Constants.Screen.GameZone);
      return true;
  };
  componentWillUnmount() {
    this.didBlurSubscription.remove();
    this.backHandler.remove(); 
  }

  responseBankDetails(data){
    this.setState({amount: data.Data.amount, walletAmount:data.Data.amount})
  }

  handleWithdraw(){
    const {amount, withdrawMethod, walletAmount} = this.state;
    const { setWithdrawError } = this.props;
    let errorMsg = false;
    if(_.trim(amount).length == 0 || _.trim(amount) <= 0 ){
      this.setState({ amount: '' });
      this.amount.focus();
      setWithdrawError(Languages.en.AmountErrorMsg);
      errorMsg = true;
    }
    else if(parseFloat(amount) > parseFloat(walletAmount))
    {
      this.setState({ amount: this.state.amount });
      this.amount.focus();
      setWithdrawError(Languages.en.WalletAmountErrorMsg);
      errorMsg = true;
    }
    else{
      this.setState({amount : _.trim(amount)});
    }
    if (errorMsg == true) {
      return
    }
    if(withdrawMethod == "Bank"){
        this.props.navigation.navigate(Constants.Screen.BankDetails, { withdrawMethod : withdrawMethod, amount: amount, data: this.props.BankDetails});
    }
    else{
      this.props.navigation.navigate(Constants.Screen.FinishTransfer, { withdrawalamount: amount, withdrawMethod : withdrawMethod, type: "paytm" });
    }
  }

  selectMethod(value){
    this.setState({withdrawMethod: value})
  }

  withdrawAmountChange(text){
    let myvalue = text.toString();
    let NewText = myvalue.replace("-", "");
     NewText= NewText.replace(",", "");
     NewText = NewText.replace(" ", "");
    this.setState({amount : NewText})
  }

  render() {
    const { validationMessage, clearWithdrawError, BankDetails } = this.props;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
        <View style={styles.TopView} >
        {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearWithdrawError} />) : (<View />)}
            <Text style={[styles.TransHeading, CommonStyle.fs18]}>{Languages.en.Amount}</Text>
                <TextInput
                  ref={input => {
                    this.amount = input;
                  }}
                  returnKeyType={'done'}
                  style={CommonStyle.inputStyle}
                  onChangeText={(text)=> this.withdrawAmountChange(text)}
                  autoCorrect={false}
                  blurOnSubmit={false}
                  keyboardType="numeric"
                  underlineColorAndroid="transparent"
                  placeholderTextColor={Colors.Black}
                  placeholder="Amount"
                  value={this.state.amount ? this.state.amount.toString() : 0}
                  selectionColor={Colors.Black}
                />
        </View>
        <View style={styles.BottomView}>
          <Text style={[styles.TransHeading, CommonStyle.fs19]}>{Languages.en.WithdrawMethod}</Text>
          <View style={[styles.ListView, this.state.withdrawMethod == "Bank" ? styles.selectedMethod : '' ]}>
          {/* <View style={styles.AmountView}>
                <Image source={Icons.InfoIcon} resizeMode="contain" style={styles.infoStyle} />
            </View> */}
            <TouchableOpacity  onPress={()=>this.selectMethod("Bank")}>
                <View style={styles.ListViewInner}>
                <View style={styles.CheckboxView}>
                    <Checkbox isChecked={this.state.withdrawMethod == "Bank" ? true : false}/>
                </View>
                <View style={styles.ImageView}>
                  {/* <Image source={Icons.BankIcon} style={styles.ImageIcon} /> */}
                  {Svgs.BankIcon}
                </View>
                <View style={styles.ListContentView}>
                    <Text style={[styles.EarningText, CommonStyle.fs13]}>{Languages.en.BankTransfer}</Text>
                    <Text style={[styles.ContentText, CommonStyle.fs11]}>{Languages.en.BankTransferEstimated}</Text>
                    
                    {/* <View style={styles.ImageView}>
                </View> */}
                    </View>
                </View>
            </TouchableOpacity>
          </View>

          <View style={[styles.ListView, this.state.withdrawMethod == "Paytm" ? styles.selectedMethod : '' ]}>
            {/* <View style={styles.AmountView}>
                <Image source={Icons.InfoIcon} resizeMode="contain" style={styles.infoStyle} />
            </View> */}
            <TouchableOpacity style={styles.ListViewInner} onPress={()=>this.selectMethod("Paytm")}>
                <View style={styles.CheckboxView}>
                    <Checkbox isChecked={this.state.withdrawMethod == "Paytm" ? true : false} />
                </View>
                <View style={styles.ImageView}>
                  {Svgs.Paytm}
                </View>
            </TouchableOpacity>
          </View>
          <View>
            <Text>{Languages.en.WithdrawCharges} {BankDetails.chargesAmount} INR</Text>
          </View>
        </View>
        <View style={styles.LoginButton}>
            <CustomButton
                buttonStyle={CommonStyle.buttonStyle}
                titleStyle={CommonStyle.buttonTitleStyle}
                title={Languages.en.Continue}
                onPress={() => { this.handleWithdraw() }}
            />
        </View>
        </KeyboardAwareScrollView>
        <BannerView
            placementId="892991524504160_892998361170143"
            type="standard"
            onPress={() => console.log('click')}
            onLoad={() => console.log('loaded')}
            onError={err => console.log('error', err)}
        />
      </View>
    );
  }
}

Withdraw.propTypes = {
  setWithdrawError: PropTypes.func,
  clearWithdrawError: PropTypes.func,
  validationMessage: PropTypes.string,
  getBankDetails: PropTypes.func,
  BankDetails: PropTypes.object
};
export default Withdraw;
