import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, BackHandler,  } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import { CustomButton, ValidationMessage, Loader } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';
import _ from 'lodash';
import firebase from 'react-native-firebase'

class Login extends Component {

  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      MobileNumber: '',
      password: '',
      removeListener: false,
      MobilePlaceholder:"Mobile number*",
      PasswordPlaceholder:"Password*",
      FCMtoken:''
    };
    this.checkPermission();
    this.messageListener();
  }

componentWillMount() {
  
  this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
}

// componentWillUnmount() {
//  this. backHandler = BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
// }

componentWillUnmount()
 { 
   this.backHandler.remove(); 
 }


handleBackButton = () => {
    BackHandler.exitApp();
    return true;
};

  async componentDidMount() { 
    SplashScreen.hide();
  }

  checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
        this.getFcmToken();
    } else {
        this.requestPermission();
    }
  }

  getFcmToken = async () => {
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      console.log("fcmToken:"+fcmToken);
      // this.props.updateFCMtoken(fcmToken);
      this.setState({FCMtoken: fcmToken});
      // this.showAlert('Your Firebase Token is:', fcmToken);
    } else {
      // this.showAlert('Failed', 'No token received');
    }
  }

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      // User has authorised
    } catch (error) {
        // User has rejected permissions
    }
  }

  messageListener = async () => {
    this.notificationListener = firebase.notifications().onNotification((notification) => {
        const { title, body } = notification;
        //this.showAlert(title, body);
        const channelId = new firebase.notifications.Android.Channel("Default1", "Default1", firebase.notifications.Android.Importance.High);
             firebase.notifications().android.createChannel(channelId);
       
             let notification_to_be_displayed = new firebase.notifications.Notification({
               data: notification.data,
               sound: 'default',
               show_in_foreground: true,
               title: notification.title,
               body: notification.body,
               // icon: Launcher,
             });
     
             console.log('notification.data',notification.data)
       
             if (Platform.OS == "android") {
               notification_to_be_displayed
                 .android.setPriority(firebase.notifications.Android.Priority.High)
                 .android.setSmallIcon('ic_launcher')
                 .android.setChannelId("Default1")
                 .android.setBigText(notification.body)
                 .android.setBigText(notification.title)
                 .android.setVibrate(1000);
             }
             
             firebase.notifications().displayNotification(notification_to_be_displayed);
    });
  
    this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
        const { title, body } = notificationOpen.notification;
        // const notification = notificationOpen.notification;
        // const pushType = notification.pushType;
        //  if(pushType =="user_challenge"){
        //     this.props.navigation.push('UsersChallenge');
        //  }
        //  console.log("pushType:"+pushType+",notificationdata:"+JSON.stringify(notification._data))
        // this.showAlert(title, body);
    });
  
    const notificationOpen = await firebase.notifications().getInitialNotification();
    if (notificationOpen) {
        const { title, body } = notificationOpen.notification;
        // this.showAlert(title, body);
    }
  
    this.messageListener = firebase.messaging().onMessage((message) => {
      // console.log(JSON.stringify(message));
    });
  }

  showAlert = (title, message) => {
    Alert.alert(
      title,
      message,
      [
        {text: 'OK', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }

  handleUsernameChange = text => {
    this.setState({ MobileNumber: text });
  };

  handlePasswordChange = text => {
    this.setState({ password: text });
  };

  goSignUp = () => {
    this.props.navigation.navigate(Constants.Screen.SignupScreen);
  };

  handleLogin = () => {
    const { MobileNumber, password } = this.state;
    const { setError } = this.props;
    let errorMsg = false;
    if (_.trim(MobileNumber).length == 0 && _.trim(password).length == 0) {
      this.setState({ MobileNumber: '', password: '' });
      this.MobileNumber.focus();
      setError(Languages.en.UserNamePasswordErrorMsg);
      errorMsg = true;
    }
    else if (MobileNumber == '' || _.trim(MobileNumber).length == 0) {
      this.setState({ MobileNumber: '' });
      this.MobileNumber.focus();
      setError(Languages.en.UserNameErrorMsg);
      errorMsg = true;
    }
    else if (_.trim(MobileNumber).length != 10) {
      this.MobileNumber.focus();
      setError(Languages.en.mobileNumberLen);
      errorMsg = true;
    }
    else if (password == '' || _.trim(password).length == 0) {
      this.setState({ password: '' });
      this.password.focus();
      setError(Languages.en.PasswordErrorMsg);
      errorMsg = true;
    }
    else {
      this.setState({ MobileNumber: _.trim(MobileNumber) });
    }
    if (errorMsg == true) {
      return
    }
    this.props.requestLogin(_.trim(MobileNumber), password, this.state.FCMtoken, (d) => this.responseCallback(d))

  };

  responseCallback=(data)=>{
    // alert(JSON.stringify(data));
    this.props.navigation.navigate(Constants.Screen.GameZone);
  }

  render() {
    const { validationMessage, clearError, loading } = this.props;
    const { MobilePlaceholder, PasswordPlaceholder, MobileNumber, password, isConnected } = this.state;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollView}
          showsVerticalScrollIndicator={false}>

          <View style={styles.LoginInnerTop}>
            <View style={styles.LoginIcon}>
              <Image source={Icons.LoginUserIcon} style={styles.userImage} />
            </View>
            <View style={styles.LoginTextView}>
              <Text style={styles.LoginText}>{Languages.en.Login}</Text>
            </View>
            {/* <View style={styles.LoginContentView}>
              <Text style={styles.LoginContent}>
                {Languages.en.LoginMessage}
              </Text>
            </View> */}
          </View>
          <View style={styles.LoginInner}>
            {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearError} />) : (<View />)}
            <View style={styles.LoginInputs}>
              <View style={styles.inputView}>
                <TextInput
                  ref={input => {
                    this.MobileNumber = input;
                  }}
                  onSubmitEditing={() => {
                    this.password.focus();
                  }}
                  returnKeyType={'next'}
                  // onBlur={() => this.onBlur(Languages.en.UserName)}
                  // onFocus={() => this.onFocus(Languages.en.UserName)}
                  style={CommonStyle.inputStyle}
                  onChangeText={this.handleUsernameChange}
                  autoCorrect={false}
                  blurOnSubmit={false}
                  keyboardType="phone-pad"
                  underlineColorAndroid="transparent"
                  placeholderTextColor={Colors.Black}
                  onFocus={()=>{this.setState({MobilePlaceholder:''})}}
                  onBlur={()=>{this.setState({MobilePlaceholder:"Mobile number*"})}}
                  placeholder={MobileNumber == "" ? MobilePlaceholder:''}
                  selectionColor={Colors.Black}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  ref={input => {
                    this.password = input;
                  }}
                  onSubmitEditing={event => this.handleLogin()}
                  returnKeyType={'done'}
                  //onBlur={() => this.onBlur(Languages.en.Password)}
                  //onFocus={() => { this.checkFocus(), this.onFocus(Languages.en.Password) }}
                  style={CommonStyle.inputStyle}
                  secureTextEntry
                  onChangeText={this.handlePasswordChange}
                  autoCorrect={false}
                  onFocus={()=>{this.setState({PasswordPlaceholder:''})}}
                  onBlur={()=>{this.setState({PasswordPlaceholder:"Password*"})}}
                  underlineColorAndroid="transparent"
                  placeholderTextColor={Colors.Black}
                  placeholder={password == "" ? PasswordPlaceholder:''}
                  selectionColor={Colors.Black}
                />
              </View>

              <View style={styles.LoginButton}>
                <CustomButton
                  buttonStyle={CommonStyle.buttonStyle}
                  titleStyle={CommonStyle.buttonTitleStyle}
                  title={Languages.en.Login}
                  onPress={() => this.handleLogin()}
                />
              </View>
            </View>
            <View style={styles.LoginBottom}>
              <View style={styles.LoginSignup}>
                <Text style={styles.LoginSignupText}>
                  {Languages.en.DontHaveAnaccount}{' '}
                </Text>
                <TouchableOpacity onPress={() => this.goSignUp()}>
                  <Text style={styles.LoginSignupTextSec}>
                    {Languages.en.SignUp}
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={styles.LoginForgot}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.navigate(
                      Constants.Screen.ForgotPassword,
                    )
                  }>
                  <Text style={styles.LoginForgotText}>
                    {Languages.en.ForgotPassword}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
              <View style={styles.supportView}>
                <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate(
                        Constants.Screen.Support,
                      )
                    }>
                    <Text style={styles.LoginForgotText1}>
                      {Languages.en.Support}
                    </Text>
                    <View style={styles.Underline}></View>
                </TouchableOpacity>
              </View>
          </View>
        </KeyboardAwareScrollView>

        <Loader
            modalVisible={loading}
        />
      </View>
    );
  }
}

Login.propTypes = {
  setError: PropTypes.func,
  clearError: PropTypes.func,
  validationMessage: PropTypes.string,
  requestLogin: PropTypes.func,
  loading: PropTypes.bool,

};
export default Login;
