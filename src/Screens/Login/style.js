
import { StyleSheet, Dimensions } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP } from 'react-native-responsive-screen';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
const height = Dimensions.get('window');
export default StyleSheet.create({
  welcomeText:{
      fontSize:20,
      color: "black"
  },
  LoginMain:{
    width:'100%',
    height:wp('100%'),
    flex:1,
    padding:20
    //paddingRight:20, 
    //paddingLeft:20,
  },
  LoginInner:{
    width:'100%',
    // flex:1,
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'column',
    paddingTop:20,
  },
  LoginInnerTop:{
    width:'100%',
    
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'column',
    // marginBottom:20,
    //marginTop:30,

  },
  userImage:
  {
    height:90,
    width:90,
    
  },
  LoginIcon:{
    height:150,
    width:150,
    borderColor:Colors.LightPurple,
    borderWidth:30,
    borderRadius:100,
    alignItems:'center',
    justifyContent:'center',
    
  },
  LoginTextView:{
    paddingTop:20,
    paddingBottom:10
  },
  LoginContentView:{
    width:'70%',
    alignItems:'center',
    justifyContent:'center',
  },
  LoginText:{
    fontSize:Constants.FontSize.large,
    color:Colors.Black,
  },
  LoginContent:{
    fontSize:Constants.FontSize.tiny,
    color:Colors.BlackOne,
    textAlign:'center',
    lineHeight:20
  },
  LoginInputs:{
    width:'100%',
    flexDirection:'column',
    padding: 25,
    borderWidth: 1, 
    borderColor: Colors.WhiteGray,
    // shadowColor:Colors.WhiteGray,
    borderRadius:10
  },
  inputView:{
  },
  LoginButton:{
    width:'100%',
    flexDirection:'column',
    marginTop:15
  },
  LoginBottom:{
    width:'100%',
    flexDirection:'row',
    paddingTop:25
  },
  LoginSignup:{
    width:'50%',
    flexDirection:'row',
    // borderColor:"#000",
    // borderWidth:1
  },
  LoginForgot:{
    width:'50%',
   flexDirection:'row',
  //  borderColor:"#000",
  //  borderWidth:1,
   alignItems:'flex-end',
   justifyContent:'flex-end'

  },
  LoginSignupTextSec:{
    // textAlign:'right',
    color:Colors.ThemeBlue,
    fontSize:Constants.FontSize.tiny,
  },
  LoginForgotText:{
    // textAlign:'right',
    color:Colors.ThemeBlue,
    fontSize:Constants.FontSize.tiny,
  },
  LoginForgotText1:{
    // textAlign:'right',
    // color:Colors.Black,
    fontWeight:'bold',
    fontSize:Constants.FontSize.small,
  },
  LoginSignupText:{
    color:Colors.BlackOne,
    fontSize:Constants.FontSize.tiny,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 20,
    paddingTop: 60,
  },
  supportView:{
    width:'100%',
    justifyContent:'flex-end',
    alignItems:'flex-end',
    // flex:1,
    marginTop:20,
    alignContent:'flex-end',
    // borderWidth:1, borderColor:"#000"
  },
  Underline:{
    width:53,
    height:1,
    backgroundColor:Colors.Black
  }
})