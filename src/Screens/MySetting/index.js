import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler,RefreshControl } from 'react-native';
import { WebView } from 'react-native-webview';
import { Icons, Colors, Svgs, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import { BannerView } from 'react-native-fbads';
import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded,
} from 'react-native-admob';

class MySetting extends Component {

    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;
    
        return {
          headerBackground: (
            <HeaderBackground title={Languages.en.MySetting} />
          ),
          headerTintColor: Colors.White,
          headerLeft:
            <View style={CommonStyles.headerRightView}>
                <HeaderLeft navigation={navigation}  sidemenu={true} fromhome={true} />
            </View>
          ,
          headerRight:
            <View style={CommonStyles.headerRightView}>
              <HeaderRight navigation={navigation} />
            </View>
        }
      };
    
      constructor(props) {
        super(props);
        this.state = {
        }
      }

      componentDidMount() {
        this.showInterstitial();
    }

    showInterstitial() {
        AdMobInterstitial.showAd().catch(error => console.log(error));
    }

      componentWillMount() {
        //Ads
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

        AdMobInterstitial.addEventListener('adLoaded', () =>
            console.log('AdMobInterstitial adLoaded'),
        );
        AdMobInterstitial.addEventListener('adFailedToLoad', error =>
            console.warn(error),
        );
        AdMobInterstitial.addEventListener('adOpened', () =>
            console.log('AdMobInterstitial => adOpened'),
        );
        AdMobInterstitial.addEventListener('adClosed', () => {
            console.log('AdMobInterstitial => adClosed');
            //AdMobInterstitial.requestAd().catch(error => console.warn(error));
        });
        AdMobInterstitial.addEventListener('adLeftApplication', () =>
            console.log('AdMobInterstitial => adLeftApplication'),
        );

        AdMobInterstitial.requestAd().catch(error => console.log(error));
        this.props.getSettings();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
      }
      
      componentWillUnmount()
       { 
         this.backHandler.remove(); 
       }
      
      handleBackButton = () => {
          this.props.navigation.navigate(Constants.Screen.GameZone);
          return true;
      };
    
      render() {
          const { settingsData, loading } =this.props;
        return (
          <View style={styles.container}>
            <KeyboardAwareScrollView
                contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
                showsVerticalScrollIndicator={false}>
            {/* <View style={styles.innerView}>
                <View style={styles.innerLeftView}>
                    {Svgs.UserIcon}
                </View>
                <View style={styles.innerRightView}>
                    <Text style={styles.LableText}>Refer Code</Text>
                    <Text style={styles.LableValue}>{settingsData.ReferalCode}</Text>
                </View>
            </View> */}
            <View style={styles.innerView}>
                <View style={styles.innerLeftView}>
                    {Svgs.UserIcon}
                </View>
                <View style={styles.innerRightView}>
                    <Text style={styles.LableText}>User name</Text>
                    <Text style={styles.LableValue}>{settingsData.Name}</Text>
                </View>
            </View>
            {/* <View style={styles.innerView}>
                <View style={styles.innerLeftView}>
                    {Svgs.EnvelopeIcon}
                </View>
                <View style={styles.innerRightView}>
                    <Text style={styles.LableText}>My Email</Text>
                    <Text style={styles.LableValue}></Text>
                </View>
            </View> */}
            <View style={styles.innerView}>
                <View style={styles.innerLeftView}>
                    {Svgs.MobileIcon}
                </View>
                <View style={styles.innerRightView}>
                    <Text style={styles.LableText}>Paytm Mobile Number</Text>
                    <Text style={styles.LableValue}>{settingsData.PhoneNumber}</Text>
                </View>
            </View>
            <View style={styles.innerView}>
                <View style={styles.innerLeftView}>
                    {Svgs.UserIcon}
                </View>
                <View style={styles.innerRightView}>
                    <Text style={styles.LableText}>Upline name</Text>
                    <Text style={styles.LableValue}>{settingsData.ParentName}</Text>
                </View>
            </View>
            <View style={styles.innerView}>
                <View style={styles.innerLeftView}>
                    {Svgs.MobileIcon}
                </View>
                <View style={styles.innerRightView}>
                    <Text style={styles.LableText}>Upline mobile number</Text>
                    <Text style={styles.LableValue}>{settingsData.ParentPhoneNumber}</Text>
                </View>
            </View>

            <Loader modalVisible={loading} />
            </KeyboardAwareScrollView>
            <BannerView
                placementId="892991524504160_892998361170143"
                type="standard"
                onPress={() => console.log('click')}
                onLoad={() => console.log('loaded')}
                onError={err => console.log('error', err)}
            />
          </View>
        );
      }
    }
    

MySetting.propTypes = {
    loading: PropTypes.bool,
    getSettings: PropTypes.func,
    settingsData: PropTypes.object
};
export default MySetting;