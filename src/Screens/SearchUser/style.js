
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height: '100%',
    flex: 1
  },
  //news
  TitleView:{
    width:'100%',
    height:40,
    flexDirection:'column',
    borderBottomColor: Colors.BlackOne,
    // borderBottomWidth:1,
    justifyContent:'center',
    marginBottom:20
  },
  NewsSection:{
    paddingLeft:20,
    paddingRight:20,
    flexDirection:'column'
  },
  AdsView:{
    alignItems:'center',
    justifyContent:'center'
   },
  // TitleText:{
  //   fontSize:Constants.FontSize.large,
  //   fontWeight:'bold'
  // },
  // ButtonView:{
  //   width:"100%",
  //   padding:20,
  //   justifyContent:'center',
  //   alignItems:'center',
  //   marginTop:20
  // },
  // WinnersView:{
  //   width:'100%',
  //   flexDirection:'row',
  //   marginBottom:20
  // },
  // SerialMainView:{
  //   width:'15%'
  // },
  // SerialNumberView:{
  //   height:40,
  //   width:40,
  //   borderRadius:5,
  //   alignItems:'center',
  //   justifyContent:'center'
  // },
  // SerialNumber:{
  //   color:Colors.White,
  //   fontSize:Constants.FontSize.large
  // },
  // DetailsView:{
  //   width:'60%',
  //   flexDirection:'column'
  // },
  // NameText:{
  //   color:Colors.Black,
  //   fontSize:Constants.FontSize.medium,
  //   fontWeight:'bold'
  // },
  // DataText:{
  //   color:Colors.Grey,
  //   fontSize:Constants.FontSize.small,
  //   //fontWeight:'bold'
  // },
  // WinnerButtonView:{
  //   width:'25%',
  //   alignItems:'flex-end',
  //   justifyContent:'center',
  // },
  // WinnerButtonTouchable:{
  //   width:'100%',
  //   alignItems:'center',
  //   justifyContent:'center',
  //   borderRadius:5,
  //   backgroundColor:Colors.ThemeBlue,
  //   height:30
  // },
  // WinnerText:{
  //   color:Colors.White,
  //   fontSize:Constants.FontSize.small,
  // },
  SearchView:{
    width:'100%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    marginTop:25,
    marginBottom:20,
    
  },
  searchIconView:{
    position:'absolute',
    // alignItems:'flex-end',
    // borderWidth:1,
    // borderColor:'red',
    right:'5%',
    width:40,
    height:52,
    justifyContent:'center',
    top:-2,
    zIndex:99
    // textAlign:'right'
  },
  UserView:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'center',
    marginBottom:20
  },
  UserNameView:{
    width:'50%',
    justifyContent:'center',
  },
  UserName:{
    color:Colors.Black,
    fontSize:Constants.FontSize.medium,
  },
  AddUserButtonView:{
    width:'50%',
    alignItems:'flex-end'
  },
  UserTouchView:{
    width:'70%',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
    backgroundColor:Colors.ThemeBlue,
    height:30
  },
  UserAddedView:{
    width:'70%',
    alignItems:'center',
    justifyContent:'center',
  },
  ButtonView:{
    color:Colors.White,
    fontSize:Constants.FontSize.small,
  },
  ButtonAddedView:{
    color:Colors.ThemeBlue,
    fontSize:Constants.FontSize.small,
  },
  StartButtonView:{
    width:"100%",
    // padding:20,
    justifyContent:'center',
    alignItems:'center',
    marginTop:20
  },
  AddInputStyle: {
    height: 40,
    // marginBottom: 10,
    // marginTop: 10,
    color: Colors.Black,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: Colors.BlackTwo,
    paddingLeft: 15,
    width:'80%',
  },
  PointsView:{
    position:'absolute',
    // alignItems:'flex-end',
    // borderWidth:1,
    // borderColor:'red',
    right:'10%',
    width:40,
    height:40,
    justifyContent:'center',
    // top:-2,
    zIndex:99
  },
  imagePoints:{
    height:30,
    width:30,
    position:'absolute',
    right:10
  },
  imagePointsOne:{
    height:30,
    width:30,
    position:'absolute',
    top:10
  }
})