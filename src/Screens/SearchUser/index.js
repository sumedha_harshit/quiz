import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl, TouchableOpacity, TextInput, Alert, Image } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader, CustomButton } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';
import LinearGradient from 'react-native-linear-gradient';
import _ from "lodash";
class SearchUser extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={Languages.en.SearchUser} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      searchUser:'',
      searchUserPlaceholder:'Search User',
      challengeID:'',
      MinimumPoints:'',
      challengeId:''
    }
  }

  componentDidMount() {
    var challengeId = this.props.navigation.getParam("data");
    this.setState({challengeId: challengeId});
    this.didBlurSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.props.getAddedUserList(challengeId);
      }
  );
  }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.Challenge);
    return true;
  };

  renderItem = ({ item }) => {
    if(global.UserId != item.UserID)
    {
      return (
        <View style={styles.UserView}>
          <View style={styles.UserNameView}>
            <Text style={styles.UserName}>{item.Name}</Text>
          </View>
          <View style={styles.AddUserButtonView}>
            {item.Status == true ?
              <View style={styles.UserAddedView}>
                  <Text style={styles.ButtonAddedView}>Added</Text>
              </View>
            :
            <TouchableOpacity style={styles.UserTouchView} onPress={()=>this.handleAddUser(item)}>
              <Text style={styles.ButtonView}>Add User</Text>
            </TouchableOpacity>
            }
          </View>
        </View>
      )
    }
  }

  renderItem1 = ({ item }) => {
      return (
        <View style={styles.UserView}>
          <View style={styles.UserNameView}>
            <Text style={styles.UserName}>{global.UserId == item.UserID ? item.Name + " (Created by)" : item.Name}</Text>
            {/* <Text style={styles.UserName}>{item.Phone}</Text> */}
          </View>
          <View style={styles.AddUserButtonView}>
              <View style={styles.UserAddedView}>
                  <Text style={styles.ButtonAddedView}>{item.Phone}</Text>
              </View>
          </View>
        </View>
      )
  }


  handleAddUser = (item) =>{
    this.props.requestAddUser(item.UserID, item.Name, item.Phone, this.state.challengeId, (d)=>this.addUserResponse(d));
  }

  addUserResponse(data){
    this.searchUserHandle();
    // Alert.alert("",data.Data.Message);

  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  refreshLevel() {
    this.props.getAddedUserList(global.UserId, this.state.challengeId);
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyle.emptyViewMain}>
        <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
      </View>
    )
  }

  searchUserHandle = ()=>{
    if(this.state.searchUser.length >=2){
      this.props.requestSearchUser(global.UserId, this.state.searchUser, this.state.challengeId, (d)=>this.searchUserResponse(d));
    }
  }

  searchUserResponse =(data)=>{
    this.props.getAddedUserList(this.state.challengeId);
  }

  handleChallengers = () =>{
    const {MinimumPoints} = this.state;
    if(MinimumPoints == 0 || MinimumPoints == null || MinimumPoints==""){
      Alert.alert("","Please enter minimum entry points to play");
    }
    else{
      this.props.requestMinimumPointsToPlay(global.UserId, this.state.challengeId, MinimumPoints, (d)=>this.responseMinimumPointsToPlay(d))
    }
  }

  responseMinimumPointsToPlay =(data)=>{
    this.props.navigation.navigate(Constants.Screen.UsersChallenge, {challengeId: this.state.challengeId});
  }

  render() {
    const { loading, UserList, AddedUserList } = this.props;
    const {searchUser, searchUserPlaceholder, MinimumPoints} = this.state;
    // alert(JSON.stringify(AddedUserList));
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
            <View style={styles.SearchView}>
              <TextInput
                ref={(input) => { this.searchUser = input; }}
                //onSubmitEditing={(event) => this.handleRegister()}
                returnKeyType={'done'}
                //onFocus={() => { this.checkFocus(), this.onFocus(Languages.en.Password) }}
                style={CommonStyle.searchInputStyle}
                onChangeText={(text)=>{
                  this.setState({searchUser :text})
                  this.searchUserHandle();
                }}
                autoCorrect={false}
                underlineColorAndroid="transparent"
                placeholder={searchUser =="" ? searchUserPlaceholder: ''}
                placeholderTextColor={Colors.Black}
                selectionColor={Colors.Black} 
                onFocus={()=>{this.setState({searchUserPlaceholder:''})}}
                onBlur={()=>{this.setState({searchUserPlaceholder:"Search User"})}} 
              />
              <TouchableOpacity style={styles.searchIconView} onPress={()=>this.searchUserHandle()}>{Svgs.SearchIcon}</TouchableOpacity>
            </View>
          <View style={[styles.NewsSection]}>
            {
            UserList.length > 0?
              <FlatList
                data={UserList}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem}
                showsVerticalScrollIndicator={false}
                column={1}
                ListEmptyComponent={this.emptyView}
                // refreshControl={
                //   <RefreshControl
                //     refreshing={false}
                //     onRefresh={(item) => this.refreshLevel(item)}
                //     automaticallyAdjustContentInsets={false}
                //     colors={[Colors.ThemeBlue]}
                //     progressBackgroundColor={Colors.White}
                //   />
                // }
              /> 
              :
              <View />
            }
            <View style={styles.AdsView}>
                    <BannerView
                        placementId="892991524504160_917721055364540"
                        type="large"
                        onPress={() => console.log('click')}
                        onLoad={() => console.log('loaded')}
                        onError={err => console.log('error', err)}
                    />
            </View>
            <View style={{marginTop:20, marginBottom:20, flexDirection:'row', alignItems:'center' }}>
              <Text style={{fontSize:20, fontWeight:'bold' }}>Added user list &nbsp;</Text>
              <Text style={{fontSize:15 }}>(Add minimum {AddedUserList.MinimumChallangerUsers} users)</Text>
            </View>
           {
            AddedUserList && AddedUserList.AddUsers && AddedUserList.AddUsers.length > 0?
              <FlatList
                data={AddedUserList.AddUsers}
                keyExtractor={this.keyExtractor}
                renderItem={this.renderItem1}
                showsVerticalScrollIndicator={false}
                column={1}
                ListEmptyComponent={this.emptyView}
                refreshControl={
                  <RefreshControl
                    refreshing={false}
                    onRefresh={() => this.refreshLevel()}
                    automaticallyAdjustContentInsets={false}
                    colors={[Colors.ThemeBlue]}
                    progressBackgroundColor={Colors.White}
                  />
                }
              /> 
              :
              <View />
            } 
           
          </View>
          { AddedUserList && AddedUserList.AddUsers && AddedUserList.AddUsers.length  >= (AddedUserList.MinimumChallangerUsers +1) ?
          <View>
              <View style={styles.StartButtonView}>
                <TextInput
                    ref={(input) => { this.MinimumPoints = input; }}
                    returnKeyType={'done'}
                    style={styles.AddInputStyle}
                    onChangeText={(text)=>this.setState({MinimumPoints :text})}
                    autoCorrect={false}
                    underlineColorAndroid="transparent"
                    placeholderTextColor={Colors.Black}
                    placeholder="Minimum entry points to play"
                    selectionColor={Colors.Black} 
                  />
                  <View style={styles.PointsView} >{Svgs.Coins}</View>
              </View>
              <TouchableOpacity style={styles.StartButtonView}>
                <CustomButton
                    buttonStyle={CommonStyle.buttonStyleSecond}
                    titleStyle={CommonStyle.buttonTitleStyle}
                    title={Languages.en.SendRequest}
                    onPress={() => { this.handleChallengers() }}
                />
              </TouchableOpacity>
            </View>
            : <View />
          }  
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
        <BannerView
              placementId="892991524504160_892998361170143"
              type="standard"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
      </View>
    );
  }
}

SearchUser.propTypes = {
  requestSearchUser: PropTypes.func,
  requestAddUser: PropTypes.func,
  loading: PropTypes.bool,
  UserList: PropTypes.array,
  AddedUserList: PropTypes.object,
  getAddedUserList: PropTypes.func,
  requestMinimumPointsToPlay: PropTypes.func
};
export default SearchUser;
