import React, { Component } from 'react';
import {
  View,
  Image,
  NetInfo,
  Linking
} from 'react-native';
import { Icons, Constants, Global } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import AsyncStorage from '@react-native-community/async-storage';
import SplashScreen from 'react-native-splash-screen';
import EventEmitter from "react-native-eventemitter";
/*========================================================
    * class Name: AuthLoading Screen
    * class Purpose: display AuthLoading screen and ActivityIndicator
    * class Description: design AuthLoading screen and checked whether user is logged or not.
    *=====================================================*/

class AuthLoadingScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  };

  constructor(props) {
    super(props);
    this.startAuthApp();
  }

  componentWillMount() {
    //alert(global.RefreshToken);
    EventEmitter.on(Constants.EventEmitterName.OpenPage, this.openSideBarPages);
    EventEmitter.on(Constants.EventEmitterName.onLogout, this.onLogout);
    // EventEmitter.on(Constants.EventEmitterName.GoRequestedChallenge, this.GoRequestedChallenge);
  }

  // GoRequestedChallenge=()=>{
  //   this.props.navigation.navigate(Constants.Screen.RequestedChallenge);
  // }

  openSideBarPages = (link) => {
    if (link == "logout") {
      this.props.logout();
    } else {
      this.props.navigation.navigate(link)
    }
  }
  
  // checked whether user is logged or not
  startAuthApp = async () => {
    // await AsyncStorage.setItem(Constants.Preferences.currentUser,'');
    var user = await AsyncStorage.getItem(Constants.Preferences.currentUser);
    //  alert(JSON.stringify(user));
    if (user != null) {
      var jsonUser = JSON.parse(user);
      if(jsonUser.FCMToken =="" || jsonUser.FCMToken == null ){
        setTimeout(()=>{
          SplashScreen.hide();
        }, 2000)
        this.props.navigation.navigate(Constants.Screen.LoginScreen);
        // this.props.logout();
      }
      else{
        setTimeout(()=>{
          SplashScreen.hide();
        }, 2000)
        this.setupUser(jsonUser);
      } 
    }
    else {
      setTimeout(()=>{
        SplashScreen.hide();
      }, 2000)
      this.props.navigation.navigate(Constants.Screen.LoginScreen);
    }

  };

  setupUser = (user) => {
    // this.props.setCurrentUser(user);
    this.props.setCurrentUser(user);
    this.props.navigation.navigate(Constants.Screen.GameZone);
  }
  render() {
    const { container, imgStyle } = styles;
    return (
      <View style={container}>
        {/* <Image source={Icons.splashImage} style={imgStyle} /> */}
      </View>
    );
  }


  onLogout = () => {
    this.props.navigation.navigate(Constants.Screen.LoginScreen);
  }
}

AuthLoadingScreen.propTypes = {
  logout: PropTypes.func,
  setCurrentUser: PropTypes.func,
};

export default AuthLoadingScreen;