import { StyleSheet } from 'react-native'
import { Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

//const size = Utils.deviceSize();

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.White,
    justifyContent: 'center'
  },
  imgStyle: {
    height: '100%',
    width: '100%',
    alignSelf: 'center'
  }
})