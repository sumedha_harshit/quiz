
import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    // flex: 1,
    // color:"black",
    // height:200,
    // width:'100%',
    // flexDirection:'row',
    // backgroundColor:"#000"
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcomeText:{
      fontSize:20,
      color: "black"
  }
})