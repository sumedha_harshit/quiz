import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import { Icons, Colors, Languages } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight } from '@components';
// import { TouchableOpacity } from 'react-native-gesture-handler';

class Home extends Component {
    static navigationOptions = ({ navigation }) => {
        const { params = {} } = navigation.state;

        return {
            headerBackground: (
                <HeaderBackground title={Languages.en.Home} />
            ),
            headerTintColor: Colors.White,
            headerLeft:
                <View style={CommonStyles.headerRightView}>
                    <HeaderLeft navigation={navigation} />
                </View>
            ,
            headerRight:
                <View style={CommonStyles.headerRightView}>
                    <HeaderRight navigation={navigation} />
                </View>
        }
    };
    constructor(props) {
        super(props);
    }

    componentDidMount() {
    }

    goProfile = () => {
        this.props.navigation.navigate('Profile');
    }

    render() {
        return (
            <View style={[styles.container, { backgroundColor: Colors.Silver }]} >
                <Image source={Icons.LoginUserIcon} style={{ height: 50, width: 50 }} />
                <Text style={styles.welcomeText}>Welcome !!!</Text>
                <TouchableOpacity onPress={() => this.goProfile()}><Text style={{ fontSize: 15 }}>Go to profile page</Text></TouchableOpacity>
            </View>
        );
    }
}


Home.propTypes = {
    // requestDashboardData: PropTypes.func,
    // loading: PropTypes.bool,
    // dashboardData: PropTypes.object,
    // dashboardBannerData: PropTypes.array,
    // dashsetError: PropTypes.func,
    // requestGetOrderIdDashboard: PropTypes.func,
    // requestSaveOrderRazorPayDashboard: PropTypes.func
    // dashboardProductTypeDetail: PropTypes.object,
};
// export default Home;
export default Home;