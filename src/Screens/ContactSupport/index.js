import React, { Component } from 'react';
import { View, Text, TextInput, BackHandler, Alert } from 'react-native';
import { WebView } from 'react-native-webview';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader, CustomButton, ValidationMessage } from '@components';
import _ from 'lodash';
import { FlatList } from 'react-native-gesture-handler';

class ContactSupport extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={Languages.en.ContactSupport} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
            <HeaderLeft navigation={navigation}  sidemenu={true} fromhome={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      MessagePlaceholder:'Write us a Message',
      Message:'',
      Conversations:[]
    }
  }

  componentDidMount(){

    this.getUserConversation();
  }

  getUserConversation=()=>{
    const {currentUser}=this.props;
    this.props.requestGetUserConversation(currentUser.MobileNumber,()=>this.setUserConversation());
  }

  setUserConversation=()=>{
    const {conversation}=this.props;
  }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  componentWillUnmount()
   { 
     this.backHandler.remove();
   }
  
  handleBackButton = () => {
      this.props.navigation.navigate(Constants.Screen.GameZone);
      return true;
  };

  handleSubmit=()=>{
    const { Message } = this.state;
    const { setContactError, currentUser } = this.props;
    let errorMsg = false;
    if (_.trim(Message).length == 0 && _.trim(Message).length == 0) {
      this.setState({ Message: '' });
      this.Message.focus();
      setContactError(Languages.en.ContactMessageErrorMsg);
      errorMsg = true;
    }
    if (errorMsg == true) {
      return
    }
    this.props.ContactSupportRequest(global.UserId, currentUser.MobileNumber, _.trim(Message), (d) => this.supportResponse(d));
  }


  supportResponse=(data)=>{
    if(data.Result == true){
      Alert.alert("", "Your message has been sent. We will soon get in touch with you!");
      this.setState({Message: ''});
    }
  }

  render() {
    const { MessagePlaceholder,Message } = this.state;
    const {clearContactError,validationMessage,loading , currentUser,conversation} = this.props;

    return (
      <View style={styles.LoginMain}>
        <View style={styles.errorMessage}>
          {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearContactError} />) : (<View />)}
        </View>
        <View style={styles.inputView}>
                <TextInput
                  ref={input => {
                    this.Message = input;
                  }}
                  onSubmitEditing={event => this.handleSubmit()}
                  returnKeyType={'done'}
                  style={CommonStyle.inputTextareaStyle}
                  onChangeText={(text)=>{this.setState({Message: text})}}
                  autoCorrect={false}
                  onFocus={()=>{this.setState({MessagePlaceholder:''})}}
                  onBlur={()=>{this.setState({MessagePlaceholder:"Write us a Message"})}}
                  underlineColorAndroid="transparent"
                  placeholderTextColor={Colors.Black}
                  placeholder={Message == "" ? MessagePlaceholder:''}
                  selectionColor={Colors.Black}
                  numberOfLines={11}
                  multiline={true}
                />
              </View>

              <View style={styles.LoginButton}>
                <CustomButton
                  buttonStyle={CommonStyle.buttonStyle}
                  titleStyle={CommonStyle.buttonTitleStyle}
                  title={Languages.en.Send}
                  onPress={() => this.handleSubmit()}
                />
              </View>
                  
              <View>
              <FlatList
                  data={conversation}
                  keyExtractor={item=>item.ContactSupportID}
                  ListHeaderComponent={()=>(
                    <View style={styles.ConverHead}>
                      <Text style={styles.HeaderText}>Conversation</Text>
                    </View>
                  )}
                  renderItem={({item,key})=>(
                    <View style={styles.ConverMain}>
                      <View>
                        <Text style={styles.QueAnsText}>Question : {item.UserMessage}</Text>
                      </View>
                      <View>
                        <Text style={styles.QueAnsText}>Answer : {item.Reply}</Text>
                      </View>
                    </View>
                  )}
                  ListEmptyComponent={()=>(
                    <View>
                      <Text>User have not any conversation</Text>
                    </View>
                  )}>
                  </FlatList>
              </View>
        <Loader modalVisible={loading} />
      </View>
    );
  }
}

ContactSupport.propTypes = {
  ContactSupportRequest: PropTypes.func,
  loading:PropTypes.bool,
  setContactError: PropTypes.func,
  clearContactError: PropTypes.func,
  currentUser: PropTypes.obj,
  validationMessage: PropTypes.string,
  requestGetUserConversation:PropTypes.func,
  conversation:PropTypes.array,

};
export default ContactSupport;
