
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  welcomeText: {
    fontSize: 20,
    color: "black"
  },
  LoginMain: {
    width: '100%',
    height: '100%',
    flex: 1
  },
  LoginButton:{
    width:'100%',
    flexDirection:'column',
    paddingTop:0,
    padding:20
  },
  errorMessage:{
    width:'100%',
    flexDirection:'column',
    paddingBottom:0,
    marginBottom:0,
    padding:20
  },
  inputView:{
    borderColor: Colors.BlackTwo,
    borderWidth: 1,
    padding: 5,
    margin:20,
    marginTop:0,
    paddingTop:0
  },
  ConverMain: {
    width: '100%',
    height: '100%',
    flex: 1,
    padding:10
  },
  ConverHead: {
    width: '100%',
    height: '100%',
    flex: 1,
    padding:10,
  },
  HeaderText:{
    fontWeight:'bold',
    fontSize:20
  },
  QueAnsText:{
    fontWeight:'bold',
    fontSize:16
  },
  QuestionBack:{
    backgroundColor:'#7ad8fa',
    opacity:0.7,
  },
  AnswerBack:{
    backgroundColor:'#70ff7e',
    opacity:0.7,
  },
})