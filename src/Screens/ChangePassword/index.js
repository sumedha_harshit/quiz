import React, { Component } from 'react';
import { View, Text, TouchableOpacity, TextInput, Image, Alert } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import { CustomButton, ValidationMessage, Loader } from '@components';
import SplashScreen from 'react-native-splash-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _ from 'lodash';


class ChangePassword extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      header: null,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      confirmPassword: '',
      password: '',
      PasswordPlaceholder:'Password*',
      ConfirmPlaceholder:'Confirm Password*'
    };
  }

  handleUsernameChange = text => {
    this.setState({ confirmPassword: text });
  };

  handlePasswordChange = text => {
    this.setState({ password: text });
  };

  handleLogin = () => {
    const phone = this.props.navigation.getParam("mobileNumber");
    const { confirmPassword, password } = this.state;
    const { setError } = this.props;
    let errorMsg = false;
    if (_.trim(password).length == 0 && _.trim(confirmPassword).length == 0 ) {
      this.setState({ confirmPassword: '', password: '' });
      this.password.focus();
      setError(Languages.en.AllFieldsMandatory);
      errorMsg = true;
  }
    if (password == '' || _.trim(password).length == 0) {
      this.setState({ password: '' });
      this.password.focus();
      setError(Languages.en.PasswordErrorMsg);
      errorMsg = true;
    }
    else if (confirmPassword == '' || _.trim(confirmPassword).length == 0) {
      this.setState({ confirmPassword: '' });
      this.confirmPassword.focus();
      setError(Languages.en.ConfirmPasswordErrorMsg);
      errorMsg = true;
    }
    else if (_.trim(password) != _.trim(confirmPassword)) {
      this.setState({ confirmPassword: '' });
      this.confirmPassword.focus();
      setError(Languages.en.PasswordMismatchErrorMsg);
      errorMsg = true;
    }
    else {
      this.setState({ password: _.trim(password) });
    }
    if (errorMsg == true) {
      return
    }
     this.props.changePassword(phone, _.trim(password), (d) => {this.responsePasswordChange(d)});
  };

  responsePasswordChange = (Data) =>{
    Alert.alert("","Password changed successfully!");
    this.props.navigation.navigate(Constants.Screen.LoginScreen);
  }

  render() {
    const { validationMessage, clearError, loading } = this.props;
    const { ConfirmPlaceholder, PasswordPlaceholder, confirmPassword, password } = this.state;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollView}
          showsVerticalScrollIndicator={false}>
          <View style={styles.LoginInner}>
            <View style={styles.LoginIcon}>
              <Image source={Icons.ChangesPasswordIcon} style={styles.userImage} />
            </View>
            <View style={styles.LoginTextView}>
              <Text style={styles.LoginText}>{Languages.en.ChangePassword}</Text>
            </View>
            <View style={styles.LoginContentView}>
              <Text style={styles.LoginContent}>
                {Languages.en.ResetContent}
              </Text>
            </View>
          </View>
          <View style={styles.LoginInner}>
            {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearError} />) : (<View />)}
            <View style={styles.LoginInputs}>

              <View style={styles.inputView}>
                <TextInput
                  ref={input => {
                    this.password = input;
                  }}
                  returnKeyType={'next'}
                  onSubmitEditing={() => {
                    this.confirmPassword.focus();
                  }}
                  style={CommonStyle.inputStyle}
                  secureTextEntry
                  onChangeText={this.handlePasswordChange}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  placeholderTextColor={Colors.Black}
                  placeholder={password == "" ? PasswordPlaceholder :''}
                  selectionColor={Colors.Black}
                  onFocus={()=>{this.setState({PasswordPlaceholder:''})}}
                  onBlur={()=>{this.setState({PasswordPlaceholder:"Password*"})}}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  ref={input => {
                    this.confirmPassword = input;
                  }}
                  onSubmitEditing={event => this.handleLogin()}
                  returnKeyType={'done'}
                  style={CommonStyle.inputStyle}
                  onChangeText={this.handleUsernameChange}
                  autoCorrect={false}
                  blurOnSubmit={false}
                  secureTextEntry
                  underlineColorAndroid="transparent"
                  placeholderTextColor={Colors.Black}
                  placeholder={confirmPassword == "" ? ConfirmPlaceholder : ''}
                  selectionColor={Colors.Black}
                  onFocus={()=>{this.setState({ConfirmPlaceholder:''})}}
                  onBlur={()=>{this.setState({ConfirmPlaceholder:"Confirm Password*"})}}
                />
              </View>
              <View style={styles.LoginButton}>
                <CustomButton
                  buttonStyle={CommonStyle.buttonStyle}
                  titleStyle={CommonStyle.buttonTitleStyle}
                  title={Languages.en.ChangePassword}
                  onPress={() => this.handleLogin()}
                />
              </View>
            </View>
          </View>
        </KeyboardAwareScrollView>
        <Loader
          modalVisible={loading}
        />
      </View>
    );
  }
}

ChangePassword.propTypes = {
  setError: PropTypes.func,
  clearError: PropTypes.func,
  validationMessage: PropTypes.string,
  changePassword: PropTypes.func,
  loading: PropTypes.bool,
};
export default ChangePassword;
