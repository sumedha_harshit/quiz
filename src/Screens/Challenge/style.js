
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height: '100%',
    flex: 1
  },
  //news
  TitleView:{
    width:'100%',
    height:40,
    flexDirection:'column',
    borderBottomColor: Colors.BlackOne,
    // borderBottomWidth:1,
    justifyContent:'center',
    marginBottom:20
  },
  NewsSection:{
    paddingLeft:20,
    paddingRight:20,
    flexDirection:'column'
  },
  TitleText:{
    fontSize:Constants.FontSize.large,
    fontWeight:'bold'
  },
  ButtonView:{
    width:"100%",
    paddingTop:5,
    paddingBottom:20,
    justifyContent:'center',
    alignItems:'center',
    //marginTop:20
  },
  ButtonSavedView:{
    width:"100%",
    paddingTop:20,
    paddingBottom:5,
    justifyContent:'center',
    alignItems:'center',
    //marginTop:20
  },
  ButtonRequestedView:{
    width:"100%",
    paddingTop:5,
    paddingBottom:5,
    justifyContent:'center',
    alignItems:'center',
    //marginTop:20
  },
  WinnersView:{
    width:'100%',
    flexDirection:'row',
    marginBottom:20
  },
  SerialMainView:{
    width:'15%'
  },
  SerialNumberView:{
    height:40,
    width:40,
    borderRadius:5,
    alignItems:'center',
    justifyContent:'center'
  },
  SerialNumber:{
    color:Colors.White,
    fontSize:Constants.FontSize.large
  },
  DetailsView:{
    width:'60%',
    flexDirection:'column'
  },
  NameText:{
    color:Colors.ThemeBlue,
    fontSize:Constants.FontSize.medium,
    fontWeight:'bold'
  },
  DataText:{
    color:Colors.Grey,
    fontSize:Constants.FontSize.small,
    //fontWeight:'bold'
  },
  WinnerButtonView:{
    width:'25%',
    alignItems:'flex-end',
    justifyContent:'center',
  },
  WinnerButtonTouchable:{
    width:'100%',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
    backgroundColor:Colors.ThemeBlue,
    height:30
  },
  WinnerText:{
    color: Colors.Black,
    fontSize: Constants.FontSize.big,
    paddingRight:2,
    fontWeight:'bold'
  },
  DetailsInnerView:{
    width:'100%',
    flexDirection:'row',
    alignItems:'center',
    // justifyContent:'center'
  }
})