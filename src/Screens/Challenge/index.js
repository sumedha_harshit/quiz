import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl, TouchableOpacity } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader, CustomButton } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';
import LinearGradient from 'react-native-linear-gradient';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';

class Challenge extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={Languages.en.StartChallenge} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
    
    }
  }

  componentDidMount() {
    this.didBlurSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.props.winnerUserList();
      }
  );
  this.showInterstitial();
  }

  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.log(error));
}

  componentWillMount() {
    //Ads
    AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

    AdMobInterstitial.addEventListener('adLoaded', () =>
        console.log('AdMobInterstitial adLoaded'),
    );
    AdMobInterstitial.addEventListener('adFailedToLoad', error =>
        console.warn(error),
    );
    AdMobInterstitial.addEventListener('adOpened', () =>
        console.log('AdMobInterstitial => adOpened'),
    );
    AdMobInterstitial.addEventListener('adClosed', () => {
        console.log('AdMobInterstitial => adClosed');
        //AdMobInterstitial.requestAd().catch(error => console.warn(error));
    });
    AdMobInterstitial.addEventListener('adLeftApplication', () =>
        console.log('AdMobInterstitial => adLeftApplication'),
    );

    AdMobInterstitial.requestAd().catch(error => console.log(error));

    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  startChallenge = () =>{
    this.props.requestStartChallenge(global.UserId, this.props.currentUser.UserName, this.props.currentUser.MobileNumber, (d)=>{this.challengeResponse(d)});
  }

  SavedChallenge = () =>{
    this.props.navigation.navigate(Constants.Screen.SavedChallenge);
  }

  RequestedChallenge = () =>{
    this.props.navigation.navigate(Constants.Screen.RequestedChallenge);
  }
  
  challengeResponse = (data) =>{
    //alert(JSON.stringify(data.Data.ChallangeId));
    this.props.navigation.navigate(Constants.Screen.SearchUser,{data: data.Data.ChallangeId});
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.GameZone);
    return true;
  };

  renderItem = ({ item, index }) => {
    return (
      <TouchableOpacity style={styles.WinnersView} onPress={()=>this.props.navigation.navigate(Constants.Screen.WinnerDetails,{ userId: item.UserId, challengeId: item.ChallangeId})}>
        <View style={styles.SerialMainView}>
          <LinearGradient colors={[Colors.ThemePurple, Colors.ThemeBlue]} style={styles.SerialNumberView}>
            <Text style={styles.SerialNumber}>{index+1}</Text>
          </LinearGradient>
        </View>
        <View style={styles.DetailsView}>
          <View style={styles.DetailsInnerView}>
              <Text style={styles.WinnerText}>Winner : </Text>
              <Text style={styles.NameText}>{item.Name}</Text>
          </View>
          <Text style={styles.DataText}>{item.WinDate}</Text>
        </View>
        <View style={styles.WinnerButtonView}>
          {/* <TouchableOpacity style={styles.WinnerButtonTouchable} onPress={()=>this.props.navigation.navigate(Constants.Screen.WinnerDetails,{ userId: item.UserId, challengeId: item.ChallangeId})}>
              <Text style={styles.WinnerText}>></Text>
          </TouchableOpacity> */}
          {Svgs.forwardIcon}
        </View>
      </TouchableOpacity>
    )
  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  refreshLevel() {
    this.props.winnerUserList();
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyle.emptyViewMain}>
        <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
      </View>
    )
  }

  render() {
    const { loading, WinnerList, currentUser } = this.props;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
            <View>
              <TouchableOpacity style={styles.ButtonSavedView}>
                <CustomButton
                    buttonStyle={CommonStyle.buttonStyleSecond}
                    titleStyle={CommonStyle.buttonTitleStyle}
                    title={Languages.en.SavedChallenge}
                    onPress={() => { this.SavedChallenge() }}
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.ButtonRequestedView}>
                <CustomButton
                    buttonStyle={CommonStyle.buttonStyleSecond}
                    titleStyle={CommonStyle.buttonTitleStyle}
                    title={Languages.en.RequestedChallenge}
                    onPress={() => { this.RequestedChallenge() }}
                />
              </TouchableOpacity>
              <TouchableOpacity style={styles.ButtonView}>
                <CustomButton
                    buttonStyle={CommonStyle.buttonStyleSecond}
                    titleStyle={CommonStyle.buttonTitleStyle}
                    title={Languages.en.CreateChallenge}
                    onPress={() => { this.startChallenge() }}
                />
              </TouchableOpacity>
            </View>
            <BannerView
              placementId="892991524504160_892998361170143"
              type="standard"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
          <View style={[styles.NewsSection]}>
                <View style={styles.TitleView}>
                  <Text style={styles.TitleText}>{Languages.en.EarlierChallenge}</Text>
                </View>
                <FlatList
                  data={WinnerList}
                  keyExtractor={this.keyExtractor}
                  renderItem={this.renderItem}
                  showsVerticalScrollIndicator={false}
                  column={1}
                  ListEmptyComponent={this.emptyView}
                  refreshControl={
                    <RefreshControl
                      refreshing={false}
                      onRefresh={() => this.refreshLevel()}
                      automaticallyAdjustContentInsets={false}
                      colors={[Colors.ThemeBlue]}
                      progressBackgroundColor={Colors.White}
                    />
                  }
                />
            
               
                
                {/* <View style={styles.WinnersView}>
                  <View style={styles.SerialMainView}>
                    <LinearGradient colors={[Colors.ThemePurple, Colors.ThemeBlue]} style={styles.SerialNumberView}>
                      <Text style={styles.SerialNumber}>2</Text>
                    </LinearGradient>
                  </View>
                  <View style={styles.DetailsView}>
                    <Text style={styles.NameText}>Kamini Singh</Text>
                    <Text style={styles.DataText}>29, Dec 2018</Text>
                  </View>
                  <View style={styles.WinnerButtonView}>
                    <TouchableOpacity style={styles.WinnerButtonTouchable}>
                        <Text style={styles.WinnerText}>Winners ></Text>
                    </TouchableOpacity>
                  </View>
                </View> */}
          </View>
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
        <BannerView
              placementId="892991524504160_892998361170143"
              type="standard"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
      </View>
    );
  }
}

Challenge.propTypes = {
  requestStartChallenge: PropTypes.func,
  loading: PropTypes.bool,
  NewsList: PropTypes.array,
  currentUser: PropTypes.object,
  WinnerList: PropTypes.array
};
export default Challenge;
