import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl, Dimensions  } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';
import HTML from 'react-native-render-html';
import * as rssParser from 'react-native-rss-parser';
import _ from "lodash";
imageWidth = Dimensions.get('window').width - 80;
import { Spinner } from '../../components/Spinner';
// import { WebView } from 'react-native-webview';

class NewsFeedDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={navigation.state.params.CategoryTitle} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      PageTitle:'',
      url:'',
      newsData:[],
      dataLength:0,
      loadingMore:true,
      start:0,
      end:5
    }
  }

  componentDidMount() {
    let url=this.props.navigation.getParam("url");
    let title=this.props.navigation.getParam("title");
    this.props.GetNewsFeedDetails(url, title);
  }

  //Render footer (Loader)
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.NewsFeed);
    return true;
  };

  render() {
    const { loading, NewsFeedDetails } = this.props;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
          <View style={styles.NewsSection}>
          <View style={styles.newsFeedView}>
            <View style={styles.TitleView}>
                <Text style={styles.TitleText}>{NewsFeedDetails.Title}</Text>
                <HTML html={NewsFeedDetails.Description} imagesMaxWidth={imageWidth} />
            </View>
              <BannerView
                placementId="892991524504160_913088409161138"
                type="rectangle"
                onPress={() => console.log('click')}
                onLoad={() => console.log('loaded')}
                onError={err => console.log('error', err)}
              />
          </View>
          </View>
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
      </View>
    );
  }
}

NewsFeedDetails.propTypes = {
  GetNewsFeedDetails: PropTypes.func,
  loading: PropTypes.bool,
  NewsFeedDetails: PropTypes.object,
};
export default NewsFeedDetails;
