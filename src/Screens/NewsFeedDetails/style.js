import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height: '100%',
    flex: 1
  },
  //news feed
  TitleView:{
    width:'100%',
    // height:40,
    flexDirection:'column',
    borderColor: Colors.BlackOne,
    borderWidth:1,
    justifyContent:'center',
    marginBottom:10,
    padding:20,
    
  },
  NewsSection:{
    padding:20,
    flexDirection:'column'
  },
  TitleText:{
    fontSize:Constants.FontSize.large,
  },
  DescriptionText:{
    fontSize:Constants.FontSize.medium,
  },
  NewsFeedFlatlist:{
    width:'100%',
    // flexDirection:'row'
  },
  newsFeedView:{
    width:'100%'
  }
})