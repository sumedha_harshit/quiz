
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height: '100%',
    // flex: 1
  },
  //news
  TitleView:{
    width:'100%',
    height:50,
    flexDirection:'column',
    // borderBottomColor: Colors.BlackOne,
    // borderBottomWidth:1,
    marginBottom:10,
    justifyContent:'center',
    alignItems:'center',
    backgroundColor:Colors.LightPurple
  },
  NewsSection:{
    padding:20,
    // paddingBottom:20,
    
    flexDirection:'column'
  },
  TitleText:{
    fontSize:Constants.FontSize.large,
  },
  pickerContainer: {
    borderColor: Colors.WhiteGray,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    marginTop: 0,
    height: 40,
    paddingRight: 10
  },
  pickerTextBox: {
    fontSize: Constants.FontSize.medium,
    paddingLeft: 15,
    height: 40,
    flex: 1,
    color: Colors.Black,
    backgroundColor: 'transparent'
  },
})