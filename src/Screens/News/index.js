import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl, TouchableOpacity } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader, PickerModalWithValue } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';

class News extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={"News Feed"} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      selectNewsList:'',
      NewsList:[],
    }
  }

  componentDidMount() {
    this.props.NewsListRequest((d)=>{this.setList(d)});
    // this.setList(this.props.NewsList);
  }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.GameZone);
    return true;
  };

  renderItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={()=>this.props.navigation.navigate(Constants.Screen.NewsFeed, {item: item})}>
          <View style={styles.TitleView}>
            <Text style={styles.TitleText}>{item.Title}</Text>
          </View>
      </TouchableOpacity>
    )
  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  refreshLevel() {
    this.props.NewsListRequest(()=>{this.props.navigation.navigate(Constants.Screen.News)});
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyle.emptyViewMain}>
        <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
      </View>
    )
  }

  setList = (data) => {
    this.setState({
      NewsList: this.getList(data),
    });
}

  getList(data) {
    var nArray = [];
    nArray.push({ key: 'noselect', value: "", label: "--select--", data:"" })
    data.forEach(element => {
        nArray.push(
            { key: element.Title, value: element.Title, label: element.Title, data: element},
        )
    });
    return nArray;
}

  render() {
    const { loading } = this.props;
    // alert(JSON.stringify(this.state.NewsList));
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
          <View style={[styles.NewsSection]}>
            {/* <FlatList
              data={NewsList}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderItem}
              showsVerticalScrollIndicator={false}
              column={1}
              ListEmptyComponent={this.emptyView}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => this.refreshLevel()}
                  automaticallyAdjustContentInsets={false}
                  colors={[Colors.ThemeBlue]}
                  progressBackgroundColor={Colors.White}
                />
              }
            /> */}
              <PickerModalWithValue
                  items={this.state.NewsList}
                  handlePress={(d) => {
                      this.setState({
                          selectNewsList: d.value
                      })
                      if(d.value !=""){
                        this.props.navigation.navigate(Constants.Screen.NewsFeed, {item: d.data});
                      }
                  }}
                  placeholder={"Select News Type"}
                  placeholderTextColor={Colors.Black}
                  viewContainer={styles.pickerContainer}
                  style={styles.pickerTextBox}
                  selectText={this.state.selectNewsList}
              />
            {/* <TouchableOpacity onPress={()=>this.props.navigation.navigate(Constants.Screen.NewsFeed, {title: "Dainik Bhasker"})}>
                <View style={styles.TitleView}>
                  <Text style={styles.TitleText}>Dainik Bhasker</Text>
                </View>
            </TouchableOpacity> */}
          </View>
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
        <BannerView
              placementId="892991524504160_892998361170143"
              type="rectangle"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
      </View>
    );
  }
}

News.propTypes = {
  NewsListRequest: PropTypes.func,
  loading: PropTypes.bool,
  NewsList: PropTypes.array
};
export default News;
