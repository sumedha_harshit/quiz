
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height: '100%',
     flex: 1,
  },
  NewsSection:{
    flexDirection:'column',
     width: '95%',
     marginLeft:10,
    // justifyContent:'center',
    //  alignItems:'center',
  },
  ChallengeView:{
    width:'100%',
    //backgroundColor:Colors.WhiteGray,
    padding:15,
    marginTop:10,
    // height:50,
    justifyContent:'center',
    borderColor:Colors.WhiteGray,
    borderWidth:1,
    //marginBottom:10
    
  },
  ChallengeText:{
    color:Colors.ThemeBlue,
    fontSize:Constants.FontSize.big,
    letterSpacing:1
  },
  ChallengeDateText:{
    color:Colors.Black,
    fontSize:Constants.FontSize.medium,
  }
})