import React, { Component } from 'react';
import { Alert, View, Text, TouchableOpacity, Image } from 'react-native';
import { Icons, Colors, CommonStyle, Languages } from '@common';
import styles from './style';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, ValidationMessage, Loader, CustomButton } from '@components';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import Constants from '../../app/common/Constants';
import PropTypes from 'prop-types';

class FinishTransfer extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerBackground: (
                <HeaderBackground title={""} />
            ),
            headerTintColor: Colors.White,
            headerLeft:
                <View style={CommonStyles.headerRightView}>
                    <HeaderLeft navigation={navigation} />
                </View>
            ,
            headerRight:
                <View style={CommonStyles.headerRightView}>
                    <HeaderRight navigation={navigation} />
                </View>
        }
    };
    constructor(props) {
        super(props);
        this.state = {
            code:'',
            isButtonAble:true
        }
    }

    confirmPayment(){
        this.setState({isButtonAble: false});
        const type = this.props.navigation.getParam("type");
        if(type == "Banktransfer")
        {
            const data = this.props.navigation.getParam("data");
            this.props.confirmPayment(data, this.state.code, (d)=>this.responseConfirmPayment(d));
        }
        else if(type == "paytm"){
            const withdrawalamount = this.props.navigation.getParam("withdrawalamount");
            const withdrawMethod = this.props.navigation.getParam("withdrawMethod");
            let datapaytm ={
                "withdrawMethod": withdrawMethod, 
                "withdrawalAmount": withdrawalamount,
            }
            this.props.confirmPayment(datapaytm, this.state.code, (d)=>this.responseConfirmPaytmPayment(d));
        }  
        else if( type== "redeem"){
            const PointsRedeem = this.props.navigation.getParam("PointsRedeem");
            this.props.redeemPoints(PointsRedeem, this.state.code, (d)=>this.responseRedeemPoints(d))
        }
    }

    responseRedeemPoints(data){
        Alert.alert("","Points redeem successfully !!");
        this.props.navigation.navigate(Constants.Screen.MyWallet);
    }

    responseConfirmPayment(data){
        if(data.Result == true){
            if(data.Data.status == "Pending")
            {
                this.props.navigation.navigate(Constants.Screen.PendingPayment);
            }
            else if(data.Data.status == "Success")
            {
                this.props.navigation.navigate(Constants.Screen.SuccessPayment);
            }
        }
        else{
            this.setState({isButtonAble:true});
        }
        // Alert.alert("","Bank details added successfully!!")
    }

    responseConfirmPaytmPayment(data){
        if(data.Result == true){
            // this.props.navigation.navigate(Constants.Screen.GameZone);
            if(data.Data.status == "Pending")
            {
                this.props.navigation.navigate(Constants.Screen.PendingPayment);
            }
            else if(data.Data.status == "Success")
            {
                this.props.navigation.navigate(Constants.Screen.SuccessPayment);
            }
        }else{
            this.setState({isButtonAble:true});
        }
        
    }

    render() {
        const {validationMessage, clearWithdrawError, loading } = this.props;
        return (
            <View style={styles.mainView}>
                <View>
                    <Text style={[styles.bigText, CommonStyles.whiteText]}>Confirm Transfer !</Text>
                     <Text style={[CommonStyles.whiteText, CommonStyles.fs14]}>Please enter your passcode</Text>
                    {/* <Text style={[CommonStyles.whiteText, CommonStyles.fs14]}>We have sent a verification code on your phone</Text>
                    <Text style={[CommonStyles.whiteText, CommonStyles.fs14]}>number +919876543210. Please enter verification code below.</Text> */}
                
                </View>
                

                <View style={styles.otpSection}>
                {validationMessage ? (
                        <ValidationMessage message={validationMessage} handleTimeout={clearWithdrawError} />
                ) : (<View />)}
                    <OTPInputView
                        style={styles.OTPInput}
                        pinCount={4}
                        autoFocusOnLoad={false}
                        code={this.state.code}
                        onCodeChanged={(code) => { this.setState({ code: code}) }}
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={styles.underlineStyleHighLighted}
                        // onCodeFilled={(code => {
                        //   alert(code)
                        // })}
                    />
                </View>
                {/* <TouchableOpacity>
                    <Text style={CommonStyle.whiteText}>
                        Didn't receive a code
                    </Text>
                </TouchableOpacity> */}
                {/* <TouchableOpacity style={styles.confirmButtonSection} onPress={()=>this.confirmPayment()}>
                    <Text style={styles.confirmButton}>
                        Confirm
                    </Text>
                </TouchableOpacity> */}
                <View style={styles.confirmButtonSection}>
                    <CustomButton
                        btndisable={this.state.code.length == 4 && this.state.isButtonAble == true ? false : true}
                        onPress={()=>this.confirmPayment()}
                        buttonStyle={this.state.code.length == 4 && this.state.isButtonAble == true  ? CommonStyle.buttonStyleNew : CommonStyle.disabledButtonStyle}
                        titleStyle={CommonStyle.buttonTitleStyle}
                        title={Languages.en.Confirm}
                    />
                </View>
                <Loader
                    modalVisible={loading}
                />
            </View>
        )
    }
}
FinishTransfer.propTypes = {
    confirmPayment : PropTypes.func,
    loading: PropTypes.bool,
    validationMessage: PropTypes.string,
    clearWithdrawError: PropTypes.func,
    redeemPoints: PropTypes.func
};

export default FinishTransfer;