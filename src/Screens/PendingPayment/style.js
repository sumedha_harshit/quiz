import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
    mainView: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        width: '100%'
    },
    looserText: {
        color: Colors.ThemeBlue,
        fontSize: 36,
        textTransform: 'uppercase',
        fontWeight: 'bold',
        letterSpacing: 10
    },
    score: {
        backgroundColor: Colors.ThemeBlue,
        padding: 6,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 50,
        width: wp('40%'),
        marginBottom: 27,
        marginTop: 14
    },
    scoreNumber: {
        fontSize: 30,
        color: '#fff',
        fontWeight: 'bold',
    },
    blueText: {
        color: Colors.ThemeBlue,
        fontSize: 22,
        fontWeight: 'bold',
        textAlign:'center'
    },
    leftText: {
        justifyContent: 'flex-start',
        fontWeight: 'bold'
    },
    rightText: {
        justifyContent: 'flex-end',
        fontWeight: 'bold'
    },
    textValuePair: {
        width: wp('35%'),
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    looserIcon: {
        width: wp('30%'),
        height: wp('30%'),
        resizeMode: 'contain'
    }
})