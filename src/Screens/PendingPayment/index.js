import React, { Component } from 'react';
import { FlatList, View, Text, TouchableOpacity, TextInput, Image } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight } from '@components';
import { BannerView } from 'react-native-fbads';

class PendingPayment extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerBackground: (
                <HeaderBackground title={""} />
            ),
            headerTintColor: Colors.White,
            headerLeft:
                <View style={CommonStyles.headerRightView}>
                    <HeaderLeft navigation={navigation} hideback={true} />
                </View>
            ,
            headerRight:
                <View style={CommonStyles.headerRightView}>
                    <HeaderRight navigation={navigation} showhome={true} />
                </View>
        }
    };
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount(){
        setTimeout(()=>{
            this.props.navigation.navigate(Constants.Screen.GameZone);
        },5000);
    }

    render() {
        return (
            <View style={styles.mainView}>

                <View style={CommonStyle.p20}>
                    <Image style={styles.looserIcon} source={Icons.PendingPaymentIcon} />
                </View>

                <View style={CommonStyle.p5}>
                    <Text style={[styles.looserText]}>{Languages.en.PendingText}</Text>
                </View>

                <View style={CommonStyle.p20}>
                    <Text style={styles.blueText}>{Languages.en.PendingPaymentMessage}</Text>
                </View>
                <BannerView
                    placementId="892991524504160_892998361170143"
                    type="standard"
                    onPress={() => console.log('click')}
                    onLoad={() => console.log('loaded')}
                    onError={err => console.log('error', err)}
                />
            </View>
        )
    }
}
PendingPayment.propTypes = {
};

export default PendingPayment;