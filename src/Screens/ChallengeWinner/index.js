import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler,TouchableHighlight, RefreshControl, TouchableOpacity, TextInput, Image, Modal } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader, CustomButton } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';
import LinearGradient from 'react-native-linear-gradient';
import { WebView } from 'react-native-webview';

class ChallengeWinner extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={Languages.en.Winner} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} hideback={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} showhome={true} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      yourPoints:'',
      // randomNumber:0,
      // randomNumber1:1,
      // randomNumber2:2,
      // randomNumber3:3,
      // randomNumber4:4,
      // randomNumber5:5,
      // randomNumber6:6,
      // randomNumber7:7,
      // randomNumber8:8,
      // randomNumber9:9,
      time: 30,
      minutes:0,
      seconds:20,
      visibleTimer:true,
      winnerUserId:'',
      challengeId:''
      // result:[],
      // winnerPhone:'',
      // winnerName:''
    }
  }

  componentDidMount() {
    var challengeId=this.props.navigation.getParam("challengeId");
    this.setState({challengeId: challengeId});
    // let challengersArray=this.props.navigation.getParam("challengersList");
    //   let item = challengersArray[Math.floor(Math.random() * challengersArray.length)];
    //   let itemArr = item.Phone.split("");
    //   this.setState({winnerUserId: item.UserId, result: itemArr, winnerPhone: item.Phone, winnerName: item.Name});
    //   this.interval = setInterval(
    //   () => this.setState((prevState) => { 
    //     var RandomNumber = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber1 = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber2 = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber3 = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber4 = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber5 = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber6 = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber7 = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber8 = Math.floor(Math.random() * 9) + 0 ;
    //     var RandomNumber9 = Math.floor(Math.random() * 9) + 0 ;
    //     this.setState({ randomNumber : RandomNumber, 
    //       randomNumber1 : RandomNumber1, 
    //       randomNumber2 : RandomNumber2, 
    //       randomNumber3 : RandomNumber3, 
    //       randomNumber4 : RandomNumber4, 
    //       randomNumber5 : RandomNumber5, 
    //       randomNumber6 : RandomNumber6, 
    //       randomNumber7 : RandomNumber7, 
    //       randomNumber8 : RandomNumber8, 
    //       randomNumber9 : RandomNumber9,
    //       time: prevState.time - 1 });
    //   }),
    //   500
      
    // );
    // this.myInterval = setInterval(() => {
    //   const { seconds, minutes } = this.state
    //   if (seconds > 0) {
    //     this.setState(({ seconds }) => ({
    //       seconds: seconds - 1
    //     }))
    //   }
    //   if (seconds === 0) {
    //     if (minutes === 0) {
    //       clearInterval(this.myInterval);
    //       this.setState({visibleTimer: false});
    //     } else {
    //       this.setState(({ minutes }) => ({
    //         minutes: minutes - 1,
    //         seconds: 59
    //       }))
    //     }
    //   }
    // }, 1000)
  }

  // componentDidUpdate() {
  //   // if (this.state.time === 0) {
  //   //   clearInterval(this.interval);
  //   //   this.props.challengeResult(this.state.winnerUserId, this.props.navigation.getParam("challengeId"), (d)=>this.responseChallengeResult(d));
  //   // }
  // }

  // responseChallengeResult =(data)=>{
  //   this.setState({time: 30});
  // }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    // clearInterval(this.interval);
    // clearInterval(this.myInterval)
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.GameZone);
    return true;
  };

  render() {
    const { loading } = this.props;

    return (
      <View style={styles.LoginMain}>
        {/* <KeyboardAwareScrollView
          contentContainerStyle={[CommonStyles.flexGroWScrollViewSec, styles.centerView]}
          showsVerticalScrollIndicator={false}>
            <View style={styles.AdsView}>
                      <BannerView
                          placementId="892991524504160_917721055364540"
                          type="large"
                          onPress={() => console.log('click')}
                          onLoad={() => console.log('loaded')}
                          onError={err => console.log('error', err)}
                      />
              </View> */}
             
                {/* <Modal animationType="slide" transparent={true} visible={visibleTimer}
                  onRequestClose={() => {
                    this.setState({visibleTimer: false});
                  }}
                >
                  <View style={styles.WinViewMain}>
                    <View style={styles.WinView}>
                    <Text style={styles.Countdown}>{minutes < 10 ? "0"+minutes : minutes}:{seconds < 10 ? "0"+seconds : seconds}</Text>
                    </View>
                  </View>
                </Modal> */}
                {/* {visibleTimer == true?
                <View style={styles.centeredView}>
                  <Modal
                    animationType="slide"
                    transparent={true}
                    visible={visibleTimer}
                    onRequestClose={() => {
                      this.setState({visibleTimer: false});
                    }}
                  >
                    <View style={styles.centeredView}>
                      <View style={styles.modalView}>
                        <View style={styles.WinViewMain}>
                          <View style={styles.WinView}>
                          <Text style={styles.modalText}>Challenge will start after</Text>
                          <Text style={styles.Countdown}>{minutes < 10 ? "0"+minutes : minutes}:{seconds < 10 ? "0"+seconds : seconds}</Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </Modal>
                </View>
                : */}
                <WebView 
                  // source={{ uri: 'https://quiz.sumedhasoftech.com/Home/ChallangeResult?challangeId=89' }} 
                  source={{ uri: 'https://quiz.sumedhasoftech.com/Home/ChallangeResult?challangeId='+this.state.challengeId }} 
                  // source={{ uri: 'http://quizadmin.sumedhasoftech.com/Home/AddSupport'}} 
                  
                />
                {/* } */}
              {/* {time == 0?
                <View style={[styles.NewsSection]}>
                  <Text style={styles.MobileCount}>{result[0]}</Text>
                  <Text style={styles.MobileCount}>{result[1]}</Text>
                  <Text style={styles.MobileCount}>{result[2]}</Text>
                  <Text style={styles.MobileCount}>{result[3]}</Text>
                  <Text style={styles.MobileCount}>{result[4]}</Text>
                  <Text style={styles.MobileCount}>{result[5]}</Text>
                  <Text style={styles.MobileCount}>{result[6]}</Text>
                  <Text style={styles.MobileCount}>{result[7]}</Text>
                  <Text style={styles.MobileCount}>{result[8]}</Text>
                  <Text style={styles.MobileCount}>{result[9]}</Text>
                </View>
              :
                <View style={[styles.NewsSection]}>
                  <Text style={styles.MobileCount}>{this.state.randomNumber}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber1}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber2}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber3}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber4}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber5}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber6}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber7}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber8}</Text>
                  <Text style={styles.MobileCount}>{this.state.randomNumber9}</Text>
                </View>
              }
              {time == 0 ?
              <View>
                <View style={styles.PointsInvestView}>
                  <Text style={styles.WinnerText}>WINNER</Text>
                </View>
                <View style={styles.PointsInvestView}>
                  <Text style={styles.NameText}>{winnerName}</Text>
                </View>
                <View style={styles.buttonViewFirst}>
                  <LinearGradient colors={[Colors.ThemePurple, Colors.ThemeBlue]} style={styles.addButton}>
                      <Text style={styles.mobileNumberText}>{winnerPhone}</Text>
                  </LinearGradient>
                </View>
                <View style={styles.CongoView}>
                  <Text style={styles.NameText}>Congratulation!! You won</Text>
                  <Image source={Icons.pointIcon} />
                  <Text style={styles.NameText}>{this.props.navigation.getParam("winningsPoints")}</Text>
                </View>
              </View>
            :
            <View style={styles.WinViewMain}>
              <View style={styles.WinView}>
                <Text style={styles.WinText}>Be patience with increase your chances of winnings</Text>
              </View>
              <View style={styles.WinView}>
                <Text style={styles.Countdown}>{this.state.time}</Text>
              </View>
            </View>
              
            } */}
          {/* <Loader modalVisible={loading} />
        </KeyboardAwareScrollView> */}
        <BannerView
              placementId="892991524504160_892998361170143"
              type="standard"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
      </View>
    );
  }
}

ChallengeWinner.propTypes = {
  NewsListRequest: PropTypes.func,
  loading: PropTypes.bool,
  NewsList: PropTypes.array,
  challengeResult: PropTypes.func
};
export default ChallengeWinner;
