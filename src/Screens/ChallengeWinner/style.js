
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height:wp('100%'),
    flex: 1,
  },
  NewsSection:{
    // marginTop:20,
    padding:20,
    // justifyContent:'space-between',

    flexDirection:'row',
  },
  
  PointsInvestView:{
    //width:'40%',
    alignSelf:'center',
    flexDirection:'column',
    marginTop:20,
    justifyContent:'center',
    alignItems:'center',
  },
  addButton: {
    marginTop:25,
    height: 35,
    width: 120,
    backgroundColor: Colors.ThemeBlue,
    borderRadius: 20,
    alignSelf:'center',
    justifyContent:'center',
    alignItems:'center',
    borderColor: Colors.Silver,
    borderWidth: 1,
  },
  MobileCount:{
    height:30,
    width:30,
    borderRadius:50,
    borderColor:Colors.ThemeBlue,
    borderWidth:1,
    textAlign:'center',
    textAlignVertical:'center',
    color:Colors.ThemeBlue,
    marginLeft:5,
    fontWeight:'bold'
  },
  WinnerText:{
    fontSize:Constants.FontSize.Sizethirty,
    fontWeight:'bold',
    letterSpacing:8
  },
  NameText:{
    fontSize:Constants.FontSize.large,
    fontWeight:'bold',
    color:Colors.ThemeBlue,
  },
  mobileNumberText:{
    color:Colors.White
  },
  centerView:{
    justifyContent:'center',
    alignItems:'center',
    width:'100%'
  },
  CongoView:{
    flexDirection:'row',
    alignItems:'center',
    marginTop:20
  },
  WinText:{
    textAlign:'center',
    color:Colors.Black,
    fontSize:Constants.FontSize.large,
    fontWeight:'bold',
    letterSpacing:2
    // width:'80%'
  },
  WinView:{
    width:'80%',
    justifyContent:'center',
    alignItems:'center'
  },
  Countdown:{
    fontSize:40,
    marginBottom:20,
    marginTop:20,
    fontWeight:'bold',
    textAlign:'center'
  },
  WinViewMain:{
    width:'100%',
    justifyContent:'center',
    alignItems:'center'
  },
  AdsView:{
    alignItems:'center',
    justifyContent:'center'
   },
   centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    // marginTop: 22,
    backgroundColor:"rgba(0,0,0,0.8)"
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  openButton: {
    backgroundColor: "#F194FF",
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }

})