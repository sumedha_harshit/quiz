import React, { Component } from 'react';
import { FlatList, View, Text, TouchableOpacity, TextInput, Image, BackHandler, ScrollView } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import loginStyle from '../Login/style';
import PropTypes from 'prop-types';
import { CustomButton } from '@components';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import * as types from "../../redux/actions/types";
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
    PublisherBanner,
    AdMobInterstitial,

} from 'react-native-admob';
import { BannerView } from 'react-native-fbads';
class Question extends Component {
    renderItem = ({ item, key }) => {
        return (
            <Text style={this.state.selectOption == item ? styles.selectedRow : styles.row} key={key} onPress={() => this.selectAnswer(item)}>
                {item}
            </Text>
        )
    }

    extractKey = (item, key) => key.toString();

    static navigationOptions = ({ navigation }) => {
        return {
            headerBackground: (
                <HeaderBackground title={navigation.state.params.quizTitle} />
            ),
            headerTintColor: Colors.White,
            headerLeft:
                <View style={CommonStyles.headerRightView}>
                    <HeaderLeft navigation={navigation} hideback={true} />
                </View>
            ,
            headerRight:
                <View style={CommonStyles.headerRightView}>
                    <HeaderRight navigation={navigation} />
                </View>
        }
    };
    constructor(props) {
        super(props);
        this.myRef = React.createRef();
        this.state = {
            time: props.questionsData.Questions[0].MaxTime,
            index: 0,
            answeredData: [],
            selectedOption: '',
            submitted: false
        };
    }

    componentDidMount() {
        this.myRef.current.scrollTo(0, 0);
      }
      

    handleBackButton = () => {
        return true;
    };

    componentWillMount() {
        this.setState({index: this.props.questionIndex, answeredData: this.props.answeredDataAll});
        //Ads
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        // AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
        AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

        AdMobInterstitial.addEventListener('adLoaded', () =>
            console.log('AdMobInterstitial adLoaded'),
        );
        AdMobInterstitial.addEventListener('adFailedToLoad', error =>
            console.warn(error),
        );
        AdMobInterstitial.addEventListener('adOpened', () =>
            console.log('AdMobInterstitial => adOpened'),
        );
        AdMobInterstitial.addEventListener('adClosed', () => {
            console.log('AdMobInterstitial => adClosed');
            //AdMobInterstitial.requestAd().catch(error => console.warn(error));
        });
        AdMobInterstitial.addEventListener('adLeftApplication', () =>
            console.log('AdMobInterstitial => adLeftApplication'),
        );

        AdMobInterstitial.requestAd().catch(error => console.log(error));

        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    async componentDidMount() {
        const quizTitle = this.props.navigation.getParam("quizTitle");
        this.setState({ quizTitle: quizTitle });
        this.restartTimer();

        var TodayPlayedQuiz = await AsyncStorage.getItem(Constants.Preferences.TodayPlayedQuiz);
        if (parseInt(TodayPlayedQuiz) % 2 == 0) {
            this.showInterstitial();
        }

    }

    showInterstitial() {
        AdMobInterstitial.showAd().catch(error => console.log(error));
    }

    restartTimer() {
        this.interval = setInterval(
            () => this.setState((prevState) => ({ time: prevState.time - 1 })),
            1000
        );
    }

    componentDidUpdate() {
        const { index, time } = this.state;
        const { questionsData } = this.props;
        const playerId = this.props.navigation.getParam("playerId");
        let lastindex = questionsData.Questions.length - 1;
        if (this.state.submitted == false) {
            if (index != lastindex) {
                if (time == 0) {
                    clearInterval(this.interval);
                    if (this.state.selectedOption == "") {
                        this.setData("");
                    }
                    this.setState({ index: index + 1, time: questionsData.Questions[index + 1].MaxTime, selectedOption: '' });
                    this.restartTimer();
                }
            } else {
                if (time == 0) {
                    this.setState({ submitted: true });
                    clearInterval(this.interval);
                    if (this.state.selectedOption == "") {
                        let oldAnsweredData = [...this.state.answeredData];
                        let answeredData = {
                            QuizQuestionID: questionsData.Questions[index].QuizQuestionID,
                            SelectedOption: "",
                            TimeTaken: this.state.time == 0 ? questionsData.Questions[index].MaxTime : questionsData.Questions[index].MaxTime - this.state.time
                        }
                        oldAnsweredData.push(answeredData);
                        this.setState({ submitted: true, answeredData: oldAnsweredData });
                        let playedDate = new Date();
                        this.props.requestSubmitQuiz(global.UserId, this.props.questionsData.Questions[0].QuizID, playedDate, oldAnsweredData, (d) => this.responseData(d), playerId);
                    }
                }
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    selectAnswer = (item) => {
        this.setState({ selectOption: item });
        this.setData(item);
    }

    setData(selectedAnswer) {
        const { questionsData } = this.props;
        const { index } = this.state;
        let oldAnsweredData = [...this.state.answeredData];
        let answeredData = {
            QuizQuestionID: questionsData.Questions[index].QuizQuestionID,
            SelectedOption: selectedAnswer,
            TimeTaken: this.state.time == 0 ? questionsData.Questions[index].MaxTime : questionsData.Questions[index].MaxTime - this.state.time
        }
        var questionIndex = oldAnsweredData.map(item => item.QuizQuestionID).indexOf(questionsData.Questions[index].QuizQuestionID);
        if (questionIndex != -1) {
            oldAnsweredData[questionIndex] = answeredData;
        }
        else {
            oldAnsweredData.push(answeredData);
        }
        this.setState({ answeredData: oldAnsweredData });
        this.props.nextQuestion(this.state.index, this.state.answeredData);
    }

    loadNextQuestion(index, time) {
        this.setState({ index: index, time: time, selectOption: '' });
        this.props.nextQuestion(this.state.index, this.state.answeredData);
        this.scroll.scrollTo({x: 0, y: 0, animated: true});
        // window.scrollTo(100, 0);
    }

    submitQuiz() {
        clearInterval(this.interval);
        const playerId = this.props.navigation.getParam("playerId");
        this.setState({ submitted: true });
        let playedDate = new Date();
        this.props.requestSubmitQuiz(global.UserId, this.props.questionsData.Questions[0].QuizID, playedDate, this.state.answeredData, (d) => this.responseData(d), playerId);
    }

    responseData(data) {
        if (data.IsWon) {
            this.props.navigation.navigate("Winner", { "points": data.PointsEarned, "percentage": data.PercentageEarn, "timeTaken": data.TimeTakeninSeconds })
        } else {
            this.props.navigation.navigate("Looser", { "points": data.PointsEarned, "percentage": data.PercentageEarn, "timeTaken": data.TimeTakeninSeconds })
        }
    }

    render() {
        const { time, index } = this.state;
        const { questionsData, loading } = this.props;
        let allQuestions = [];
        let currentQuestion = {};
        if (Object.keys(questionsData).length > 0) {
            allQuestions = questionsData.Questions;
            currentQuestion = allQuestions[index];
        }
        return (
            Object.keys(currentQuestion).length > 0 &&
            (
                <ScrollView
                ref={(c) => {this.scroll = c}}
              ><View style={styles.mainView}>

                <View>
                    <Image source={Icons.headerCureved} />
                </View>
                <View style={styles.timerSection}>
                    <View style={styles.timerLeft}>
                        <Image source={Icons.giftIcon} style={{ marginRight: 5 }} />
                        <Text style={[CommonStyle.whiteText, styles.timer]}>{currentQuestion.QuestionPoint} Points</Text>
                    </View>
                    <View style={styles.timerRight}>
                        <Text style={[CommonStyle.whiteText, styles.timer]}>00:{time < 10 ? "0" + time : time == 0 ? "00" : time}</Text>
                        <Image source={Icons.timerIcon} />
                    </View>
                </View>
                <KeyboardAwareScrollView
                    contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
                    showsVerticalScrollIndicator={false}>
                    <View style={styles.questionTextSection}>
                        <Text style={[styles.questionText, CommonStyle.fs19]}>
                            {currentQuestion.Question}
                        </Text>
                    </View>
                    <View style={styles.questionImageSection}>
                        <BannerView
                            placementId="892991524504160_917721055364540"
                            type="large"
                            onPress={() => console.log('click')}
                            onLoad={() => console.log('loaded')}
                            onError={err => console.log('error', err)}
                        />

                    </View>
                    {/* {
                    currentQuestion.ImageUrl ? 
                    <View style={styles.questionImageSection}>
                        <Image source={{ uri: currentQuestion.ImageUrl }} style={styles.questionImage} />
                    </View>
                    :
                    <View />
                } */}
                    <View style={styles.optionSection}>
                        <FlatList
                            style={styles.container}
                            data={currentQuestion.Options}
                            extraData={this.state}
                            renderItem={this.renderItem}
                            keyExtractor={this.extractKey}
                            showsVerticalScrollIndicator={false}
                        />
                    </View>
                    {index != allQuestions.length - 1 ?
                        <View style={styles.buttonCenterSection}>
                            <CustomButton
                                buttonStyle={[CommonStyle.buttonStyle, styles.customButton]}
                                titleStyle={[CommonStyle.buttonTitleStyle, styles.buttonTitleStyle]}
                                onPress={() => this.loadNextQuestion(index + 1, currentQuestion.MaxTime)}
                                title={"Next"}
                            />
                        </View>
                        :
                        <View style={styles.buttonCenterSection}>
                            <CustomButton
                                buttonStyle={[CommonStyle.buttonStyle, styles.customButton]}
                                titleStyle={[CommonStyle.buttonTitleStyle, styles.buttonTitleStyle]}
                                title={"Submit"}
                                onPress={() => this.submitQuiz()}
                            />
                        </View>
                    }
                    <Loader
                        modalVisible={loading}
                    />
                </KeyboardAwareScrollView>
                <BannerView
                    placementId="892991524504160_892998361170143"
                    type="standard"
                    onPress={() => console.log('click')}
                    onLoad={() => console.log('loaded')}
                    onError={err => console.log('error', err)}
                />
            </View>
            </ScrollView>)

        )
    }
}
Question.propTypes = {
    questionsData: PropTypes.object,
    type: PropTypes.string,
    requestSubmitQuiz: PropTypes.func,
    loading: PropTypes.bool,
    questionIndex: PropTypes.int,
    nextQuestion: PropTypes.func,
    answeredDataAll: PropTypes.array
};

export default Question;