import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
    mainView: {
        flex: 1
    },
    questionTextSection: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 15,
        width: '100%'
    },
    questionText: {
        textAlign: 'justify'
    },
    questionImageSection: {
        borderWidth: 0,
        borderBottomWidth: 1,
        borderColor: '#000',
        width: '100%',
        padding: 15,
        alignItems:'center'
    },
    questionImage: {
        height: wp('20%'),
        width: '100%',
        marginBottom: 15,
        resizeMode: 'cover'
    },
    optionSection: {
        height: wp('58%'),
        marginBottom: 10
    },
    container: {
        flex: 1,
        borderWidth: 0
    },
    row: {
        padding: 14,
        textAlign: 'center',
        borderWidth: 0,
        borderBottomWidth: 1,
        fontSize: 16,
        fontWeight: '600',
        letterSpacing: 2
    },
    selectedRow: {
        padding: 14,
        textAlign: 'center',
        borderWidth: 0,
        borderBottomWidth: 1,
        fontSize: 16,
        fontWeight: '600',
        letterSpacing: 2,
        color: "#fff",
        backgroundColor: Colors.ThemeBlue,
    },
    customButton: {
        width: wp('72%'),
        height: wp('12.8%')
    },
    buttonCenterSection: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonTitleStyle: {
        fontSize: 18,
        fontWeight: '600'
    },
    timer: {
        marginRight: 5,
        textAlignVertical: 'center',
        fontSize: wp('3.25'),
        fontWeight: 'bold'
    },
    timerSection: {
        flex: 1,
        justifyContent: 'space-between',
        flexDirection: 'row',
        position: 'absolute',
        width: '100%',
        marginTop: 25,
        padding: 10
    },
    timerLeft: {
        justifyContent: 'flex-start',
        flex: 1,
        flexDirection: 'row'
    },
    timerRight: {
        justifyContent: 'flex-end',
        flex: 1,
        flexDirection: 'row',
    }
})