import React, { Component } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import styles from './style';
import CommonStyles from '../../app/common/CommonStyle';
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { Languages, Constants } from '@common';
import { Loader } from '@components';


class Passcode extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            header: null,
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            passcode:'',
            mobileNumber:'',
            type:'',
            userid:''
        }
    }

    componentDidMount(){
        const getMobile= this.props.navigation.getParam("mobilenumber");
        const getType= this.props.navigation.getParam("type");
        const getUserid= this.props.navigation.getParam("UserDataId");
        this.setState({ mobileNumber: getMobile, type: getType, userid: getUserid });

    }

    savePasscode(code){
        this.props.SavePasscode(this.state.userid, code, ()=>{this.props.navigation.navigate(Constants.Screen.VerificationScreen, {mobilenumber: this.state.mobileNumber, action: "signup"})})
    }

    render() {
        const {loading} = this.props;
        return (
            <View style={styles.mainView}>
                <View>
                    <Text style={[styles.bigText, CommonStyles.whiteText]}>{Languages.en.SetYourPasscode}</Text>
                    <Text style={[CommonStyles.whiteText, CommonStyles.fs14]}>{Languages.en.SetPasscodeContent}</Text>
                </View>
                <View style={styles.otpSection}>
                    <OTPInputView
                        style={styles.OTPInput}
                        pinCount={4}
                        autoFocusOnLoad={false}
                        code={this.state.passcode} 
                        onCodeChanged={code => { this.setState({passcode: code }) }}
                        codeInputFieldStyle={styles.underlineStyleBase}
                        codeInputHighlightStyle={styles.underlineStyleHighLighted}
                        // onCodeFilled={(code => {
                        //     this.savePasscode(code);
                        // })}
                    />
                </View>

                <TouchableOpacity style={styles.confirmButtonSection} onPress={()=>this.savePasscode(this.state.passcode)}>
                    <Text style={styles.confirmButton}>
                        {Languages.en.SavePasscode}
                    </Text>
                </TouchableOpacity>
                <Loader modalVisible= { loading } />
            </View>
        )
    }
}
Passcode.propTypes = {
    SavePasscode: PropTypes.func,
    loading: PropTypes.bool
};

export default Passcode;