
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
    mainView: {
        backgroundColor: Colors.ThemeBlue,
        width: '100%',
        flex: 1,
        padding: 25,
        paddingTop: wp('25%')
    },
    bigText: {
        fontSize: 30,
        fontWeight: 'bold',
        marginBottom: 10
    },
    otpSection: {
        marginTop: 25
    },
    OTPInput: {
        width: wp('80%'),
        height: wp('25%')
    },
    underlineStyleBase: {
        borderRadius: 10,
        fontSize: 18,
        fontWeight: 'bold',
        color: '#fff',
        width: wp('13%'),
        height: wp('11.5%')
    },
    underlineStyleHighLighted: {
        backgroundColor: '#fff',
        fontSize: 20,
        color: Colors.ThemeBlue
    },
    confirmButtonSection: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        flex: 1,
        marginBottom: 10,
        marginTop:20
    },
    confirmButton: {
        width: '100%',
        textAlign: 'center',
        borderRadius: 25,
        fontSize: 18,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        color: Colors.ThemeBlue,
        lineHeight: wp('11.5%'),
        height: wp('11.5%')
    }
});