import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';
import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';

class QuizScore extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={navigation.state.params.quizTitle} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      index: 0,
      QuizId: '',
      QuizTitle: ''
    }
  }

  componentDidMount() {
    const quizid = this.props.navigation.getParam("quizId");
    const quiztitle = this.props.navigation.getParam("quizTitle");
    this.setState({ QuizId: quizid, QuizTitle: quiztitle });
    this.props.getUsersScore(quizid, global.UserId);
    this.showInterstitial();
  }

  componentWillMount() {

    //Ads
    AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
    // AdMobInterstitial.setAdUnitID('ca-app-pub-3940256099942544/1033173712');
    AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

    AdMobInterstitial.addEventListener('adLoaded', () =>
      console.log('AdMobInterstitial adLoaded'),
    );
    AdMobInterstitial.addEventListener('adFailedToLoad', error =>
      console.warn(error),
    );
    AdMobInterstitial.addEventListener('adOpened', () =>
      console.log('AdMobInterstitial => adOpened'),
    );
    AdMobInterstitial.addEventListener('adClosed', () => {
      console.log('AdMobInterstitial => adClosed');
      //AdMobInterstitial.requestAd().catch(error => console.warn(error));
    });
    AdMobInterstitial.addEventListener('adLeftApplication', () =>
      console.log('AdMobInterstitial => adLeftApplication'),
    );

    AdMobInterstitial.requestAd().catch(error => console.log(error));
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  showInterstitial() {
    AdMobInterstitial.showAd().catch(error => console.log(error));
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.GameZone);
    return true;
  };

  renderItem = ({ item }) => {
    return (
      <View style={[CommonStyle.row, styles.listItemsAll]}>
        <Text style={[styles.listItems, item.status == true ? styles.Highlight : '']}>{item.Name}</Text>
        <Text style={[styles.listItems, item.status == true ? styles.Highlight : '']}>{item.Score}%</Text>
        <Text style={[styles.listItems, item.status == true ? styles.Highlight : '']}>{item.Time}</Text>
      </View>
    )
  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  addListHeader = () => {
    return (
      <View style={styles.LevelHeaderView}>
        <View style={styles.listHeader}>
          <Text style={styles.listHeaderItem}>Name</Text>
          <Text style={styles.listHeaderItem}>Score</Text>
          <Text style={styles.listHeaderItem}>Time</Text>
        </View>
      </View>
    )
  }

  refreshLevel() {
    this.props.getUsersScore(this.state.QuizId, global.UserId);
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyle.emptyViewMain}>
        <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
      </View>
    )
  }

  render() {
    const { loading, AllScoresData } = this.props;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
          <View>
            <Text style={styles.LoginText}>{Languages.en.Score}</Text>
          </View>
          <View style={[styles.tabSection]}>
            <FlatList
              data={AllScoresData}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderItem}
              ListHeaderComponent={this.addListHeader}
              showsVerticalScrollIndicator={false}
              column={1}
              stickyHeaderIndices={[0]}
              style={styles.NetworkFlatlist}
              ListEmptyComponent={this.emptyView}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => this.refreshLevel()}
                  automaticallyAdjustContentInsets={false}
                  colors={[Colors.ThemeBlue]}
                  progressBackgroundColor={Colors.White}
                />
              }
            />
          </View>
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
        <BannerView
              placementId="892991524504160_892998361170143"
              type="standard"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
      </View>
    );
  }
}

QuizScore.propTypes = {
  getUsersScore: PropTypes.func,
  loading: PropTypes.bool,
  AllScoresData: PropTypes.array
};
export default QuizScore;
