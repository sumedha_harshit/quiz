import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler,RefreshControl } from 'react-native';
import { WebView } from 'react-native-webview';
import { Icons, Colors, Languages, CommonStyle, Constants } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';

class PrivacyPolicy extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={Languages.en.PrivacyPolicy} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
            <HeaderLeft navigation={navigation}  sidemenu={true} fromhome={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
    }
  }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }
  
  componentWillUnmount()
   { 
     this.backHandler.remove(); 
   }
  
  handleBackButton = () => {
      this.props.navigation.navigate(Constants.Screen.GameZone);
      return true;
  };

  render() {
    return (
      <View style={styles.LoginMain}>
        <WebView 
          source={{ uri: 'http://quizadmin.sumedhasoftech.com/Home/PrivacyPolicy' }} 
        />
        {/* <Loader modalVisible={loading} /> */}
      </View>
    );
  }
}

PrivacyPolicy.propTypes = {
  
};
export default PrivacyPolicy;
