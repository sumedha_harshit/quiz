import React, { Component } from 'react';
import { Alert, View, Text, TouchableOpacity, TextInput, Image, AsyncStorage, ScrollView, FlatList, RefreshControl, ImageBackground, BackHandler } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import * as types from "../../redux/actions/types";
import PropTypes from 'prop-types';
import SplashScreen from 'react-native-splash-screen';
import Modal from "react-native-modal";
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader, CustomButton } from '@components';
import _ from "lodash";
import {
    PublisherBanner,
    AdMobInterstitial,
} from 'react-native-admob';
import { BannerView } from 'react-native-fbads';

class GameZone extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerBackground: (
                <HeaderBackground title={"Game Zone"} />
            ),
            headerTintColor: Colors.White,
            headerLeft:
                <View style={CommonStyles.headerRightView}>
                    <HeaderLeft navigation={navigation} fromhome={true} sidemenu={true} />
                </View>
            ,
            headerRight:
                <View style={CommonStyles.headerRightView}>
                    <HeaderRight navigation={navigation} />
                </View>
        }
    };
    constructor(props) {
        super(props);
        this.state = {
            dataFetched: false,
            quizTitle: "",
            fluidSizeIndex: 0,
            ModalVisibility: true,
        };
    }
    
    handleBackButton = () => {
        BackHandler.exitApp();
        return true;
    };

 
    async componentDidMount() {
        var user = await AsyncStorage.getItem(Constants.Preferences.currentUser);
        //  alert(JSON.stringify(user));
        if (user != null) {
        var jsonUser = JSON.parse(user);
        if(jsonUser.FCMToken =="" || jsonUser.FCMToken == null ){
            this.props.navigation.navigate(Constants.Screen.LoginScreen);
            // this.props.logout();
        }
        }
        // this.props.getAllQuiz();
        this.didBlurSubscription = this.props.navigation.addListener(
            'didFocus',
            payload => {
                this.props.getAllQuiz((d)=>{this.responseGetAllQuiz(d)});
                
            }
        );
        this.showInterstitial();
      }
    
    responseGetAllQuiz=(data)=>{
    this.props.challengeRequest();
    // this.props.ChallengeStartingSoon();
    }

    componentWillMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

        AdMobInterstitial.addEventListener('adLoaded', () =>
            console.log('AdMobInterstitial adLoaded'),
        );
        AdMobInterstitial.addEventListener('adFailedToLoad', error =>
            console.warn(error),
        );
        AdMobInterstitial.addEventListener('adOpened', () =>
            console.log('AdMobInterstitial => adOpened'),
        );
        AdMobInterstitial.addEventListener('adClosed', () => {
            console.log('AdMobInterstitial => adClosed');
            //AdMobInterstitial.requestAd().catch(error => console.warn(error));
        });
        AdMobInterstitial.addEventListener('adLeftApplication', () =>
            console.log('AdMobInterstitial => adLeftApplication'),
        );

        AdMobInterstitial.requestAd().catch(error => console.log(error));
    }

    componentWillUnmount() {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
        this.didBlurSubscription.remove();
    }

    showInterstitial() {
        AdMobInterstitial.showAd().catch(error => console.log(error));
    }

    componentWillReceiveProps(props) {
        if (props.type == types.GET_ALL_QUIZ_SUCCESS) {
            if (this.state.dataFetched == false) {
                this.setState({ dataFetched: true });
            }
        }
    }

    getQuizQuestion(quizId, quizTitle) {
        this.setState({ quizTitle: quizTitle});
        this.props.getAllQuizQuestions(quizId,(d)=>this.getQuizQuestionResponse(d));
    }

    //Used to show When Data is empty
    emptyView = () => {
        return (
        <View style={[CommonStyle.emptyViewMain,{marginBottom:10}]}>
            <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.QuizDataNotFound}</Text>
        </View>
        )
    }

    emptyViewFirst = () => {
        return (
        <View style={[CommonStyle.emptyViewMain,{marginBottom:10}]}>
            <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.QuizComingSoon}</Text>
        </View>
        )
    }

    getQuizQuestionResponse(data){
        if(data.Result == true){
            this.props.navigation.navigate('Question', { questionsData: this.props.questionsData, playerId: data.Data, quizTitle: this.state.quizTitle });
        }
        else{
            Alert.alert("",data.Message);
        }
    }

    checkPlayedQuizScore(quizid, quiztitle){
        this.props.navigation.navigate(Constants.Screen.QuizScore, { quizId: quizid, quizTitle: quiztitle });
    }

    toggleModal = () => {
        this.setState({ ModalVisibility: !this.state.ModalVisibility });
    };

    render() {
        const { dataFetched } = this.state;
        const { quizData, loading , currentUser, requestedChallengeIds, startingSoonChallengeList} = this.props;
        let newQuiz =_.filter(quizData.QuizList, { AlreadyPlayed: false });
        let playedQuiz =_.filter(quizData.QuizList, { AlreadyPlayed: true });
        let soonChallenge =_.filter(startingSoonChallengeList, { CreatedByUserId: global.UserId });
        return (
            
            <ScrollView style={[styles.mainView]} refreshControl={
                <RefreshControl
                    refreshing={false}
                    onRefresh={() => this.props.getAllQuiz((d)=>{this.responseGetAllQuiz(d)})}
                    automaticallyAdjustContentInsets={false}
                    colors={[Colors.ThemeBlue]}
                    progressBackgroundColor={Colors.White}
                />
            }>
                <View style={styles.SubHeaderView}>
                    <Text style={styles.SubHeaderText}>{Languages.en.homePageSubheading} {quizData.MinimumQuiz} quiz</Text>
                </View>
                {/* {
                startingSoonChallengeList && startingSoonChallengeList.length > 0 && soonChallenge.length == 0 ?
                    <TouchableOpacity onPress={()=>this.props.navigation.navigate(Constants.Screen.StartSoonChallenge)}>
                        <View style={styles.NotificationView}>
                            {Svgs.BellIcon}<Text style={styles.SubHeaderText}>{startingSoonChallengeList.length} Challenge(s) going to start within 2 minutes.</Text>
                        </View>
                    </TouchableOpacity>
                    :
                    <View />
                }  */}
                {/* {
                requestedChallengeIds && requestedChallengeIds.length > 0 ?
                    // <TouchableOpacity onPress={()=>this.props.navigation.navigate(Constants.Screen.RequestedChallenge)}>
                    //     <View style={styles.NotificationView}>
                    //         {Svgs.BellIcon}<Text style={styles.SubHeaderText}> You have received new challenge request.</Text>
                    //     </View>
                    // </TouchableOpacity>
                    <Modal style={CommonStyles.modalContainerTimer} isVisible={this.state.ModalVisibility}>
                        <View style={CommonStyles.modalView}>
                            <View style={CommonStyles.modalHeaderViewTimer}>
                                <View style={CommonStyles.headingModal}><Text style={CommonStyles.modalHeaderTitleProfile}></Text></View>
                                <View style={CommonStyles.closeIconModal}>
                                    <TouchableOpacity style={CommonStyles.modalCloseViewTimer} onPress={() => this.toggleModal()}>
                                        <Image source={Icons.CloseModalImage} style={CommonStyles.modalCloseIcon} resizeMode={'stretch'} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={CommonStyles.modalSubViewTimer}>
                                <View style={styles.NotificationViewModal}>
                                    {Svgs.BellIcon}<Text style={styles.ModalSubHeaderText}> You have received new challenge request.</Text>
                                </View>
                                <CustomButton
                                    buttonStyle={CommonStyles.buttonStyleProfileBrowse}
                                    titleStyle={CommonStyles.ProfilebuttonTitleStyle}
                                    title="Check Request"
                                    onPress={() => { this.props.navigation.navigate(Constants.Screen.RequestedChallenge) }}
                                />
                                <CustomButton
                                    buttonStyle={CommonStyles.buttonStyleProfile}
                                    titleStyle={CommonStyles.ProfilebuttonTitleStyle}
                                    title="Close"
                                    onPress={() => { this.toggleModal() }}
                                />
                            </View>
                        </View>
                    </Modal>
                    :
                    <View />
                }  */}
                <View style={styles.QuizViewInner} >
                     
                    <FlatList
                        data={newQuiz}
                        keyExtractor={item => item.QuizID}
                        numColumns={2}
                        showsVerticalScrollIndicator={false} 
                        ListHeaderComponent={()=>(
                            <View style={styles.headerList}>
                                <Text style={[styles.QuizHeading, CommonStyle.fs18]}>New Quiz</Text>
                            </View>
                        )}
                        renderItem={({ item, key }) => (
                            <TouchableOpacity key={key} onPress={() => this.getQuizQuestion(item.QuizID, item.QuizTitle)} >
                                <ImageBackground source={{ uri: item.QuizBannerImage }} style={[styles.gameCard]}>
                                    <View style={styles.imageDiv}>
                                        <Image source="" style={styles.gameImage} />
                                    </View>
                                    <View style={styles.gameHeadingArea}>
                                        <Text style={[styles.gameHeading, styles.whiteText]}>
                                            {item.QuizTitle}
                                        </Text>
                                        <View style={styles.directedText}>
                                            <View>
                                                <Text style={[styles.whiteText, styles.fs12]}>
                                                    {item.StartDateStr}
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </ImageBackground>
                            </TouchableOpacity>
                            
                        )}
                        ListEmptyComponent={this.emptyViewFirst}
                    />
                    <View style={styles.walletBalanceMain}>
                        <View style={styles.walletBalance}>
                            <View style={styles.walletBalanceInner}>
                                <Text style={[styles.BalanceText, CommonStyle.fs13]}>{Languages.en.Yourbalance}</Text>
                                <Text style={[styles.MoneyText, CommonStyle.fs21]}>{Constants.Symbol.rupee} {quizData.CurrentBalance ? quizData.CurrentBalance : '00.00'}</Text>
                            </View>
                            <View style={styles.walletBalanceInner}>
                                <Text style={[styles.BalanceText, CommonStyle.fs13]}>{Languages.en.MonthlyEarning}</Text>
                                <Text style={[styles.MoneyText, CommonStyle.fs21]}>{Constants.Symbol.rupee} {quizData.MothlyIncome ? quizData.MothlyIncome : '00.00'}</Text>
                            </View>
                            <View style={styles.walletBalanceInner}>
                                <Text style={[styles.BalanceText, CommonStyle.fs13]}>{Languages.en.TotalWithdraw}</Text>
                                <Text style={[styles.MoneyText, CommonStyle.fs21]}>{Constants.Symbol.rupee} {quizData.TotalWithdraw ? quizData.TotalWithdraw : '00.00'}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={styles.AdsView}>
                            <BannerView
                                placementId="892991524504160_917721055364540"
                                type="large"
                                onPress={() => console.log('click')}
                                onLoad={() => console.log('loaded')}
                                onError={err => console.log('error', err)}
                            />
                    </View>

                    <FlatList
                        data={playedQuiz}
                        keyExtractor={item => item.QuizID}
                        showsVerticalScrollIndicator={false}                        
                        numColumns={2}
                        ListHeaderComponent={()=>(
                            <View style={styles.headerList}>
                                <Text style={[styles.QuizHeading, CommonStyle.fs18]} >Previously Played Quiz</Text>
                            </View>
                        )}
                        renderItem={({ item, key }) => (
                            <TouchableOpacity key={key} onPress={() => this.checkPlayedQuizScore(item.QuizID, item.QuizTitle)}  >
                                <ImageBackground source={{ uri: item.QuizBannerImage }} style={[styles.gameCard]}>
                                    <View style={styles.imageDiv}>
                                        <Image source="" style={styles.gameImage} />
                                        {/* <Image source={{ uri: item.PlayingDescriptionImg }} style={styles.gameImage} /> */}
                                    </View>
                                    <View style={styles.gameHeadingArea}>
                                        <Text style={[styles.gameHeading, styles.whiteText]}>
                                            {item.QuizTitle}
                                        </Text>
                                        <View style={styles.directedText}>
                                            <View>
                                                <Text style={[styles.whiteText, styles.fs12]}>
                                                    {item.StartDateStr}
                                                </Text>
                                                <Text style={[styles.whiteText, styles.fs12]}>
                                                    {/* {Constants.Symbol.bullet} Starts in 03h:24m:10s */}
                                                    {Languages.en.LastScore} : {item.WinPrecentage}%
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                </ImageBackground>
                            </TouchableOpacity>
                           
                        )}
                        // refreshControl={
                        //     <RefreshControl
                        //         refreshing={false}
                        //         onRefresh={() => this.props.getAllQuiz()}
                        //         automaticallyAdjustContentInsets={false}
                        //         colors={[Colors.ThemeBlue]}
                        //         progressBackgroundColor={Colors.White}
                        //     />
                        // }
                        ListEmptyComponent={this.emptyView}
                        />
                    </View>
                <Loader
                    modalVisible={loading}
                />
                    {/* <AdMobBanner
                        adSize="fullBanner"
                        adUnitID="892991524504160_1010685692734742"
                        testDevices={["EMULATOR"]}
                        onAdFailedToLoad={error => alert(error)}
                    /> */}
                    {/* <View style={{flexDirection:'column', width:'100%', marginTop:15}}>
                    <PublisherBanner
                    adSize="mediumRectangle"
                    adUnitID="ca-app-pub-3940256099942544/6300978111"
                    // testDevices={["33BE2250B43518CCDA7DE426D04EE231"]}
                    onAdFailedToLoad={error => alert(error)}
                    onAppEvent={event => alert(event.name, event.info)}
                    />
                    
                    </View> */}
                   <BannerView
                        placementId="892991524504160_892998361170143"
                        type="standard"
                        onPress={() => console.log('click')}
                        onLoad={() => console.log('loaded')}
                        onError={err => console.log('error', err)}
                    />
            </ScrollView>
            
        );
    }
}

GameZone.propTypes = {
    getAllQuiz: PropTypes.func,
    getAllQuizQuestions: PropTypes.func,
    getPlayerId: PropTypes.func,
    quizData: PropTypes.object,
    type: PropTypes.string,
    typeGetQuestion: PropTypes.string,
    questionsData: PropTypes.object,
    playerId: PropTypes.number,
    loading: PropTypes.bool,
    currentUser: PropTypes.object,
    requestedChallengeIds: PropTypes.array,
    ChallengeStartingSoon: PropTypes.func,
    startingSoonChallengeList: PropTypes.array
};

export default GameZone;