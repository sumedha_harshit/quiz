import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  mainView: {
    // paddingTop: 8,
    // paddingRight:3,
    // paddingLeft:3,
    // padding:10,
    width: '100%',
    // flexDirection: 'row',
    flex: 1,
    // height:'100%'
  },
  whiteText: {
    color: '#fff'
  },
  gameCard: {
    width: wp("47.50%"),
    padding: 3,
    margin: 3,
    borderRadius: 5,
    flexDirection: 'column',
  },
  imageDiv: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row'
  },
  gameImage: {
    height: wp('20%'),
    width: 60,
    resizeMode: 'contain',
    margin: 10
  },
  gameHeadingArea: {
    alignItems: 'flex-start',
    justifyContent: 'flex-start'
  },
  gameHeading: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  directedText: {
    flexDirection: 'row'
  },
  fs12: {
    fontSize: wp('2.75%'),
  },
  rightText: {
    // marginLeft: 9
  },
  italic: {
    fontStyle: 'italic'
  },
  boldFont: {
    fontWeight: 'bold'
  },
  QuizHeader:{
    width:'100%',
    flexDirection:'column',
    paddingTop:10,
    paddingBottom:15,
    paddingLeft:5,
    backgroundColor:Colors.White
  },
  QuizHeading: {
    
  },
  QuizViewInner:{width:'100%', flexDirection:'column', flex:1, paddingBottom:10,
  paddingTop: 8,
  paddingRight:3,
  paddingLeft:3,
},
  headerList:{
    paddingTop:10,
    paddingBottom:10,
    paddingLeft:5
  },
  walletBalanceMain:{
    width:'100%',
    flexDirection:'row',
    paddingLeft:5,
    paddingRight:5
  },
  walletBalance:{
    width:'100%',
    flexDirection:'row',
    borderWidth:1,
    borderColor:Colors.WhiteGray,
    padding:15,
    marginBottom:10,
    marginTop:10
  },
  AdsView:{
   alignItems:'center',
   justifyContent:'center'
  },
  walletBalanceInner:{
    width:'33.33%',
  },
  BalanceText: {
    paddingBottom: 10,
    color: Colors.BlackOne,
    textAlign:'left'
  },
  MoneyText: {
    paddingBottom: 10,
    textAlign:'left',
    color: Colors.ThemeBlue,
    fontWeight: 'bold'
  },
  SubHeaderView:{
    backgroundColor:Colors.ThemeBlue,
    padding:10,
  },
  SubHeaderText:{
    color:Colors.White,
    fontSize:Constants.FontSize.medium,
    textAlign:'center',
  },
  ModalSubHeaderText:{
    color:Colors.Black,
    fontSize:22,
    textAlign:'center',
    lineHeight:30
  },
  NotificationView:{
    width:'95%',
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:Colors.ThemeBlue,
    borderRadius:10,
    marginTop:10,
    height:50,
    alignSelf:'center',
    flexDirection:'row',
    paddingLeft:10,
    paddingRight:10
  },
  NotificationViewModal:{
    width:'95%',
    alignItems:'flex-start',
    justifyContent:'center',
    marginTop:10,
    // height:50,
    alignSelf:'center',
    flexDirection:'row',
    paddingLeft:10,
    paddingRight:10,
    // borderWidth:1
  }
});
