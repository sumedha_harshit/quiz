
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height: '100%',
    flex: 1
  },
  //news
  TitleView:{
    width:'100%',
    height:40,
    flexDirection:'column',
    borderBottomColor: Colors.BlackOne,
    // borderBottomWidth:1,
    justifyContent:'center',
    marginBottom:20
  },
  NewsSection:{
    paddingLeft:20,
    paddingRight:20,
    flexDirection:'column'
  },
  PointsInvestView:{
    width:'100%',
    alignSelf:'center',
    flexDirection:'row',
    padding:15,
  },
  PointsInvestView1:{
    width:'100%',
    alignSelf:'center',
    flexDirection:'row',
    paddingLeft:15,
    paddingRight:15,
    paddingBottom:15,
  },
  PointsInvestFirstView:{
    width:'30%',
    justifyContent:'center',
    alignItems:'flex-start',
  },
  PointsInvestSecondView:{
    width:'50%',
    justifyContent:'center',
    alignItems:'center',
  },
  buttonViewFirst:{
    width:'20%',
    justifyContent:'center',
    alignItems:'flex-end',
  },
  UserView:{
    width:'100%',
    flexDirection:'row',
    justifyContent:'center',
    marginBottom:10
  },
  UserNameView:{
    width:'30%',
    justifyContent:'center',
    // borderWidth:1,
    
  },
  StatusView:{
    width:'40%',
    justifyContent:'center',
    alignItems:'flex-end',
    // borderWidth:1,
    
  },
  StatusViewSecond:{
    width:'90%',
    backgroundColor:Colors.ThemeBlue,
    borderRadius:5,
    height:40,
    justifyContent:'center',
    alignItems:'center'
  },
  StatusTouchView:{
    width:'50%',
    justifyContent:'center',
    alignItems:'flex-end',
    // borderWidth:1
  },
  StatusTouchViewMain:{
    width:'40%',
    justifyContent:'center',
    alignItems:'flex-end',
    flexDirection:'row',
    // borderWidth:1
  },
  UserName:{
    color:Colors.Black,
    fontSize:Constants.FontSize.medium,
  },
  StatusText:{
    color:Colors.ThemeBlue,
    fontSize:Constants.FontSize.medium,
  },
  StatusButtonText:{
    color:Colors.White,
    fontSize:Constants.FontSize.medium,
  },
  AddUserButtonView:{
    width:'30%',
    alignItems:'center',
    justifyContent:'center',
    flexDirection:'row',
    // textAlign:'right',
    // borderWidth:1,
  },
  UserTouchView:{
    width:'70%',
    alignItems:'center',
    justifyContent:'center',
    borderRadius:5,
    backgroundColor:Colors.ThemeBlue,
    height:30
  },
  ButtonView:{
    color:Colors.Black,
    fontSize:Constants.FontSize.small,
  },
  StartButtonView:{
    width:"100%",
    padding:20,
    justifyContent:'center',
    alignItems:'center',
    marginTop:20
  },
  InvestPoints:
  {
    color:Colors.Black,
    textAlign:'center',
    fontSize:Constants.FontSize.medium,
  },
  AddInputStyle: {
    height: 40,
    marginBottom: 10,
    marginTop: 10,
    color: Colors.Black,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: Colors.BlackTwo,
    paddingLeft: 15,
    width:'95%',
  },
  addButton: {
    height: 40,
    width: '100%',
    backgroundColor: Colors.ThemeBlue,
    borderRadius: 7,
    alignSelf:'center',
    justifyContent:'center',
    alignItems:'center',
    borderColor: Colors.Silver,
    borderWidth: 1,
  },
  //news
  TitleView:{
    width:'100%',
    // height:60,
    flexDirection:'column',
    borderBottomColor: Colors.BlackOne,
    // borderBottomWidth:1,
    justifyContent:'center',
    // marginBottom:20,
    // marginTop:20,
    // borderWidth:1,
    paddingLeft:20,
    marginBottom:15
  },
  TitleText:{
    fontSize:Constants.FontSize.medium,
    fontWeight:'bold'
  },
  TitleAcceptText:{
    fontSize:Constants.FontSize.tiny,
  },
  AdsView:{
    alignItems:'center',
    justifyContent:'center'
   },
   Highlight:{
     fontWeight:'bold'
   }
})