import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl, TouchableOpacity, TextInput, Image, Alert } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader, CustomButton , ValidationMessage} from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';
import _ from 'lodash';

class UsersChallenge extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={Languages.en.UsersChallengeText} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      yourPoints:0,
    }
    
  }

  componentDidMount() {
    this.didBlurSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.getChallengersList();
      }
  );
  }

  
  getChallengersList(){
    var challengeid= this.props.navigation.getParam("challengeId");
     this.props.getChallengersList(challengeid, (d)=>{this.responseChallengersList(d)});
      
  }

  responseChallengersList=(data)=>{
    // this.setState({yourPoints: this.props.ChallengersList.AdminPoints});
    this.props.requestedChallengeIds();
  }
  
  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.SearchUser);
    return true;
  };

  responseAcceptChallangeRequest =(data)=>{
    this.getChallengersList();
  }

  renderItem = ({ item }) => {
    return (
      <View style={styles.UserView}>
        <View style={styles.UserNameView}>
          <Text style={styles.UserName}>{item.Name}</Text>
        </View>
        <View style={styles.AddUserButtonView}>
            <Image source={Icons.pointIcon} />
            <Text style={styles.ButtonView}>{item.Points}</Text>
        </View>
        {
           item.IsAdmin == true ?
           <View style={styles.StatusView}>
             <Text style={[styles.StatusText , styles.Highlight]}>Created by</Text>
           </View>
        
        :
       
          item.UserId == global.UserId &&  (item.IsAccepted == false || item.IsAccepted == null) && (item.IsRejected == false || item.IsRejected == null)?
          <View style={styles.StatusTouchViewMain}>
            <TouchableOpacity style={styles.StatusTouchView} onPress={()=>this.acceptRequest(item.UserId, item.ChallangeId, item.Points)}>
              <View style={styles.StatusViewSecond}>
                  <Text style={styles.StatusButtonText}>Accept</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity style={styles.StatusTouchView} onPress={()=>this.rejectRequest(item.UserId, item.ChallangeId)}>
              <View style={styles.StatusViewSecond}>
                  <Text style={styles.StatusButtonText}>Reject</Text>
              </View>
            </TouchableOpacity>
          </View>
        :
          <View style={styles.StatusView}>
            <Text style={styles.StatusText}>{item.IsAccepted == true? "Accepted" : item.IsRejected == true ? "Rejected": "Requested"}</Text>
          </View>
        }
      </View>
    )
  }

  acceptRequest =( UserId, ChallangeId, points )=>{
    if(points == 0 || points == null || points==""){
      Alert.alert("","Please invest points first");
    }
    else{
      this.props.AcceptChallangeRequest(UserId, ChallangeId, true, (d)=>this.responseAcceptChallangeRequest(d))
    }
  }

  rejectRequest = (UserId, ChallangeId) =>{
    this.props.RejectChallangeRequest(UserId, ChallangeId, true, (d)=>this.responseRejectChallangeRequest(d))
  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  responseRejectChallangeRequest =(data)=>{
    Alert.alert("","Request rejected");
    this.props.navigation.navigate(Constants.Screen.Challenge);
  }

  refreshLevel() {
    this.getChallengersList();
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyles.emptyViewMain}>
        <Text style={[CommonStyles.emptyViewText, CommonStyles.fs15]}>{Languages.en.DataNotFound}</Text>
      </View>
    )
  }

  handleInvestPoint=()=>{
    const {yourPoints} = this.state;
    const { setChallengeError } = this.props;
    let errorMsg = false;
    if(yourPoints == '' || _.trim(yourPoints).length == 0){
            this.setState({ yourPoints: '' });
            setChallengeError(Languages.en.InvestPointsErrorMsg);
            errorMsg = true;
    }
    if (errorMsg == true) {
      return
    }
    var challengeid= this.props.navigation.getParam("challengeId");
    if(yourPoints < this.props.ChallengersList.MinimumEntryPoints)
    {
        Alert.alert("", "Minimum entry points is "+ this.props.ChallengersList.MinimumEntryPoints);
    }else
    {
      this.props.requestInvestPoints(global.UserId, challengeid, yourPoints, (d)=>this.responseInvestPoints(d));
    }
  }

  responseInvestPoints =(data)=>{
    // Alert.alert("",data.Message);
    this.getChallengersList();
  }

  phoneList() {
    let phoneArray=[];
     this.props.ChallengersList.challangesLists.map((data) => {
       if(data.IsAccepted == true)
       {
        phoneArray.push({"Phone": data.Phone, "UserId": data.UserId, "Name": data.Name});
       }
    })

  return phoneArray;
  
}
  playChallenge(){
    if(this.props.ChallengersList.AdminPoints > 0)
    {
      var challengeid= this.props.navigation.getParam("challengeId");
      this.props.SendChallengeStartNotification(challengeid, (d)=>this.responseNotificationSend(d))
    }
    else{
      Alert.alert("", "Please invest points first!");
    }
    
  }

  responseNotificationSend=(data)=>{
    if(data.Result == true){
      var challengeid= this.props.navigation.getParam("challengeId");
      this.props.SetStartingChallengeTime(challengeid, (d)=>this.responseNotificationSend1(d));
      }
  }

  responseNotificationSend1=(data)=>{
    if(data.Result == true){
      this.props.ChallengeStartingSoon();
      var challengeid= this.props.navigation.getParam("challengeId");
      this.props.navigation.navigate(Constants.Screen.ChallengeWinner, {challengeId : challengeid});
    }
  }

  render() {
    const { loading, validationMessage, clearChallengeError, ChallengersList } = this.props;
    let IsAcceptedArray=_.filter(ChallengersList.challangesLists, { IsAccepted: true });
    let IsAdminArray=_.filter(ChallengersList.challangesLists, { IsAdmin: true });
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyles.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
            <View style={validationMessage ? {paddingLeft:20, paddingBottom:0, paddingRight:20, paddingTop:20} : ''}>
            {validationMessage ? (<ValidationMessage message={validationMessage} handleTimeout={clearChallengeError} />) : (<View />)}
            </View>
            <View style={validationMessage ? styles.PointsInvestView1 : styles.PointsInvestView}>
              <View style={styles.PointsInvestFirstView}><Text style={styles.InvestPoints}>Invest Points</Text></View>
              <View style={styles.PointsInvestSecondView}>
                <TextInput
                  ref={(input) => { this.searchUser = input; }}
                  returnKeyType={'done'}
                  style={styles.AddInputStyle}
                  onChangeText={(text)=>this.setState({yourPoints :text})}
                  autoCorrect={false}
                  underlineColorAndroid="transparent"
                  placeholderTextColor={Colors.Black}
                  selectionColor={Colors.Black} 
                />
              </View>
             
              <View style={styles.buttonViewFirst}>
                <CustomButton
                    disabled={true}
                    buttonStyle={styles.addButton}
                    titleStyle={CommonStyles.buttonTitleStyle}
                    title={Languages.en.AddButtonText}
                    onPress={() => { this.handleInvestPoint() }}
                />
              </View>
              
            </View>
            <View style={styles.AdsView}>
                    <BannerView
                        placementId="892991524504160_917721055364540"
                        type="large"
                        onPress={() => console.log('click')}
                        onLoad={() => console.log('loaded')}
                        onError={err => console.log('error', err)}
                    />
            </View>
            <View style={styles.TitleView}>
                <Text style={styles.TitleText}>List of Users ready to play</Text>
                <Text style={styles.TitleAcceptText}>( Challenge will start when minimum {ChallengersList.MinimumChallangerUsers} user(s) will accept request )</Text>
            </View>
          <View style={[styles.NewsSection]}>
            <FlatList
              data={ChallengersList.challangesLists}
              keyExtractor={this.keyExtractor}
              renderItem={this.renderItem}
              showsVerticalScrollIndicator={false}
              column={1}
              ListEmptyComponent={this.emptyView}
              refreshControl={
                <RefreshControl
                  refreshing={false}
                  onRefresh={() => this.refreshLevel()}
                  automaticallyAdjustContentInsets={false}
                  colors={[Colors.ThemeBlue]}
                  progressBackgroundColor={Colors.White}
                />
              }
            />     
          </View>
          {
            IsAcceptedArray.length >= (ChallengersList.MinimumChallangerUsers +1) && IsAdminArray[0].UserId == global.UserId?
            <View style={styles.StartButtonView}>
                  <CustomButton
                      buttonStyle={CommonStyles.buttonStyleSecond}
                      titleStyle={CommonStyles.buttonTitleStyle}
                      title={Languages.en.PlayChallenge}
                      onPress={() => { this.playChallenge() }}
                  />
              </View>
              :
              <View />
            }
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
        <BannerView
              placementId="892991524504160_892998361170143"
              type="standard"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
      </View>
    );
  }
}

UsersChallenge.propTypes = {
  setChallengeError: PropTypes.func,
  clearChallengeError: PropTypes.func,
  requestInvestPoints: PropTypes.func,
  getChallengersList: PropTypes.func,
  loading: PropTypes.bool,
  ChallengersList: PropTypes.object,
  requestedChallengeIds: PropTypes.func,
  SendChallengeStartNotification: PropTypes.func,
  ChallengeStartingSoon: PropTypes.func,
  SetStartingChallengeTime: PropTypes.func,
  RejectChallangeRequest: PropTypes.func
};
export default UsersChallenge;
