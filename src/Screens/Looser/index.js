import React, { Component } from 'react';
import { FlatList, View, Text, TouchableOpacity, BackHandler, Image } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {
    AdMobBanner,
    AdMobInterstitial,
    PublisherBanner,
    AdMobRewarded,
} from 'react-native-admob';
import { BannerView } from 'react-native-fbads';

class Looser extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            headerBackground: (
                <HeaderBackground title={"Your Result"} />
            ),
            headerTintColor: Colors.White,
            headerLeft:
                <View style={CommonStyles.headerRightView}>
                    <HeaderLeft navigation={navigation} hideback={true} />
                </View>
            ,
            headerRight:
                <View style={CommonStyles.headerRightView}>
                    <HeaderRight navigation={navigation} showhome={true} />
                </View>
        }
    };
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.showInterstitial();
    }

    showInterstitial() {
        AdMobInterstitial.showAd().catch(error => console.log(error));
    }

    componentWillMount() {
        //Ads
        AdMobInterstitial.setTestDevices([AdMobInterstitial.simulatorId]);
        AdMobInterstitial.setAdUnitID('892991524504160_1010685692734742');

        AdMobInterstitial.addEventListener('adLoaded', () =>
            console.log('AdMobInterstitial adLoaded'),
        );
        AdMobInterstitial.addEventListener('adFailedToLoad', error =>
            console.warn(error),
        );
        AdMobInterstitial.addEventListener('adOpened', () =>
            console.log('AdMobInterstitial => adOpened'),
        );
        AdMobInterstitial.addEventListener('adClosed', () => {
            console.log('AdMobInterstitial => adClosed');
            //AdMobInterstitial.requestAd().catch(error => console.warn(error));
        });
        AdMobInterstitial.addEventListener('adLeftApplication', () =>
            console.log('AdMobInterstitial => adLeftApplication'),
        );

        AdMobInterstitial.requestAd().catch(error => console.log(error));

        this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    handleBackButton = () => {
        this.props.navigation.navigate(Constants.Screen.GameZone);
        return true;
    };

    render() {
        return (
            <KeyboardAwareScrollView
                contentContainerStyle={CommonStyle.flexGroWScrollView}
                showsVerticalScrollIndicator={false}>
                <View style={styles.mainView}>

                    <View style={CommonStyle.p20}>
                        <Image style={styles.looserIcon} source={Icons.looserThumb} />
                    </View>

                    {/* <View style={CommonStyle.p5}>
                        <Text style={[styles.looserText]}>looser</Text>
                    </View> */}

                    <View style={CommonStyle.p20}>
                        <Text style={styles.blueText}>Better Luck Next Time !</Text>
                    </View>

                    <View style={[CommonStyle.p20, CommonStyle.row, styles.score]}>
                        <Text style={styles.scoreNumber}> {this.props.navigation.getParam("points")} </Text>
                        <Image style={{ marginLeft: 10 }} source={Icons.pointIcon} />
                    </View>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate(Constants.Screen.GameZone)}}>
                        <View style={[CommonStyle.p20, CommonStyle.row, styles.score]}>
                            <Text style={styles.scoreNumber1}> Go to Home </Text>
                            {/* <Image style={{ marginLeft: 10 }} source={Icons.pointIcon} /> */}
                        </View>
                    </TouchableOpacity>
                    <View style={[CommonStyle.p11, styles.textValuePair]}>
                        <Text style={[styles.blueText, styles.leftText, CommonStyle.fs18]}>Result</Text>
                        <Text style={[styles.blueText, styles.RightText, CommonStyle.fs18]}>{this.props.navigation.getParam("percentage")}%</Text>
                    </View>

                    <View style={[CommonStyle.p11, styles.textValuePair]}>
                        <Text style={[styles.blueText, styles.leftText, CommonStyle.fs18]}>Time</Text>
                        <Text style={[styles.blueText, styles.RightText, CommonStyle.fs18]}> {this.props.navigation.getParam("timeTaken")}</Text>
                    </View>

                </View>
                <BannerView
                    placementId="892991524504160_892998361170143"
                    type="standard"
                    onPress={() => console.log('click')}
                    onLoad={() => console.log('loaded')}
                    onError={err => console.log('error', err)}
                />
            </KeyboardAwareScrollView>
        )
    }
}
Looser.propTypes = {
};

export default Looser;