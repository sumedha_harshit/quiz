import React, { Component } from 'react';
import { View, Text, FlatList, BackHandler, RefreshControl, TouchableOpacity, Image } from 'react-native';
import { Icons, Colors, Languages, CommonStyle, Constants, Svgs } from '@common';
import styles from './style';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackground, HeaderLeft, HeaderRight, Loader } from '@components';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import { BannerView } from 'react-native-fbads';

class WinnerDetails extends Component {
  static navigationOptions = ({ navigation }) => {
    const { params = {} } = navigation.state;

    return {
      headerBackground: (
        <HeaderBackground title={"Winner Details"} />
      ),
      headerTintColor: Colors.White,
      headerLeft:
        <View style={CommonStyles.headerRightView}>
          <HeaderLeft navigation={navigation} sidemenu={true} />
        </View>
      ,
      headerRight:
        <View style={CommonStyles.headerRightView}>
          <HeaderRight navigation={navigation} />
        </View>
    }
  };

  constructor(props) {
    super(props);
    this.state = {
    
    }
  }

  componentDidMount() {
    var userId= this.props.navigation.getParam("userId");
    var challengeId= this.props.navigation.getParam("challengeId");
    this.didBlurSubscription = this.props.navigation.addListener(
      'didFocus',
      payload => {
        this.props.WinnerDetailsRequest(userId, challengeId);
      }
  );
  }

  componentWillMount() {
    this.backHandler = BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    this.backHandler.remove();
    //  AdMobInterstitial.removeAllListeners();
  }

  handleBackButton = () => {
    this.props.navigation.navigate(Constants.Screen.ChallengeWinner);
    return true;
  };

  renderItem = ({ item }) => {
    return (
      <View style={styles.TitleView}>
              <View style={styles.ListView}>
                {
                item.IsWinner == true ?
                  <View style={styles.IconView}>
                    {Svgs.WinnerTrophy}
                  </View>
                  :
                  <View style={styles.IconView}>
                    {Svgs.ParicipantsTrophy}
                  </View>
                }
                <View style={styles.DetailsView}>
                  <Text style={styles.TitleText1}>{item.Name}</Text>
                  {/* <Text style={styles.TitleText}>{item.Phone}</Text> */}
                </View>
                <View style={styles.PointsView}>
                  <Text style={styles.TitleText}><Image source={Icons.pointIcon} style={{height:25, width:25}}/> {item.Points}</Text>
                </View>
              </View>
            </View>
    )
  }

  keyExtractor = (item, index) => {
    return (index.toString());
  }

  refreshLevel() {
    var userId= this.props.navigation.getParam("userId");
    var challengeId= this.props.navigation.getParam("challengeId");
    this.props.WinnerDetailsRequest(userId, challengeId);
  }
  //Used to show When Data is empty
  emptyView = () => {
    return (
      <View style={CommonStyle.emptyViewMain}>
        <Text style={[CommonStyle.emptyViewText, CommonStyle.fs15]}>{Languages.en.DataNotFound}</Text>
      </View>
    )
  }


  render() {
    const { loading, WinnerDetails } = this.props;
    return (
      <View style={styles.LoginMain}>
        <KeyboardAwareScrollView
          contentContainerStyle={CommonStyle.flexGroWScrollViewSec}
          showsVerticalScrollIndicator={false}>
          <View style={[styles.NewsSection]}>
          <FlatList
                  data={WinnerDetails}
                  keyExtractor={this.keyExtractor}
                  renderItem={this.renderItem}
                  showsVerticalScrollIndicator={false}
                  column={1}
                  ListEmptyComponent={this.emptyView}
                  refreshControl={
                    <RefreshControl
                      refreshing={false}
                      onRefresh={() => this.refreshLevel()}
                      automaticallyAdjustContentInsets={false}
                      colors={[Colors.ThemeBlue]}
                      progressBackgroundColor={Colors.White}
                    />
                  }
                />
            
          </View>
          <Loader modalVisible={loading} />
        </KeyboardAwareScrollView>
        <BannerView
              placementId="892991524504160_892998361170143"
              type="standard"
              onPress={() => console.log('click')}
              onLoad={() => console.log('loaded')}
              onError={err => console.log('error', err)}
          />
      </View>
    );
  }
}

WinnerDetails.propTypes = {
  WinnerDetailsRequest: PropTypes.func,
  loading: PropTypes.bool,
  WinnerDetails: PropTypes.array
};
export default WinnerDetails;
