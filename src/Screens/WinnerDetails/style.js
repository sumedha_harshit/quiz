
import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
export default StyleSheet.create({
  LoginMain: {
    width: '100%',
    height: '100%',
     flex: 1,
  },
  //news
  TitleView:{
    width:'100%',
    flexDirection:'column',
    // borderBottomColor: Colors.BlackOne,
    // borderBottomWidth:1,
    // marginBottom:10,
    justifyContent:'center',
    alignItems:'flex-start',
    // borderWidth:1,
    // borderColor: Colors.WhiteGray,
     paddingTop:15,
     paddingLeft:15,
     paddingRight:15
  },
  NewsSection:{
    //padding:20,
    // paddingBottom:20,
    
    flexDirection:'column'
  },
  TitleText:{
    fontSize:Constants.FontSize.medium,
    textAlign:'center'
  },
  TitleText1:{
    fontSize:Constants.FontSize.big,
    textAlign:'center',
    // marginBottom:10,
    color:Colors.ThemeBlue,
    fontWeight:'bold'
  },
  ListView:{
    
    width:'100%',
    flexDirection:'row',
    borderWidth:1,
    borderColor:Colors.WhiteGray
  },
  IconView:{
    width:'15%',
    justifyContent:'center',
    alignItems:'center'
  },
  DetailsView:{
    width:'65%',
    justifyContent:'center',
    alignItems:'flex-start',
    backgroundColor:"#fcfcfc",
    padding:10,
    paddingLeft:15,
  },
  PointsView:{
    paddingRight:10,
    width:'20%',
    justifyContent:'center',
    alignItems:'flex-end',
    backgroundColor:"#fcfcfc"
  }
})