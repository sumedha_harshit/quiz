import React, { Component } from 'react';
import { connect } from 'react-redux';
import {setError, clearError, requestLogin} from "../redux/actions/authActions";
import Login from '../Screens/Login';

const mapStateToProps = state => ({
    validationMessage: state.auth.error,
    loading: state.auth.loading
});

const dispatchers = {
      setError,
     clearError,
     requestLogin
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Login);
