import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setContactError, clearContactError,ContactSupportRequest ,requestGetUserConversation} from '../redux/actions';
import ContactSupport from '../Screens/ContactSupport';

/*========================================================
     * class Name: Profile pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/

     
const mapStateToProps = state => ({
    loading: state.contactSupport.loading,
    currentUser: state.auth.currentUser,
    validationMessage: state.auth.error,
    conversation:state.contactSupport.conversation,
});

const dispatchers = {
    setContactError,
    clearContactError,
    ContactSupportRequest,
    requestGetUserConversation

};

export default connect(
    mapStateToProps,
    dispatchers,
)(ContactSupport);
