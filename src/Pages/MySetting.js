import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getSettings } from '../redux/actions';
import MySetting from '../Screens/MySetting';

/*========================================================
     * class Name: Profile pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/

     
const mapStateToProps = state => ({
    loading: state.auth.loading,
    settingsData: state.auth.settingsData,
});

const dispatchers = {
    getSettings
};

export default connect(
    mapStateToProps,
    dispatchers,
)(MySetting);
