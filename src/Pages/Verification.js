import React, { Component } from 'react';
import { connect } from 'react-redux';
import Verification from '../Screens/Verification';
import { verifyOTP , clearError} from "../redux/actions/authActions";
/*========================================================
 * class Name: Profile pages
 * class Purpose: pass the props of navigation
 * class Parameters: navigation
 * class Description: pass the props of navigation in home pages
 *=====================================================*/

const mapStateToProps = state => ({
  // type: state.auth.type,
  loading: state.auth.loading,
  validationMessage: state.auth.error
});

const dispatchers = {
  verifyOTP,
  clearError
};

export default connect(mapStateToProps, dispatchers)(Verification);
