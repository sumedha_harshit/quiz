import React, { Component } from 'react';
import { connect } from 'react-redux';
import {setError, clearError, changePassword} from "../redux/actions/authActions";
import ChangePassword from '../Screens/ChangePassword';

const mapStateToProps = state => ({
    validationMessage: state.auth.error,
    loading: state.auth.loading
});

const dispatchers = {
      setError,
     clearError,
     changePassword
};

export default connect(
    mapStateToProps,
    dispatchers,
)(ChangePassword);
