import React, { Component } from 'react';
import { connect } from 'react-redux';
import {setError, clearError, sendOtpForgotPassword} from "../redux/actions/authActions";
import ForgotPassword from '../Screens/ForgotPassword';

const mapStateToProps = state => ({
    validationMessage: state.auth.error,
    loading: state.auth.loading
});

const dispatchers = {
      setError,
     clearError,
     sendOtpForgotPassword
};

export default connect(
    mapStateToProps,
    dispatchers,
)(ForgotPassword);
