import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GetSavedChallengeList, deleteSavedChallenge } from '../redux/actions/challengeAction';
import SavedChallenge from '../Screens/SavedChallenge';

const mapStateToProps = state => ({
    loading: state.challenge.loading,
    SavedChallengeList: state.challenge.SavedChallengeList,
    validationMessage: state.auth.error
});

const dispatchers = {
    GetSavedChallengeList,
    deleteSavedChallenge
};

export default connect(
    mapStateToProps,
    dispatchers,
)(SavedChallenge);
