import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GetNewsFeedDetails } from '../redux/actions/newsActions';
import NewsFeedDetails from '../Screens/NewsFeedDetails';

const mapStateToProps = state => ({
    loading: state.news.loading,
    NewsFeedDetails: state.news.NewsFeedDetails,
    validationMessage: state.auth.error,
});

const dispatchers = {
    GetNewsFeedDetails
};

export default connect(
    mapStateToProps,
    dispatchers,
)(NewsFeedDetails);
