import React, { Component } from 'react';
import { connect } from 'react-redux';
 import { setError, clearError, requestSignup, getReferralCode  } from '../redux/actions';
import SignUp from '../Screens/SignUp';

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/

     
const mapStateToProps = state => ({
    validationMessage: state.auth.error,
    loading: state.auth.loading
});

const dispatchers = {
    setError,
    clearError,
    requestSignup,
    getReferralCode
};

export default connect(
    mapStateToProps,
    dispatchers,
)(SignUp);
