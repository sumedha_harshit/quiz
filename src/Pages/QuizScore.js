import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUsersScore } from '../redux/actions/gameZoneActions';
import QuizScore from '../Screens/QuizScore';

const mapStateToProps = state => ({
    loading: state.gameZone.loading,
    AllScoresData: state.gameZone.AllScoresData,
    validationMessage: state.auth.error
});

const dispatchers = {
    getUsersScore
};

export default connect(
    mapStateToProps,
    dispatchers,
)(QuizScore);
