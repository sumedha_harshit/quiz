import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GetNewsFeed, getPaginationNewsFeed } from '../redux/actions/newsActions';
import NewsFeed from '../Screens/NewsFeed';

const mapStateToProps = state => ({
    loading: state.news.loading,
    NewsFeed: state.news.NewsFeed,
    validationMessage: state.auth.error,
    pageCount: state.news.pageCount,
    loadingMore: state.news.loadingMore,
});

const dispatchers = {
    GetNewsFeed,
    getPaginationNewsFeed
};

export default connect(
    mapStateToProps,
    dispatchers,
)(NewsFeed);
