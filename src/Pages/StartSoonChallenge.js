import React, { Component } from 'react';
import { connect } from 'react-redux';
import { ChallengeStartingSoon } from '../redux/actions/authActions';
import StartSoonChallenge from '../Screens/StartSoonChallenge';

const mapStateToProps = state => ({
    loading: state.challenge.loading,
    startingSoonChallengeList: state.auth.startingSoonChallengeList,
    validationMessage: state.auth.error
});

const dispatchers = {
    ChallengeStartingSoon
};

export default connect(
    mapStateToProps,
    dispatchers,
)(StartSoonChallenge);
