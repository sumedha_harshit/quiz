import React, { Component } from 'react';
import { connect } from 'react-redux';
import { requestSearchUser, requestAddUser, getAddedUserList, requestMinimumPointsToPlay } from '../redux/actions/challengeAction';
import SearchUser from '../Screens/SearchUser';

const mapStateToProps = state => ({
    loading: state.challenge.loading,
    UserList: state.challenge.UserList,
    validationMessage: state.auth.error,
    AddedUserList: state.challenge.AddedUserList,
    currentUser: state.auth.currentUser,
});

const dispatchers = {
    requestSearchUser,
    requestAddUser,
    getAddedUserList,
    requestMinimumPointsToPlay
};

export default connect(
    mapStateToProps,
    dispatchers,
)(SearchUser);
