import React, { Component } from 'react';
import { connect } from 'react-redux';
import {SavePasscode} from "../redux/actions";
import Passcode from '../Screens/Passcode';

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({
    loading: state.auth.loading
});

const dispatchers = {
    SavePasscode
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Passcode);
