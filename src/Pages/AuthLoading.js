import React, { Component } from 'react';
import { connect } from 'react-redux';
import {  logout, setCurrentUser, updateFCMtoken } from '../redux/actions';
import AuthLoadingScreen from '../Screens/AuthLoading';

/*========================================================
    * function Name: AuthLoading Screen
    * function Purpose: called action
    * function Parameters: setCurrentUser, dispatchers
    * function ReturnType: method
    * function Description: called action of dispatchers and stored state
    *=====================================================*/
const mapStateToProps = state => ({
    currentUser: state.auth.currentUser
});

const dispatchers = {
    setCurrentUser,
    updateFCMtoken,
    //setupUser,
    logout,
};

export default connect(
    null,
    dispatchers,
)(AuthLoadingScreen);
