import React, { Component } from 'react';
import { connect } from 'react-redux';
import { NewsListRequest } from '../redux/actions/newsActions';
import News from '../Screens/News';

const mapStateToProps = state => ({
    loading: state.news.loading,
    NewsList: state.news.NewsList,
    validationMessage: state.auth.error
});

const dispatchers = {
    NewsListRequest
};

export default connect(
    mapStateToProps,
    dispatchers,
)(News);
