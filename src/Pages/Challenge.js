import React, { Component } from 'react';
import { connect } from 'react-redux';
import { requestStartChallenge, winnerUserList } from '../redux/actions/challengeAction';
import Challenge from '../Screens/Challenge';

const mapStateToProps = state => ({
    loading: state.challenge.loading,
    validationMessage: state.auth.error,
    currentUser: state.auth.currentUser,
    WinnerList: state.challenge.WinnerList
});

const dispatchers = {
    requestStartChallenge,
    winnerUserList
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Challenge);
