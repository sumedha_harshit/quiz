import React, { Component } from 'react';
import { connect } from 'react-redux';
import { confirmPayment, clearWithdrawError, redeemPoints,  } from "../redux/actions";
import finishTransfer from '../Screens/FinishTransfer';

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({
    loading: state.withdraw.loading,
    validationMessage: state.auth.error,
});

const dispatchers = {
    confirmPayment,
    clearWithdrawError,
    redeemPoints
};

export default connect(
    mapStateToProps,
    dispatchers,
)(finishTransfer);
