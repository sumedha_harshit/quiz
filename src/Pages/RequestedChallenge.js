import React, { Component } from 'react';
import { connect } from 'react-redux';
import { GetRequestedChallengeList } from '../redux/actions/challengeAction';
import RequestedChallenge from '../Screens/RequestedChallenge';

const mapStateToProps = state => ({
    loading: state.challenge.loading,
    RequestedChallengeList: state.challenge.RequestedChallengeList,
    validationMessage: state.auth.error
});

const dispatchers = {
    GetRequestedChallengeList
};

export default connect(
    mapStateToProps,
    dispatchers,
)(RequestedChallenge);
