import React, { Component } from 'react';
import { connect } from 'react-redux';
import Looser from '../Screens/Looser';

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({
});

const dispatchers = {
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Looser);
