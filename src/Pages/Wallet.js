import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getWalletData, clearWalletError, setWalletError } from '../redux/actions';
import Wallet from '../Screens/Wallet';

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({
    WalletData: state.wallet.WalletData,
    loading: state.wallet.loading,
    validationMessage: state.wallet.error
});

const dispatchers = {
    getWalletData,
    setWalletError,
    clearWalletError
    // redeemPoints
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Wallet);
