import React, { Component } from 'react';
import { connect } from 'react-redux';
import { challengeResult } from '../redux/actions/challengeAction';
import ChallengeWinner from '../Screens/ChallengeWinner';

const mapStateToProps = state => ({
    loading: state.challenge.loading,
    validationMessage: state.auth.error
});

const dispatchers = {
    challengeResult
};

export default connect(
    mapStateToProps,
    dispatchers,
)(ChallengeWinner);
