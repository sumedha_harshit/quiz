import React, { Component } from 'react';
import { connect } from 'react-redux';
import { WinnerDetailsRequest } from '../redux/actions/challengeAction';
import WinnerDetails from '../Screens/WinnerDetails';

const mapStateToProps = state => ({
    loading: state.challenge.loading,
    WinnerDetails: state.challenge.WinnerDetails,
    validationMessage: state.auth.error
});

const dispatchers = {
    WinnerDetailsRequest
};

export default connect(
    mapStateToProps,
    dispatchers,
)(WinnerDetails);
