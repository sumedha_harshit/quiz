import React, { Component } from 'react';
import { connect } from 'react-redux';
import SuccessPayment from '../Screens/SuccessPayment';
// import { setError, clearError, getAllQuiz } from "../redux/actions/gameZoneActions";

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({

});

const dispatchers = {
};

export default connect(
    mapStateToProps,
    dispatchers,
)(SuccessPayment);
