import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setChallengeError, RejectChallangeRequest, SendChallengeStartNotification, SetStartingChallengeTime, clearChallengeError, requestInvestPoints, getChallengersList, AcceptChallangeRequest } from '../redux/actions/challengeAction';
import { requestedChallengeIds, ChallengeStartingSoon } from '../redux/actions/authActions';
import UsersChallenge from '../Screens/UsersChallenge';

const mapStateToProps = state => ({
    loading: state.challenge.loading,
    ChallengersList: state.challenge.ChallengersList,
    validationMessage: state.auth.error
});

const dispatchers = {
    setChallengeError,
    clearChallengeError,
    requestInvestPoints,
    getChallengersList,
    AcceptChallangeRequest,
    requestedChallengeIds,
    SendChallengeStartNotification,
    ChallengeStartingSoon,
    SetStartingChallengeTime,
    RejectChallangeRequest
};

export default connect(
    mapStateToProps,
    dispatchers,
)(UsersChallenge);
