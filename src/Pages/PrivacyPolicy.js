import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { dashsetError, requestDashboardData,requestGetOrderIdDashboard, requestSaveOrderRazorPayDashboard  } from '../redux/actions';
import PrivacyPolicy from '../Screens/PrivacyPolicy';

/*========================================================
     * class Name: Profile pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/

     
const mapStateToProps = state => ({
});

const dispatchers = {

};

export default connect(
    mapStateToProps,
    dispatchers,
)(PrivacyPolicy);
