import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { dashsetError, requestDashboardData,requestGetOrderIdDashboard, requestSaveOrderRazorPayDashboard  } from '../redux/actions';
import Home from '../Screens/Home';

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/

     
const mapStateToProps = state => ({
    // currentUser: state.auth.currentUser,
    // loading: state.auth.loading,
    // dashboardData: state.home.dashboardData,
    // dashboardBannerData: state.home.dashboardBannerData,
    //dashboardProductTypeDetail: state.home.dashboardProductTypeDetail,
});

const dispatchers = {
    // dashsetError,
    // requestDashboardData,
    // requestGetOrderIdDashboard,
    // requestSaveOrderRazorPayDashboard
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Home);
