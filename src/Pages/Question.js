import React, { Component } from 'react';
import { connect } from 'react-redux';
import Question from '../Screens/Question';
import { setError, clearError, getAllQuizQuestions, requestSubmitQuiz, nextQuestion } from "../redux/actions/getQuizQuestionActions";

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({
    questionsData: state.quizQuestion.questionsData,
    loading: state.quizQuestion.loading,
    type: state.quizQuestion.type,
    questionIndex: state.quizQuestion.questionIndex,
    answeredDataAll: state.quizQuestion.answeredDataAll,
});

const dispatchers = {
    setError,
    clearError,
    getAllQuizQuestions,
    requestSubmitQuiz,
    nextQuestion
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Question);
