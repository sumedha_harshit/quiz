import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAactiveUser } from '../redux/actions';
import ActiveUsers from '../Screens/ActiveUsers';

/*========================================================
     * class Name: Profile pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/

     
const mapStateToProps = state => ({
    loading: state.network.loading,
    ActiveUserData: state.network.ActiveUserData,
});

const dispatchers = {
    getAactiveUser
};

export default connect(
    mapStateToProps,
    dispatchers,
)(ActiveUsers);
