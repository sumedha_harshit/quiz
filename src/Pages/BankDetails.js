import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setWithdrawError, clearWithdrawError } from "../redux/actions";
import BankDetails from '../Screens/BankDetails';

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({
    validationMessage: state.auth.error
});

const dispatchers = {
    setWithdrawError,
    clearWithdrawError
};

export default connect(
    mapStateToProps,
    dispatchers,
)(BankDetails);
