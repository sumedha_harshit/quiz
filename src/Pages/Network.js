import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getAllLevel } from '../redux/actions';
import Network from '../Screens/Network';

const mapStateToProps = state => ({
    loading: state.network.loading,
    AllLevelData: state.network.AllLevelData,
    validationMessage: state.auth.validationMessage
});

const dispatchers = {
    getAllLevel
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Network);
