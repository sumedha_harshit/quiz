import React, { Component } from 'react';
import { connect } from 'react-redux';
import GameZone from '../Screens/GameZone';
import { setError, clearError, getAllQuiz } from "../redux/actions/gameZoneActions";
import { challengeRequest, ChallengeStartingSoon } from "../redux/actions/authActions";
import { getAllQuizQuestions, getPlayerId } from "../redux/actions/getQuizQuestionActions";

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({
    quizData: state.gameZone.quizData,
    loading: state.gameZone.loading,
    playerId: state.quizQuestion.playerId,
    typeGetQuestion: state.quizQuestion.type,
    type: state.gameZone.type,
    questionsData: state.quizQuestion.questionsData,
    currentUser: state.auth.currentUser,
    requestedChallengeIds: state.auth.requestedChallengeIds,
    startingSoonChallengeList: state.auth.startingSoonChallengeList
});

const dispatchers = {
    setError,
    clearError,
    getAllQuiz,
    getPlayerId,
    getAllQuizQuestions,
    challengeRequest,
    ChallengeStartingSoon
};

export default connect(
    mapStateToProps,
    dispatchers,
)(GameZone);
