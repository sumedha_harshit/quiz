import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setWithdrawError, clearWithdrawError, getBankDetails } from "../redux/actions";
import Withdraw from '../Screens/Withdraw';

/*========================================================
     * class Name: Home pages
     * class Purpose: pass the props of navigation
     * class Parameters: navigation
     * class Description: pass the props of navigation in home pages
     *=====================================================*/


const mapStateToProps = state => ({
    validationMessage: state.auth.error,
    BankDetails: state.withdraw.BankDetails,
});

const dispatchers = {
    setWithdrawError,
    clearWithdrawError,
    getBankDetails
};

export default connect(
    mapStateToProps,
    dispatchers,
)(Withdraw);
