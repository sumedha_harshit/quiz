import { apiRequest } from './index';
import * as URL from './WebApiList';
import * as types from './types';
import { Global } from '@common';
const ciphertoken = Global.default.ciphertoken;

export function incrementChallengeLoading() {
    return { type: types.CHANGE_LOADING, payload: true };
}

export function decrementChallengeLoading() {
    return { type: types.CHANGE_LOADING, payload: false };
}

export function setChallengeError(payload) {
    return { type: types.SET_ERROR, payload };
}

export function clearChallengeError() {
    return { type: types.CLEAR_ERROR, payload: '' };
}

export function setUserList(payload) {
    return { type: types.SET_USERS_LIST, payload: payload };
}

export function setChallengersList(payload) {
    return { type: types.SET_CHALLENGERS_LIST, payload: payload };
}

export function setWinnerList(payload) {
    return { type: types.SET_WINNER_LIST, payload: payload };
}

export function setAddedUserList(payload) {
    return { type: types.SET_ADDED_USER_LIST, payload: payload };
}

export function setWinnerDetails(payload) {
    return { type: types.SET_WINNER_DETAILS, payload: payload };
}

export function setSavedChallengeList(payload) {
    return { type: types.SET_SAVED_CHALLENGE, payload: payload };
}

export function setRequestedChallengeList(payload) {
    return { type: types.SET_REQUESTED_CHALLENGE, payload: payload };
}


export function requestStartChallenge( UserID, Name, Phone, callback ) {
    return apiRequest({
        url: URL.START_CHALLENGE,
        method: 'POST',
        body: { UserID, Name, Phone },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload);
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Search user
export function requestSearchUser( UserID, Search, ChallangeId, callback ) {
    return apiRequest({
        url: URL.SEARCH_USER,
        method: 'POST',
        body: { UserID, Search, ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    //callback(payload);
                    dispatch(setUserList(payload));
                    callback(payload);
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Add user
export function requestAddUser( UserID, Name, Phone, ChallangeId, callback ) {
    return apiRequest({
        url: URL.ADD_USERS_CHALLENGE,
        method: 'POST',
        body: { UserID, Name, Phone, ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    // dispatch(setAddedUserList(payload));
                    callback(payload);

                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Invest points
export function requestInvestPoints( UserID, ChallangeId, Points, callback ) {
    return apiRequest({
        url: URL.INVEST_POINTS,
        method: 'POST',
        body: { UserID, ChallangeId, Points },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload);
                    //dispatch(setUserList(payload));
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//challengers list
export function getChallengersList( ChallangeId, callback ) {
    return apiRequest({
        url: URL.GET_CHALLENGERS_LIST,
        method: 'POST',
        body: { ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            //  alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(setChallengersList(payload));
                    callback(payload);
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}


//ChallengeResult
export function challengeResult( UserId, ChallangeId, callback ) {
    return apiRequest({
        url: URL.SET_CHALLENGE_RESULT,
        method: 'POST',
        body: { UserId, ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload);
                    // dispatch(setChallengersList(payload));
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//winner list
export function winnerUserList() {
    return apiRequest({
        url: URL.WINNER_USER_LIST,
        method: 'POST',
        body: { },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                     dispatch(setWinnerList(payload));
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Winner Details
export function WinnerDetailsRequest(UserId, ChallangeId) {
    return apiRequest({
        url: URL.WINNER_USER_DETAILS,
        method: 'POST',
        body: { UserId, ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                     dispatch(setWinnerDetails(payload));
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Saved Challenge list
export function GetSavedChallengeList() {
    return apiRequest({
        url: URL.GET_SAVED_CHALLENGE,
        method: 'POST',
        body: { UserId: global.UserId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                     dispatch(setSavedChallengeList(payload));
                } else {
                    dispatch(setSavedChallengeList(payload));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Requested Challenge
export function GetRequestedChallengeList() {
    return apiRequest({
        url: URL.GET_REQUESTED_CHALLENGE,
        method: 'POST',
        body: { UserId: global.UserId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                     dispatch(setRequestedChallengeList(payload));
                } else {
                    dispatch(setRequestedChallengeList(payload));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Accept Challenge
export function AcceptChallangeRequest(UserId, ChallangeId, IsAccepted, callback) {
    return apiRequest({
        url: URL.ACCEPT_CHALLENGE,
        method: 'POST',
        body: { UserId, ChallangeId, IsAccepted },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                     //dispatch(setRequestedChallengeList(payload));
                     callback(payload);
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}


//Reject request
export function RejectChallangeRequest(UserId, ChallangeId, IsRejected,  callback) {
    return apiRequest({
        url: URL.REJECT_CHALLENGE,
        method: 'POST',
        body: { UserId, ChallangeId, IsRejected },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                     //dispatch(setRequestedChallengeList(payload));
                     callback(payload);
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Added user list
export function getAddedUserList(ChallangeId) {
    return apiRequest({
        url: URL.GET_ADDED_USER_LIST,
        method: 'POST',
        body: { ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                     dispatch(setAddedUserList(payload));
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//Delete Challenge

export function deleteSavedChallenge(ChallangeId, callback) {
    return apiRequest({
        url: URL.DELETE_SAVED_CHALLENGE,
        method: 'POST',
        body: { ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload);
                } else {

                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//minimum points to play challenge
export function requestMinimumPointsToPlay(UserID, ChallangeId, Points, callback) {
    return apiRequest({
        url: URL.ADD_MINIMUM_POINTS,
        method: 'POST',
        body: { UserID, ChallangeId, Points },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload);
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//send notification
export function SendChallengeStartNotification(ChallangeId, callback) {
    return apiRequest({
        url: URL.SEND_CHALLENGE_START_NOTIFICATION,
        method: 'POST',
        body: { ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload);
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}

//update challenge start datetime
export function SetStartingChallengeTime(ChallangeId, callback) {
    // alert("SetStartingChallengeTime"+ChallangeId);
    return apiRequest({
        url: URL.SET_STARTING_CHALLENGE_TIME,
        method: 'POST',
        body: { ChallangeId },
        onLoadStart: incrementChallengeLoading,
        onLoadEnd: decrementChallengeLoading,
        onSuccess: payload => {
            //  alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload);
                } else {
                    dispatch(setChallengeError(payload.Message));
                }
            }
        },
        onError: error => setChallengeError(error.Message),
    });
}
