import { apiRequest } from './index';
import * as URL from './WebApiList';
import * as types from './types';
import { Global } from '@common';
const ciphertoken = Global.default.ciphertoken;

export function incrementNewsLoading() {
    return { type: types.CHANGE_LOADING, payload: true };
}

export function decrementNewsLoading() {
    return { type: types.CHANGE_LOADING, payload: false };
}

export function setNewsError(payload) {
    return { type: types.SET_ERROR, payload };
}

export function clearNewsError() {
    return { type: types.CLEAR_ERROR, payload: '' };
}

export function getNewsList(payload) {
    return { type: types.SET_NEWS_LIST, payload: payload };
}

export function getNewsFeedData(payload, url, page) {
    // alert(JSON.stringify(payload));
    return { type: types.SET_NEWS_FEED_DATA, payload: payload, pageCount: page, pageUrl:url};
}

export function getNewsFeedDetailsData(payload) {
  return { type: types.SET_NEWS_FEED_DETAILS_LIST, payload: payload };
}

export function NewsListRequest( callback ) {
    return apiRequest({
        url: URL.GET_NEWS_LIST,
        method: 'POST',
        body: { },
        onLoadStart: incrementNewsLoading,
        onLoadEnd: decrementNewsLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload.Data);
                    dispatch(getNewsList(payload.Data));
                } else {
                    dispatch(setNewsError(payload.Message));
                }
            }
        },
        onError: error => setNewsError(error.Message),
    });
}

export function GetNewsFeed( Url,PageNo ) {
    return apiRequest({
        url: URL.GET_NEWS_FEED,
        method: 'POST',
        body: { Url, PageNo},
        onLoadStart: incrementNewsLoading,
        onLoadEnd: decrementNewsLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(getNewsFeedData(payload, Url, PageNo));
                } else {
                    dispatch(setNewsError(payload.Message));
                }
            }
        },
        onError: error => setNewsError(error.Message),
    });
}


//GetNewsFeedDetails
export function GetNewsFeedDetails( Url,Title ) {
  return apiRequest({
      url: URL.GET_NEWS_FEED_DETAILS,
      method: 'POST',
      body: { Url, Title},
      onLoadStart: incrementNewsLoading,
      onLoadEnd: decrementNewsLoading,
      onSuccess: payload => {
          // alert(JSON.stringify(payload));
          return dispatch => {
              if (payload.Result == true) {
                  dispatch(getNewsFeedDetailsData(payload));
              } else {
                  dispatch(setNewsError(payload.Message));
              }
          }
      },
      onError: error => setNewsError(error.Message),
  });
}


/*  Pagination api section */

export function incrementNewsFeedMoreLoading() {
    return { type: types.NEWS_FEED_LOADING_MORE, payload: true };
  }
  
  export function decrementNewsFeedMoreLoading() {
    return { type: types.NEWS_FEED_LOADING_MORE, payload: false };
  }
  
  export function paginationNewsFeedSuccess(payload, page) {
    return { type: types.NEWS_FEED_PAGINATION_SUCCESS, payload: payload, pageCount: page };
  }

  export function NewsFeedPagination(Url, PageNo) {
    return apiRequest({
      url: URL.GET_NEWS_FEED,
      method: 'POST',
      body: { Url, PageNo },
      onLoadStart: incrementNewsFeedMoreLoading,
      onLoadEnd: decrementNewsFeedMoreLoading,
      onSuccess: payload => {
        return (dispatch, getState) => {
          const state = getState();
        //   if (resetData) {
        //     state.news.stockData = [];
        //     const maxPages = Math.ceil(payload.Data.Count / 10);
        //     state.stockCheck.MaxPage = maxPages;
        //   }
          dispatch(paginationNewsFeedSuccess(payload, PageNo));
        }
      },
      onError: error => setNewsError(error.Message),
    });
  }
  

export function getPaginationNewsFeed(isRefresh) {
    return (dispatch, getState) => {
      const state = getState();
      if (isRefresh) {
        dispatch(
            NewsFeedPagination(state.news.pageUrl, 1)
        );
      } else {
        if (state.news.pageCount && !state.news.loadingMore ) {
          dispatch(
            NewsFeedPagination(state.news.pageUrl, state.news.pageCount +1)
          );
        }
      }
    };
  };
