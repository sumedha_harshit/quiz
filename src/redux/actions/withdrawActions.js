import { apiRequest } from './index';
import * as URL from '../actions/WebApiList';
import * as types from './types';
import { Global } from '@common';
const ciphertoken = Global.default.ciphertoken;

export function incrementWithdrawLoading() {
    return { type: types.CHANGE_LOADING, payload: true };
}

export function decrementWithdrawLoading() {
    return { type: types.CHANGE_LOADING, payload: false };
}

export function setWithdrawError(payload) {
    return { type: types.SET_ERROR, payload };
}

export function clearWithdrawError() {
    return { type: types.CLEAR_ERROR, payload: '' };
}

export function getBankDetailsSuccess(payload) {
    return { type: types.GET_BANK_DETAILS, payload};
}

export function getBankDetails(callback) {
    // console.log("ciphertoken : " + ciphertoken);
    return apiRequest({
        url: URL.GET_BANK_DETAILS,
        method: 'POST',
        body: { UserID: global.UserId, ciphertoken: ciphertoken },
        onLoadStart: incrementWithdrawLoading,
        onLoadEnd: decrementWithdrawLoading,
        onSuccess: payload => {
            return dispatch => {
                console.log(JSON.stringify(payload));
                if (payload.Result == true) {
                    dispatch(getBankDetailsSuccess({ ...payload }));
                    callback(payload);
                } else {
                    dispatch(setWithdrawError(payload.Message));
                }
            }
        },
        onError: error => setWithdrawError(error.Message),
    });
}

//confirm paascode
export function confirmPayment(data, passcode, callback) {
    // alert(JSON.stringify(data));
    let postData={};
    if(data.withdrawMethod == "Bank")
    {
        postData ={
            UserID: global.UserId,
            AccountNumber: data.accountNo,
            NameInAccount: data.accountHolderName,
            Bank: data.bankName,
            IFSCCode: data.ifscCode,
            amount: data.withdrawalAmount,
            WithdrawType: data.withdrawMethod,
            Passcode: passcode,
            ciphertoken: ciphertoken
        }
    }
    else{
       postData ={
            UserID: global.UserId,
            amount: data.withdrawalAmount,
            WithdrawType: data.withdrawMethod,
            Passcode: passcode,
            ciphertoken: ciphertoken
        }
    }
    return apiRequest({
        url: URL.CONFIRM_PAYMENT,
        method: 'POST',
        body: postData,
        onLoadStart: incrementWithdrawLoading,
        onLoadEnd: decrementWithdrawLoading,
        onSuccess: payload => {
            return dispatch => {
                //if (payload.Result == true) {
                    // callback(payload);
                ///} else {
                    dispatch(setWithdrawError(payload.Message));
                    callback(payload);
                //}
            }
        },
        onError: error => setWithdrawError(error.Message),
    });
}


//points redeem
export function redeemPoints(points, passcode, callback) {
    return apiRequest({
        url: URL.REDEEM_POINTS,
        method: 'POST',
        body: {UserID: global.UserId, PointsWithdraw: points, Passcode : passcode, ciphertoken: ciphertoken},
        onLoadStart: incrementWithdrawLoading,
        onLoadEnd: decrementWithdrawLoading,
        onSuccess: payload => {
            return dispatch => {
                if (payload.Result == true) {
                    //dispatch(getAllWalletSuccess({ ...payload }));
                    
                    callback(payload);
                } else {
                    dispatch(setWithdrawError(payload.Message));
                }
            }
        },
        onError: error => setWithdrawError(error.Message),
    });
}