
import { AsyncStorage, Alert } from 'react-native';
import { apiRequest } from './index';
import * as URL from '../actions/WebApiList';
import * as types from './types';
import { Constants, Global } from '@common';

/*========================================================
     * function Name: authAction.js 
     * function Purpose: method of action
     * function Parameters: url, method, body, onLoadStart, onLoadEnd, onSuccess, onError using api calling
     * function ReturnType: onLoadStart, onLoadEnd, onSuccess, onError
     * function Description: api calling using method of action in authAction.js
     *=====================================================*/

export function dashincrementLoading() {
  return { type: types.CHANGE_LOADING, payload: true };
}

export function dashdecrementLoading() {
  return { type: types.CHANGE_LOADING, payload: false };
}

export function dashsetError(payload) {
  return { type: types.SET_ERROR, payload };
}

export function clearDashError() {
  return { type: types.CLEAR_ERROR, payload: '' };
}

// export function setDashboardData(payload) {
//     return { type: types.DASHBOARD_DATA, payload };
//   }

// export function setDashboardBannerData(payload) {
//   return { type: types.DASHBOARD_BANNER_DATA, payload };
// }

// export function setDashboardProductTypeData(payload) {
//   return { type: types.DASHBOARD_PRODUCT_DATA, payload };
// }
// export function setSalesProducttypeWiseData(payload) {
//   return { type: types.SALESPRODUCTTYPEWISE_DATA, payload };
// }
// export function setSalesProducttypeWiseError(payload) {
//   return { type: types.SALESPRODUCTTYPEWISE_ERROR, payload };
// }

/*****Dashboard *********/
// export function requestDashboardData(UserId, Role) {
//   //alert(Role+" call api");
//   return apiRequest({
//     url: URL.DASHBOARD_URL,
//     method: 'POST',
//     body: { UserId, Role },
//     onLoadStart: dashincrementLoading,
//     onLoadEnd: dashdecrementLoading,
//     onSuccess: payload => {
//       return dispatch => {
//         const user = payload;
//         if (user.ResultFlag == 1) {
//             dispatch(setDashboardData({ ...payload.Data }));
//             dispatch(setDashboardBannerData(payload));
//             //dispatch(setDashboardProductTypeData(payload));
//         } else {

//           dispatch(dashsetError(user.Message));
//         }
//       }
//     },
//     onError: error => dashsetError(error.Message),
//   });
// }
/*****SalesProducttypeWise *********/
// export function SalesProducttypeWise(StartDate, EndDate, GroupId, Frequency, Growth, Role, UserId){
//   return apiRequest({
//     url: URL.DASHBOARDPRODUCTTYPEDETAILS_URL,
//     method: 'POST',
//     body: { IsFromMobile: true, StartDate: StartDate, EndDate: EndDate, GroupId: GroupId, Frequency: Frequency, Growth: Growth, Role: Role, UserId: UserId },
//     onLoadStart: dashincrementLoading,
//     onLoadEnd: dashdecrementLoading,
//     onSuccess: payload => {
//       return dispatch => {
//         const user = payload;
//         if (user.ResultFlag == 1) {
//           //alert(JSON.stringify(payload));
//           dispatch(setSalesProducttypeWiseData(payload));
//         } else {
//           //dispatch(setSalesProducttypeWiseError(payload));
//           dispatch(dashsetError(user.Message));
//           //Alert.alert("",user.Message);
//         }
//       }
//     },
//     onError: error => dashsetError(error.Message),
//   });
// }

/*****Get Product Group (Category) *********/
// export function AllProductGroup(callback) {
//   return apiRequest({
//     url: URL.ALL_PRODUCT_GROUP,
//     method: 'POST',
//     onLoadStart: dashincrementLoading,
//     onLoadEnd: dashdecrementLoading,
//     onSuccess: payload => {
//       return dispatch => {
//         callback(payload);
//       }
//     },
//     onError: error => dashsetError(error.Message),
//   });
// }