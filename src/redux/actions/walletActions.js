import { apiRequest } from './index';
import * as URL from '../actions/WebApiList';
import * as types from './types';
import { Global } from '@common';
const ciphertoken = Global.default.ciphertoken;

export function incrementWalletLoading() {
    return { type: types.CHANGE_LOADING, payload: true };
}

export function decrementWalletLoading() {
    return { type: types.CHANGE_LOADING, payload: false };
}

export function setWalletError(payload) {
    return { type: types.SET_ERROR, payload };
}

export function clearWalletError() {
    return { type: types.CLEAR_ERROR, payload: '' };
}

export function getAllWalletSuccess(payload) {
    return { type: types.GET_ALL_WALLET_SUCCESS, payload };
}

export function getWalletData(UserId) {
    return apiRequest({
        url: URL.GET_WALLET_DATA,
        method: 'POST',
        body: {UserId, ciphertoken},
        onLoadStart: incrementWalletLoading,
        onLoadEnd: decrementWalletLoading,
        onSuccess: payload => {
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(getAllWalletSuccess({ ...payload }));
                } else {
                    dispatch(setWalletError(payload.Message));
                }
            }
        },
        onError: error => setWalletError(error.Message),
    });
}



