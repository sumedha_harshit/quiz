
import { Alert } from 'react-native';
import { apiRequest } from './index';
import * as URL from '../actions/WebApiList';
import * as types from './types';
import { Constants, Global } from '@common';
import AsyncStorage from '@react-native-community/async-storage';
import EventEmitter from "react-native-eventemitter";
const ciphertoken = Global.default.ciphertoken;

/*========================================================
     * function Name: authAction.js 
     * function Purpose: method of action
     * function Parameters: url, method, body, onLoadStart, onLoadEnd, onSuccess, onError using api calling
     * function ReturnType: onLoadStart, onLoadEnd, onSuccess, onError
     * function Description: api calling using method of action in authAction.js
     *=====================================================*/

export function authIrementLoading() {
  return { type: types.CHANGE_LOADING, payload: true };
}

export function authDecrementLoading() {
  return { type: types.CHANGE_LOADING, payload: false };
}

export function setError(payload) {
  return { type: types.SET_ERROR, payload };
}

export function clearError() {
  return { type: types.CLEAR_ERROR, payload: '' };
}

export function setChallengeRequest(payload) {
  return { type: types.SET_CHALLENGE_REQUEST, payload: payload };
}

export function getSettingSucces(payload) {
  return { type: types.GET_SETTING_SUCCESS, payload };
}

export function loginSuccess(payload) {
  global.AccessToken = payload.access_token;
  global.UserId = payload.id;
  global.ReferralCode = payload.RefferalCode;
  global.RefreshToken = payload.refresh_token;
  return { type: types.LOGIN_SUCCESS, payload }
}

export function setCurrentUser(payload) {
  global.RefreshToken = payload.refresh_token;
  global.AccessToken = payload.access_token;
  global.UserId = payload.id;
  global.ReferralCode = payload.RefferalCode;
  return { type: types.LOGIN_SUCCESS, payload }
}

export function setStartingSoonChallengeList(payload) {
  return { type: types.SET_STARTING_SOON_CHALLENGE, payload: payload}
}
/*****Login *********/
export function requestLogin(phonenumber, password, FCMToken, callback) {
  return apiRequest({
    url: URL.LOGIN_URL,
    method: 'POST',
    body: { phonenumber, password, ciphertoken },
    onLoadStart: authIrementLoading,
    onLoadEnd: authDecrementLoading,
    onSuccess: payload => {
      return dispatch => {
        if (payload.result == true) {
          AsyncStorage.setItem(Constants.Preferences.currentUser, JSON.stringify(payload));
          dispatch(loginSuccess({ ...payload, password }));
          dispatch(updateFCMtoken(FCMToken, callback));
          // callback();
        } else {
          dispatch(setError(payload.error_message));
        }
      }
    },
    onError: error => setError(error.Error),
    
  });
}

/*****Register*********/
export function requestSignup(PhoneNumber, Password, Name, Email, referralcode, DeviceID, callback) {
  return apiRequest({
    url: URL.REGISTER_URL,
    method: 'POST',
    body: {
      Email: '', Name: Name, PhoneNumber: PhoneNumber, Password: Password, UsedReferalCode: referralcode,
      DeviceID: DeviceID, Platform: '', NotificationKey: '', IP: '',  ciphertoken: ciphertoken
    },
    onLoadStart: authIrementLoading,
    onLoadEnd: authDecrementLoading,
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        if (user.result == true) {
          callback(user);
        } else {
          dispatch(setError(user.error_message));
        }
      }
    },
    onError: error => setError(error.error_message),
  });
}


// Verify user with OTP ////
export function verifyOTP(phone, OTP, action, callback) {
  let url = "";
  if (action == "signup") {
    url = URL.VERIFY_SIGNUP_OTP;
  }
  else {
    url = URL.VERIFY_SIGNUP_OTP;
  }
  return apiRequest({
    url: url,
    method: 'POST',
    body: {
      PhoneNumber: phone,
      OTP: OTP,
      ciphertoken: ciphertoken
    },
    onLoadStart: authIrementLoading,
    onLoadEnd: authDecrementLoading,
    onSuccess: payload => {
      return dispatch => {
        if (payload.Result == true) {
          // dispatch(verifyOTPSuccess())
          callback();
        } else {
          dispatch(setError(payload.Message));
        }
      }
    },
    onError: error => setError(error.Message),
  });
}

//logout
export function logout(){
  AsyncStorage.setItem(Constants.Preferences.currentUser,'');  
  EventEmitter.emit(Constants.EventEmitterName.onLogout);
}

export function sendOtpForgotPassword(PhoneNumber, callback){
  return apiRequest({
    url: URL.FORGOT_PASSWORD_SEND_OTP,
    method: 'POST',
    body: { PhoneNumber, ciphertoken },
    onLoadStart: authIrementLoading,
    onLoadEnd: authDecrementLoading,
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        if (user.Result == true) {
          callback(user);
        } else {
          dispatch(setError(user.Message));
        }
      }
    },
    onError: error => setError(error.Message),
  });
}

export function changePassword(PhoneNumber, NewPassword, callback){
  return apiRequest({
    url: URL.CHANGE_PASSWORD,
    method: 'POST',
    body: { PhoneNumber, NewPassword, ciphertoken },
    onLoadStart: authIrementLoading,
    onLoadEnd: authDecrementLoading,
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        if (user.Result == true) {
          callback(user);
        } else {
          dispatch(setError(user.Message));
        }
      }
    },
    onError: error => setError(error.Message),
  });
}

//Save passcode
export function SavePasscode(UserId, Passcode, callback){
  return apiRequest({
    url: URL.SAVE_PASSCODE,
    method: 'POST',
    body: { UserId, Passcode, ciphertoken },
    onLoadStart: authIrementLoading,
    onLoadEnd: authDecrementLoading,
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        if (user.Result == true) {
          // dispatch(passcodeSuccess())
          callback();
        } else {
          dispatch(setError(user.Message));
        }
      }
    },
    onError: error => setError(error.Message),
  });
}

//User settings
export function getSettings(){
  return apiRequest({
    url: URL.GET_SETTING_URL,
    method: 'POST',
    body: {UserID: global.UserId, ciphertoken: ciphertoken },
    onLoadStart: authIrementLoading,
    onLoadEnd: authDecrementLoading,
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        if (user.Result == true) {
           dispatch(getSettingSucces({...payload}))
        } else {
          dispatch(setError(user.Message));
        }
      }
    },
    onError: error => setError(error.Message),
  });
}


//reAuthentication
export function reAuthentication() {
  return apiRequest({
    url: URL.RE_AUTHENTICATION_URL,
    method: 'POST',
    body: { RefreshToken: global.RefreshToken, UserId: global.UserId, ciphertoken: ciphertoken },
    onLoadStart: authIrementLoading,
    onLoadEnd: authDecrementLoading,
    onSuccess: payload => {
      return dispatch => {
        if (payload.result == true) {
          AsyncStorage.setItem(Constants.Preferences.currentUser, JSON.stringify(payload));
          dispatch(loginSuccess({ ...payload }));
          callback();
        } else {
          AsyncStorage.setItem(Constants.Preferences.currentUser,'');  
          EventEmitter.emit(Constants.EventEmitterName.onLogout);
          //dispatch(setError(payload.error_message));
        }
      }
    },
    onError: error => setError(error),
  });
}

//User challenge request
export function challengeRequest(){
  return apiRequest({
    url: URL.GET_CHALLENGE_REQUEST,
    method: 'POST',
    body: {UserID: global.UserId },
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        //if (user.Result == true) {
           dispatch(setChallengeRequest(payload))
           dispatch(ChallengeStartingSoon());
        // } else {
        //   // dispatch(setError(user.Message));
        //   dispatch(setChallengeRequest(payload))
        // }
      }
    },
    onError: error => setError(error.Message),
  });
}

//User FCM Token update
export function updateFCMtoken(FCMToken, callback){
  // alert(FCMToken);
  return apiRequest({
    url: URL.UPDATE_FCM_TOKEN,
    method: 'POST',
    body: {UserID: global.UserId , FCMToken: FCMToken, ciphertoken: ciphertoken},
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        if (user.Result == true) {
          // alert("hello");
          callback(payload);
        } else {
          // dispatch(setError(user.Message));
        }
      }
    },
    onError: error => setError(error.Message),
  });
}

//challange starting soon list
export function ChallengeStartingSoon(){
  return apiRequest({
    url: URL.CHALLENGE_STARTING_SOON,
    method: 'POST',
    body: {UserId: global.UserId},
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        // alert(JSON.stringify(payload));
        
          dispatch(setStartingSoonChallengeList(payload))
         
      }
    },
    onError: error => setError(error.Message),
  });
}

//get referral code

export function getReferralCode(ipAddress, callback){
  //  alert(ipAddress);
  return apiRequest({
    url: URL.GET_REFERRAL_CODE+"?ipAddress="+ipAddress,
    method: 'GET',
    onSuccess: payload => {
      return dispatch => {
        const user = payload;
        if (user.Result == true) {
          callback(payload);
        } 
      }
    },
    onError: error => setError(error.Message),
  });
}