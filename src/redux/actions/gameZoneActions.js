import { apiRequest } from './index';
import * as URL from '../actions/WebApiList';
import * as types from './types';
import { Global } from '@common';
const ciphertoken = Global.default.ciphertoken;

export function irementLoading() {
    return { type: types.CHANGE_LOADING, payload: true };
}

export function decrementLoading() {
    return { type: types.CHANGE_LOADING, payload: false };
}

export function setError(payload) {
    return { type: types.SET_ERROR, payload };
}

export function clearError() {
    return { type: types.CLEAR_ERROR, payload: '' };
}

export function getAllQuizSuccess(payload) {
    return { type: types.GET_ALL_QUIZ_SUCCESS, payload };
}

export function getAllQuizScoreSuccess(payload) {
    return { type: types.GET_ALL_QUIZ_SCORE_SUCCESS, payload };
}

export function getAllQuiz(callback) {
    // alert(ciphertoken)
    return apiRequest({
        url: URL.GET_ALL_QUIZ,
        method: 'POST',
        body: { UserId : global.UserId, ciphertoken: ciphertoken },
        onLoadStart: irementLoading,
        onLoadEnd: decrementLoading,
        onSuccess: payload => {
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(getAllQuizSuccess({ ...payload }));
                    callback(payload);
                } else {
                    dispatch(setError(payload.Message));
                }
            }
        },
        onError: error => setError(error.Message),
        // onError: error => setError(''),
    });
}


export function getUsersScore(QuizId, UserId) {
    return apiRequest({
        url: URL.GET_QUIZ_SCORE,
        method: 'POST',
        body: { QuizId, UserId, ciphertoken },
        onLoadStart: irementLoading,
        onLoadEnd: decrementLoading,
        onSuccess: payload => {
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(getAllQuizScoreSuccess({ ...payload }));
                } else {
                    dispatch(setError(payload.Message));
                }
            }
        },
        onError: error => setError(error.Message),
    });
}

