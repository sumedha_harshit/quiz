import { apiRequest } from './index';
import * as URL from '../actions/WebApiList';
import * as types from './types';
import { useCallback } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from '../../app/common/Constants';
import { Global } from '@common';
const ciphertoken = Global.default.ciphertoken;

export function irementLoading() {
    return { type: types.CHANGE_LOADING, payload: true };
}

export function decrementLoading() {
    return { type: types.CHANGE_LOADING, payload: false };
}

export function setError(payload) {
    return { type: types.SET_ERROR, payload };
}

export function clearError() {
    return { type: types.CLEAR_ERROR, payload: '' };
}

export function getAllQuizQuestionsSuccess(payload) {
    return { type: types.GET_QUIZ_QUESTIONS_SUCCESS, payload };
}

export function getPlayerIdSuccess(payload) {
    return { type: types.GET_PLAYER_ID_SUCCESS, payload };
}

export function setNextQuestion(index, payload) {
    return { type: types.GET_NEXT_QUESTION_INDEX, index , payload};
}

export function getAllQuizQuestions(quizId, callback) {
    return apiRequest({
        url: URL.GET_QUIZ_QUESTIONS,
        method: 'POST',
        body: {
            QuizId: quizId,
            UserId: global.UserId,
            ciphertoken: ciphertoken
        },
        onLoadStart: irementLoading,
        onLoadEnd: decrementLoading,
        onSuccess: payload => {
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(getAllQuizQuestionsSuccess({ ...payload }));
                    dispatch(getPlayerId(quizId, callback))
                } else {
                    dispatch(setError(payload.Message));
                    callback(payload);
                }
            }
        },
        onError: error => setError(error.Message),
    });
}

export function getPlayerId(quizId, callback) {
    return apiRequest({
        url: URL.GET_PLAYER_ID,
        method: 'POST',
        body: {
            QuizId: quizId,
            UserId: global.UserId,
            ciphertoken: ciphertoken
        },
        onLoadStart: irementLoading,
        onLoadEnd: decrementLoading,
        onSuccess: payload => {
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(getPlayerIdSuccess({ ...payload }));
                    callback(payload);
                } else {
                    dispatch(setError(payload.Message));
                }
            }
        },
        onError: error => setError(error.Message),
    });
}

export function requestSubmitQuiz(userid, quizId, playedDate, UserAnswer, callback, playerId) {
    return apiRequest({
        url: URL.FINISH_QUIZ_URL,
        method: 'POST',
        body: {
            QuizID: quizId,
            UserId: userid,
            PlayedDate: playedDate,
            UserAnswer: UserAnswer,
            PlayerId: playerId,
            ciphertoken: ciphertoken
        },
        onLoadStart: irementLoading,
        onLoadEnd: decrementLoading,
        onSuccess: payload => {
            return dispatch => {
                if (payload.Result == true) {
                    AsyncStorage.setItem(Constants.Preferences.TodayPlayedQuiz, parseInt(Constants.Preferences.TodayPlayedQuiz) + 1);
                    callback(payload.Data);
                } else {
                    dispatch(setError(payload.Message));
                }
            }
        },
        onError: error => setError(error.Message),
    });
}

export function nextQuestion(index, payload){
    // alert(JSON.stringify(payload));
    return dispatch => {
        dispatch(setNextQuestion(index, payload));
    }
}