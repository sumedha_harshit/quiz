import { apiRequest } from './index';
import * as URL from '../actions/WebApiList';
import * as types from './types';
import { Global } from '@common';
const ciphertoken = Global.default.ciphertoken;

export function incrementNetworkLoading() {
    return { type: types.CHANGE_LOADING, payload: true };
}

export function decrementNetworkLoading() {
    return { type: types.CHANGE_LOADING, payload: false };
}

export function setNetworkError(payload) {
    return { type: types.SET_ERROR, payload };
}

export function clearNetworkError() {
    return { type: types.CLEAR_ERROR, payload: '' };
}

export function getAllLevelSuccess(payload) {
    return { type: types.GET_ALL_LEVEL_SUCCESS, payload };
}

export function getActiveUserSuccess(payload) {
    return { type: types.GET_ACTIVE_USER_SUCCESS, payload };
}

export function getAllLevel( UserID ) {
    return apiRequest({
        url: URL.GET_ALL_LEVEL,
        method: 'POST',
        body: { UserID, ciphertoken },
        onLoadStart: incrementNetworkLoading,
        onLoadEnd: decrementNetworkLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(getAllLevelSuccess({ ...payload }));
                } else {
                    dispatch(setNetworkError(payload.Message));
                }
            }
        },
        onError: error => setNetworkError(error.Message),
    });
}

export function getAactiveUser( UserId, Level ) {
    return apiRequest({
        url: URL.GET_ACTIVE_USER,
        method: 'POST',
        body: { UserId , Level, ciphertoken},
        onLoadStart: incrementNetworkLoading,
        onLoadEnd: decrementNetworkLoading,
        onSuccess: payload => {
            return dispatch => {
                if (payload.Result == true) {
                    dispatch(getActiveUserSuccess({ ...payload }));
                }  else {
                    dispatch(setNetworkError(payload.Message));
                }
            }
        },
        onError: error => setNetworkError(error.Message),
    });
}