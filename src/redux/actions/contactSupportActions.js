import { apiRequest } from './index';
import * as URL from '../actions/WebApiList';
import * as types from './types';
import { Global } from '@common';
const ciphertoken = Global.default.ciphertoken;

export function incrementContactLoading() {
    return { type: types.CHANGE_LOADING, payload: true };
}

export function decrementContactLoading() {
    return { type: types.CHANGE_LOADING, payload: false };
}

export function setContactError(payload) {
    return { type: types.SET_ERROR, payload };
}

export function clearContactError() {
    return { type: types.CLEAR_ERROR, payload: '' };
}

export function setUserConversation(payload){
    return {type:types.GET_USER_CONVERSATION,payload};
}

export function ContactSupportRequest( UserId, Mobile, UserMessage, callback ) {
    return apiRequest({
        url: URL.SAVE_CONTACT_SUPPORT,
        method: 'POST',
        body: { UserId, Mobile, UserMessage, ciphertoken },
        onLoadStart: incrementContactLoading,
        onLoadEnd: decrementContactLoading,
        onSuccess: payload => {
            // alert(JSON.stringify(payload));
            return dispatch => {
                if (payload.Result == true) {
                    callback(payload);
                } else {
                    dispatch(setContactError(payload.Message));
                }
            }
        },
        onError: error => setContactError(error.Message),
    });
}
//****************************************************************************
export function requestGetUserConversation(mobile,callback)
{
    return apiRequest({
        url:URL.GET_USER_CONVERSATION+"?mobile="+mobile,
        method:'GET',
        onLoadStart:incrementContactLoading,
        onLoadEnd:decrementContactLoading,
        onSuccess:payload=>{
            return dispatch=>{
                
                if(payload.Data != null)
                {
                    dispatch(setUserConversation(payload));
                    callback();
                }
                else{
                    dispatch(setContactError(payload.Message));
                }
            }
        },
        onError: error => setContactError(error.Message),
    });
}
