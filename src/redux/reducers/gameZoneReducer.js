import * as types from '../actions/types';
import { addErrorMessage, updateLoading } from './commonReducer';

const INITIAL_STATE = {
    AuthLoginState: false,
    loading: false,
    error: '',
    type: '',
    quizData: {},
    AllScoresData:[]
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return updateLoading(state, action)
        case types.SET_ERROR:
            return addErrorMessage(state, action);
        case types.CLEAR_ERROR:
            return addErrorMessage(state, action);
        case types.GET_ALL_QUIZ_SUCCESS:
            return { ...state, type: types.GET_ALL_QUIZ_SUCCESS, quizData: action.payload.Data }
        case types.GET_ALL_QUIZ_SCORE_SUCCESS:
            return { ...state, type: types.GET_ALL_QUIZ_SCORE_SUCCESS, AllScoresData: action.payload.Data }
        default:
            return state;
    }
};

