import * as types from '../actions/types';
import { addErrorMessage, updateLoading } from './commonReducer';

const INITIAL_STATE = {
    AuthLoginState: false,
    loading: false,
    error: '',
    type: '',
    questionsData: {},
    playerId: 0,
    questionIndex:0,
    answeredDataAll: [],
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return updateLoading(state, action)
        case types.SET_ERROR:
            return addErrorMessage(state, action);
        case types.CLEAR_ERROR:
            return addErrorMessage(state, action);
        case types.GET_QUIZ_QUESTIONS_SUCCESS:
            return { ...state, type: types.GET_QUIZ_QUESTIONS_SUCCESS, questionsData: action.payload.Data }
        case types.GET_PLAYER_ID_SUCCESS:
            return { ...state, type: types.GET_PLAYER_ID_SUCCESS, playerId: action.payload.Data }
        case types.GET_NEXT_QUESTION_INDEX:
            return {...state, questionIndex: index, answeredDataAll: payload}
        default:
            return state;
    }
};

