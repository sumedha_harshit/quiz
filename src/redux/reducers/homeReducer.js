
import * as types from '../actions/types';
// import { updateLoading, addErrorMessage } from './commonReducer';

/*========================================================
     * function Name: authReducer.js 
     * function Purpose: state management
     * function Parameters: state and action
     * function ReturnType: action type and payload
     * function Description: api calling response action type and set payload of state stored in authReducer.js
     *=====================================================*/

const INITIAL_STATE = {
    AuthLoginState: false,
    loading: false,
    // currentUser: {},
    // userid: '',
     error: '',
    // dashboardData: {},
    // dashboardBannerData: [],
    // //dashboardProductTypeDetail: {},
    // SalesProducttypewisedata: [],
    // SalesProducttypewiseerror:''

};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return {...state, error:'change loading'};
        case types.SET_ERROR:
            return {...state, error:'error'};
        // case types.DASHBOARD_DATA:
        //     return { ...state, dashboardData: action.payload };
        // case types.DASHBOARD_BANNER_DATA:
        //     return { ...state, dashboardBannerData: action.payload.Data.Banners };
        // // case types.DASHBOARD_PRODUCT_DATA:
        // //     return { ...state, dashboardProductTypeDetail: action.payload.Data.ProductTypeDetail };
        // case types.SALESPRODUCTTYPEWISE_DATA:
        //     return { ...state, SalesProducttypewisedata: action.payload.Data };
        // case types.SALESPRODUCTTYPEWISE_ERROR:
        //     return { ...state, SalesProducttypewiseerror: action.payload.Message };
        default:
            return state;
    }
};

