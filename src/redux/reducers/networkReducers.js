import * as types from '../actions/types';
import { addErrorMessage, updateLoading } from './commonReducer';

const INITIAL_STATE = {
    AuthLoginState: false,
    loading: false,
    error: '',
    AllLevelData: [],
    ActiveUserData: []
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return updateLoading(state, action)
        case types.SET_ERROR:
            return addErrorMessage(state, action);
        case types.CLEAR_ERROR:
            return addErrorMessage(state, action);
        case types.GET_ALL_LEVEL_SUCCESS:
            return { ...state, AllLevelData: action.payload.Data }
        case types.GET_ACTIVE_USER_SUCCESS:
            return { ...state, ActiveUserData: action.payload.Data }
        default:
            return state;
    }
};

