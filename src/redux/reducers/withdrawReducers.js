import * as types from '../actions/types';
import { addErrorMessage, updateLoading } from './commonReducer';

const INITIAL_STATE = {
    AuthLoginState: false,
    loading: false,
    error: '',
    type: '',
    BankDetails:{}
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return updateLoading(state, action)
        case types.SET_ERROR:
            return addErrorMessage(state, action);
        case types.CLEAR_ERROR:
            return addErrorMessage(state, action);
        case types.GET_BANK_DETAILS:
            return { ...state, BankDetails: action.payload.Data }
        default:
            return state;
    }
};

