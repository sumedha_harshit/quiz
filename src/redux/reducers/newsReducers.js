import * as types from '../actions/types';
import { addErrorMessage, updateLoading } from './commonReducer';

const INITIAL_STATE = {
    AuthLoginState: false,
    loading: false,
    error: '',
    NewsList:[],
    NewsFeed:[],
    pageCount:'',
    pageUrl:'',
    loadingMore:false,
    type: '',
    NewsFeedDetails:{}
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return updateLoading(state, action)
        case types.SET_ERROR:
            return addErrorMessage(state, action);
        case types.CLEAR_ERROR:
            return addErrorMessage(state, action);
        case types.SET_NEWS_LIST:
            return {...state, NewsList: action.payload};
        case types.SET_NEWS_FEED_DETAILS_LIST:
            return {...state, NewsFeedDetails: action.payload.Data};
        case types.SET_NEWS_FEED_DATA:
            return {...state, NewsFeed: action.payload.Data, pageCount: action.pageCount, pageUrl: action.pageUrl};
        case types.NEWS_FEED_PAGINATION_SUCCESS:
            return {
                ...state, NewsFeed: [
                    ...state.NewsFeed,
                    ...action.payload.Data],
                pageCount: action.pageCount,
                type: types.NEWS_FEED_PAGINATION_SUCCESS
            }
        default:
            return state;
    }
};

