import * as types from '../actions/types';
import { addErrorMessage, updateLoading } from './commonReducer';

const INITIAL_STATE = {
    AuthLoginState: false,
    loading: false,
    error: '',
    UserList:[],
    ChallengersList:{},
    WinnerList:[],
    AddedUserList:{},
    WinnerDetails:[],
    SavedChallengeList:[],
    RequestedChallengeList:[]
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return updateLoading(state, action)
        case types.SET_ERROR:
            return addErrorMessage(state, action);
        case types.CLEAR_ERROR:
            return addErrorMessage(state, action);
        case types.SET_USERS_LIST:
            return {...state, UserList: action.payload.Data};
        case types.SET_CHALLENGERS_LIST:
            return {...state, ChallengersList: action.payload.Data};
        case types.SET_WINNER_LIST:
            return {...state, WinnerList: action.payload.Data};
        case types.SET_ADDED_USER_LIST:
            return {...state, AddedUserList: action.payload.Data};
        case types.SET_WINNER_DETAILS:
            return {...state, WinnerDetails: action.payload.Data};
        case types.SET_SAVED_CHALLENGE:
            return {...state, SavedChallengeList: action.payload.Data};
        case types.SET_REQUESTED_CHALLENGE:
            return {...state, RequestedChallengeList: action.payload.Data};
        default:
            return state;
    }
};

