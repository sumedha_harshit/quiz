
import * as types from '../actions/types';
import { addErrorMessage, updateLoading } from './commonReducer';

/*========================================================
     * function Name: authReducer.js 
     * function Purpose: state management
     * function Parameters: state and action
     * function ReturnType: action type and payload
     * function Description: api calling response action type and set payload of state stored in authReducer.js
     *=====================================================*/

const INITIAL_STATE = {
    AuthLoginState: false,
    loading: false,
    error: '',
    type:'',
    currentUser: {},
    settingsData:{},
    requestedChallengeIds:[],
    startingSoonChallengeList:[]
};

export default function reducer(state = INITIAL_STATE, action) {
    switch (action.type) {
        case types.CHANGE_LOADING:
            return updateLoading(state, action)
        case types.SET_ERROR:
            return addErrorMessage(state, action);
        case types.CLEAR_ERROR:
            return addErrorMessage(state, action);
        case types.LOGIN_SUCCESS:
            return { ...state, currentUser: action.payload };
        case types.GET_SETTING_SUCCESS:
           return { ...state, settingsData: action.payload.Data };
        case types.SET_CHALLENGE_REQUEST:
            return { ...state, requestedChallengeIds: action.payload.Data };
        case types.SET_STARTING_SOON_CHALLENGE:
            return { ...state, startingSoonChallengeList: action.payload.Data };
        default:
            return state;
    }
};

