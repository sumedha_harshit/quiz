import * as types from '../actions/types';
import { combineReducers } from 'redux';
import homeReducer from './homeReducer';
import authReducer from './authReducer';
import gameZoneReducer from './gameZoneReducer';
import getQuizQuestionReducer from './getQuizQuestionReducer';
import walletReducers from './walletReducers';
import networkReducers from './networkReducers';
import withdrawReducers from './withdrawReducers';
import contactSupportReducers from './contactSupportReducer';
import newsReducers from './newsReducers';
import challengeReducers from './challengeReducer';

const appReducer = combineReducers({
  home: homeReducer,
  auth: authReducer,
  gameZone: gameZoneReducer,
  quizQuestion: getQuizQuestionReducer,
  wallet: walletReducers,
  network: networkReducers,
  withdraw: withdrawReducers,
  contactSupport: contactSupportReducers,
  news : newsReducers,
  challenge : challengeReducers,
});

const initialState = appReducer({}, {});

const rootReducer = (state, action) => {
  // if (action.type === types.LOGOUT) {
  //   state = initialState
  // }
  return appReducer(state, action)
}

export default rootReducer;