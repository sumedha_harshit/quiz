import { Linking, Dimensions, Alert, PermissionsAndroid, AsyncStorage } from 'react-native';
import { Languages, Constants } from '@common';
// import Moment from 'moment';

// export const isNotEmpty = (str) => {
//     return (str && str.length > 0) ? true : false
// }

// return string value for aspectratio of device wise
export const deviceSize = () => {
    const { height, width } = Dimensions.get('window');
    const aspectRatio = height / width;
    if (aspectRatio > 1.6) {
        return 'medium'
    }
    else {
        return 'large';
    }
};

// // return convert object to array
// export const convertToArray = (myObject) => {
//     let nArray = [];
//     Object.keys(myObject).map((key, index) => {
//         const myItem = myObject[key]
//         nArray.push(myItem);
//     })
//     return nArray;
// }

// // return date and time format
// export const dateTimeFormat = (date) => {
//     return Moment(date).format('DD/MM/YYYY H:mm');
// }

// export const onSendEmailViaEmailApp = (toMailId, subject, body) => {
//     if (toMailId != null) {
//         let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
//         if (!toMailId || !reg.test(toMailId)) {
//             Alert.alert(Languages.en.InValidEmail);
//         }
//         else {
//             let link = `mailto:${toMailId}`;
//             if (subject != null) {
//                 link = `${link}?subject=${subject}`;
//             }
//             if (subject != null) {
//                 link = `${link}&body=${body}`;
//             } else {
//                 if (body != null) {
//                     link = `${link}?body=${body}`;
//                 }
//             }

//             Linking.canOpenURL(link)
//                 .then(supported => {
//                     if (!supported) {
//                         Alert.alert(Languages.en.InValidEmail);
//                     } else {
//                         Linking.openURL(link);
//                     }
//                 })
//                 .catch(err => console.error('An error occurred', err));
//         }
//     }
//     else {
//         console.log('sendEmailViaEmailApp -----> ', 'mail link is undefined');
//     }
// };

// export const onPhoneCall = (phone) => {
//     let phoneNumber = phone.replace(/\D/g, '');
//     if (phoneNumber.length > 0) {
//         phoneNumber = `tel:${phoneNumber}`;

//         Linking.canOpenURL(phoneNumber)
//             .then(supported => {
//                 if (!supported) {
//                     Alert.alert(Languages.en.InValidPhone);
//                 } else {
//                     return Linking.openURL(phoneNumber);
//                 }
//             })
//             .catch(err => console.log(err));
//     }
//     else {
//         Alert.alert(Languages.en.InValidPhone);
//     }
// }


// export function setStorage(name, data) {
//     AsyncStorage.setItem(name, JSON.stringify(data));
// }

// export async function getTempOrderId() {
//     const dataItem = await AsyncStorage.getItem(Constants.Preferences.TempOrderId);
//     if (dataItem != null) {
//         return parseInt(dataItem);
//     } else {
//         return 0;
//     }
// }

// export async function RecentSearch() {
//     var arrayrecent=[];
//     arrayrecent = await AsyncStorage.getItem(Constants.Preferences.RecentSearch);
//     if (arrayrecent != null) {
//         return arrayrecent;
//     } else {
//         return "no recent search";
//     }
// }