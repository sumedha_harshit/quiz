import CryptoJS from "crypto-js";
import moment from 'moment';
import DeviceInfo from 'react-native-device-info';
let deviceID = DeviceInfo.getUniqueId();
var datetime = moment()
      .utcOffset('+00:00')
      .format('MM-DD-YYYY HH:mm:ss');
var data_string = "encrypt-"+ datetime +"-decrypt-"+deviceID;
console.log("CryptoJS current date : "+data_string);
const Global = {
        ciphertoken: CryptoJS.AES.encrypt(data_string, 'secret_key_encrypt').toString(),
}
console.log("CryptoJS : "+ CryptoJS.AES.encrypt(data_string, 'secret_key_encrypt').toString());
export default Global;