import { StyleSheet, Platform } from 'react-native';
import Colors from './Colors';
import Constants from './Constants';
import { widthPercentageToDP } from 'react-native-responsive-screen';

const styles = StyleSheet.create({

  /******** Common Styles **********/
  mainView: {
    flex: 1
  },
  headerTitle: {
    fontSize: Constants.FontSize.extraLarge,
    color: Colors.White,

    //textAlign:'center'
  },
  headersection: {
    width: "100%",
    justifyContent: 'center',
    alignItems: 'center'
  },
  headerRightView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  heightStyle: {
    height: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  },
  headerRightIconStyle: {
    
    marginRight: 20,
    alignItems:'center',
    justifyContent:'center'

  },

  // cartPosition:{
  //   position:'absolute'
  // },
  flexGroWScrollView: {
    flexGrow: 1,
    justifyContent:'center',
    //height:'100%',
  },
  flexGroWScrollViewSec:{
    flexGrow: 1,
  },
  inputStyle: {
    height: 45,
    marginBottom: 8,
    color: Colors.Black,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: Colors.BlackTwo,
    paddingLeft: 15
  },
  searchInputStyle: {
    height: 50,
    marginBottom: 8,
    color: Colors.Black,
    borderRadius: 7,
    borderWidth: 1,
    borderColor: Colors.BlackTwo,
    width:'90%',
    paddingLeft:20,
    paddingTop:10
  },
  inputTextareaStyle: {
     marginBottom: 8,
    // color: Colors.Black,
    // borderRadius: 10,
    // borderWidth: 1,
    // // borderColor: Colors.BlackTwo,
    // paddingLeft: 15,
    height: 200,
    
    // justifyContent: "flex-start",
    textAlignVertical: "top"
  },
  buttonStyle: {
    height: 50,
    width: '100%',
    backgroundColor: Colors.ThemeBlue,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.Silver,
    borderWidth: 1,
  },
  buttonStyleSecond: {
    height: 50,
    width: '80%',
    backgroundColor: Colors.ThemeBlue,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.Silver,
    borderWidth: 1,
  },
  buttonStyleNew: {
    height: 50,
    width: '100%',
    backgroundColor: Colors.ThemePurple,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',    
  },
  disabledButtonStyle: {
    height: 50,
    width: '100%',
    backgroundColor: Colors.WhiteGray,
    borderRadius: 30,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: Colors.Silver,
    borderWidth: 1,
  },
  buttonTitleStyle: {
    color: Colors.White,
    fontSize: Constants.FontSize.medium,
  },
  row: {
    flexDirection: 'row'
  },
  spacedFlex: {
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  whiteText: {
    color: "#fff"
  },
  w100: {
    width: '100%'
  },
  NoInternetView:{
    alignItems:'center',
    justifyContent:'center',
    height:'100%',
    paddingLeft:30,
    paddingRight:30,
  },
  NoDataHeading:{
    paddingTop:10,
    paddingBottom:10,
    textAlign:'center'
  },
  //font
  fs11: {
    fontSize: 11
  },
  fs12: {
    fontSize: 12
  },
  fs13: {
    fontSize: 13
  },
  fs14: {
    fontSize: 14
  },
  fs15: {
    fontSize: 15
  },
  fs16: {
    fontSize: 16
  },
  fs18: {
    fontSize: 18
  },
  fs19: {
    fontSize: 19
  },
  fs20: {
    fontSize: 21
  },
  fs21: {
    fontSize: 21
  },
  //padding
  p1: {
    padding: 1
  },
  p2: {
    padding: 2
  },
  p3: {
    padding: 3
  },
  p4: {
    padding: 4
  },
  p5: {
    padding: 5
  },
  p6: {
    padding: 6
  },
  p8: {
    padding: 8
  },
  p9: {
    padding: 9
  },
  p10: {
    padding: 10
  },
  p11: {
    padding: 11
  },
  p12: {
    padding: 12
  },
  p15: {
    padding: 15
  },
  p17: {
    padding: 17
  },
  p18: {
    padding: 18
  },
  p20: {
    padding: 20
  },
  emptyViewMain:{
    width:'100%',
    padding:10
  },
  emptyViewText:{
    color:Colors.BlackOne
  },
  menuGradientView:{
    height:'100%',
    width:'100%',
  },
  menuMainView:{
    paddingTop:10,
    width:'100%',
  },
  menuView:{
    flexDirection:'row',
    width:'100%',
    paddingBottom:10,
    paddingTop:10,
    paddingLeft:20,
    borderBottomColor:Colors.WhiteGray,
    borderBottomWidth:0.3,
    alignItems:'flex-start',
    // justifyContent:'center'
  },
  menuTextView:{
    width:'80%',
    // justifyContent:'center',
    flexDirection:'row',
    // alignItems:'flex-start'
  },
  menuText:{
    color:Colors.White,
    fontSize:Constants.FontSize.medium,
    textAlignVertical:'center'
  },
  headerLeftIconStyleBack:{
    height: 18,
    width: 18,
    marginLeft: 10,
    marginTop: -10,
  },
  /******** modal style********/
  modalView: {
    flexDirection: 'column',
    backgroundColor: Colors.White,
    
    paddingLeft : 25,
    paddingRight : 25,
    paddingTop : 20,
    paddingBottom : 25,
  },
  modalCloseViewTimer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 7,
    textAlign:'right'
  },
  modalCloseIcon: {
    height: 20,
    width: 20,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  modalHeaderTitleTimer: {
    fontSize: Constants.FontSize.extraLarge,  
    color : Colors.Black,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  modalSubViewTimer: {
    marginTop:20,
    // borderWidth:1
  },
  buttonStyleTimer: {
    height: 40,
    borderRadius: 30,
    backgroundColor: Colors.ThemeBlue,
    justifyContent: 'center',
    alignItems: 'center',
    width : "100%"
  },
  
  buttonTitleStyle: {
    color: Colors.White,
    fontSize: Constants.FontSize.medium,
  },
  modalHeaderTitleTimer: {
    fontSize: Constants.FontSize.extraLarge,
    paddingBottom: 10,
    color : Colors.Black,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalDescription:{
    fontSize: Constants.FontSize.medium,
    paddingBottom: 10,  
    color : Colors.Black,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  modalHeaderViewTimer:{
    width:"100%",
    flexDirection:"row"
  },
  headingModal:{
    width:"91%",
    flexDirection:"row",
    alignItems:'flex-start',
    
  },
  closeIconModal:{
    width:"7%",
    flexDirection:"row",
    alignItems:'flex-start',
    textAlign:'right',
   
  },
  ModalContentView:{
    marginTop:25,
    marginBottom:20
  },
  InputView:{
    width:"100%",
    flexDirection:'column',
     marginBottom:20,
     marginTop:25,
  },
  userDetails:{
    width:"100%",
    flexDirection:'column',
    borderBottomWidth:1,
    borderBottomColor: Colors.WhiteGray,
    padding:15,
  },
  userName:{
    fontSize: Constants.FontSize.big,
    color: Colors.White,
    fontWeight:'bold'
  },
  userMobilenumber:{
    fontSize: Constants.FontSize.medium,
    color: Colors.White
  },
  NotificatinCount:{
    width:20,
    height:20,
    backgroundColor:Colors.Green,
    borderRadius:20,
    color:Colors.White,
    textAlign:'center',
    marginLeft:15
  },
  NotificationCount:{
    backgroundColor:Colors.White,
    borderRadius:20,
    height:18,
    width:18,
    // padding:2,
    fontSize:12,
    textAlign:'center'
  },
  NotificationCountView:{
    position:'absolute',
    zIndex:999,
    // borderWidth:1,
    top:-8,
    justifyContent:'flex-end',
    alignItems:'flex-end',
    alignSelf:'flex-end',
    right:10
  },
  headerRightBellIconStyle: {
    marginRight: 20,
    alignItems:'center',
    justifyContent:'center',
    // width:50,
    // height:50,
    // borderWidth:1,
  },
  heightBellStyle: {
    height: '100%',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  NotificationText:{
    fontSize:12,
    color:Colors.White,
    paddingRight:5,
    marginTop:10
  },
  NotificationViewModal:{
    width:'95%',
    alignItems:'flex-start',
    justifyContent:'center',
    marginTop:10,
    // height:50,
    alignSelf:'center',
    flexDirection:'row',
    paddingLeft:10,
    paddingRight:10,
  },
  ModalSubHeaderText:{
    color:Colors.Black,
    fontSize:22,
     textAlign:'left',
    lineHeight:30,
    // borderWidth:1,
    paddingLeft:5
  },
  ModalSubHeaderBellIcon:{
    alignItems:'center',
    justifyContent:'center',
    lineHeight:30
  },
  modalButtonView:{
    width:'100%',
    flexDirection: 'row',
    padding:10,
    alignItems:'center',
    justifyContent:'center',
    marginTop:30
  },
  buttonStyleProfileBrowse:{
    width:"45%",
    padding:10,
    marginLeft:10,
    backgroundColor:Colors.ThemeBlue,
    alignItems:'center',
    justifyContent:'center',
    height:50,
    // borderWidth:1
  },
  ProfilebuttonTitleStyle:{
    color:Colors.White,
    fontSize: Constants.FontSize.medium,
    textAlign:'center'
  }
});

export default styles;
