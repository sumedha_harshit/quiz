import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome';
import colors from './Colors';

module.exports = {
    userIcon : <Icon name="user" size={25} color={colors.Black} />,
    inventoryIcon : <Icon name="cubes" size={25} color={colors.White} />,
    bookmarkIcon : <Icon name="bookmark" size={25} color={colors.White} />,
    fileIcon : <Icon name="file" size={25} color={colors.White} />,
    logoutIcon : <Icon name="sign-out" size={25} color={colors.Red} />,
    homeIcon : <Icon name="home" size={22} color={colors.White} />,
    infocircle: <Icon name="info-circle" size={20} />,
    starIcon: <Icon name="star" size={25} color={colors.Yellow}/>,
    starIconBlack: <Icon name="star" size={25} color={colors.GrayDark} />

}