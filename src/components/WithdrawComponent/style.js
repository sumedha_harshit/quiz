import { StyleSheet } from 'react-native'
import { Colors, Constants } from '@common'
export default StyleSheet.create({
//component
ListView:{
    padding:10,
    borderWidth:1,
    borderColor:Colors.WhiteGray,
    flexDirection:'column',
    width:'100%',
    marginBottom:10
  },
  ImageView:{
    width:'10%',
    // alignItems:'center',
    justifyContent:'center'
  },
  ImageIcon:{
    height:25, width:25
  },
  ListContentView:{
    width:'80%',
    alignItems:'flex-start',
    justifyContent:'center',
  },
  EarningText:{
    color:Colors.BlackOne,
    fontWeight:'bold'
  },
  AmountView:{
    width:'100%',
    //alignItems:'center',
    flexDirection:'column',
    justifyContent:'flex-start'
  },
  AmountText:{
    textAlign:'right',
    color:Colors.ThemeBlue,
    fontWeight:'bold',

  },
  CheckboxView:
  {
      width:'10%',
      justifyContent:'center',
  },
  ListViewInner:{
      width:'100%',
      flexDirection:'row'
  },
  ContentText: {
    color:Colors.ThemeBlue
  }
})
