import React from 'react'
import {
  View,
  TouchableHighlight,
  Image, Text
} from 'react-native'
import styles from './style'
import { Colors, Icons, CommonStyle, Constants,FontAwesomeIcon } from '@common';
import Checkbox  from '../Checkbox';

class WithdrawComponent extends React.Component {
    
  render() {
    let { item, onClick, isChecked, rightText, style, txtStyle, withdrawMethod } = this.props
    return (
      <View style={styles.ListView}>
        <View style={styles.AmountView}>
                <Text style={[styles.AmountText, CommonStyle.fs16]}><Image source={Icons.giftIcon} style={{width:15, height:15}} /></Text>
        </View>
        <View style={styles.ListViewInner}>
            
            <View style={styles.CheckboxView}>
                <Checkbox isChecked={withdrawMethod == "BankTransfer" ? true: false}/>
            </View>
            <View style={styles.ImageView}>
              <Image source={Icons.giftIcon} style={styles.ImageIcon} />
            </View>
            <View style={styles.ListContentView}>
                <Text style={[styles.EarningText, CommonStyle.fs13]}>{item.text}</Text>
                <Text style={[styles.ContentText, CommonStyle.fs11]}>{item.description}</Text>
            </View>
            
        </View>
      </View>
    )
  }
}

export default WithdrawComponent


