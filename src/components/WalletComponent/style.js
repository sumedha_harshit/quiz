import { StyleSheet } from 'react-native'
import { Colors, Constants } from '@common'
export default StyleSheet.create({
    ListView:{
        padding:10,
        borderWidth:1,
        borderColor:Colors.WhiteGray,
        flexDirection:'row',
        width:'100%',
        marginBottom:10
      },
      ImageView:{
        width:'10%',
        // alignItems:'center',
        justifyContent:'center'
      },
      ImageIcon:{
        height:25, width:25
      },
      ListContentView:{
        width:'70%',
        alignItems:'flex-start',
        justifyContent:'center',
      },
      EarningText:{
        color:Colors.BlackOne,
        fontWeight:'bold'
      },
      AmountView:{
        width:'20%',
        //alignItems:'center',
        justifyContent:'center'
      },
      AmountText:{
        textAlign:'right',
        color:Colors.ThemeBlue,
        fontWeight:'bold'
      },
      AmountTextOut:{
        textAlign:'right',
        color:Colors.Pink,
        fontWeight:'bold'
      }

})