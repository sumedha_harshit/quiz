import React from 'react'
import {
  View, Image, Text
} from 'react-native'
import styles from './style'
import { Icons, CommonStyle, Constants,Svgs } from '@common';

class WalletComponent extends React.Component {

  render() {
    let { item } = this.props;
    return (
      <View style={styles.ListView}>
        <View style={styles.ImageView}><Image source={item.paymentStatus == "Earning" ? Icons.WithDrawOut : Icons.WithDrawIn} style={styles.ImageIcon} /></View>
        {/* <View style={styles.ImageView}>{item.paymentStatus == "Earning" ? Svgs.WithdrawIcon : Icons.AddAmountIcon}</View> */}
        <View style={styles.ListContentView}>
          <Text style={[styles.EarningText, CommonStyle.fs13]}>{item.paymentStatus}</Text>
          <Text style={[styles.ContentText, CommonStyle.fs11]}>{item.transactionDateTime}</Text>
        </View>
        <View style={styles.AmountView}>
          <Text style={[item.active == true ? styles.AmountTextOut : styles.AmountText , CommonStyle.fs16]}>{Constants.Symbol.rupee} {item.amount}</Text>
        </View>
      </View>
    )
  }
}

export default WalletComponent

