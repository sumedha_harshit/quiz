import { StyleSheet } from 'react-native';
import { Constants, Colors }  from '@common';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    //height:40,
    backgroundColor:Colors.ErrorBack,
    color:Colors.DarkRed,
    borderRadius:50,
    alignItems:'center',
    paddingLeft:20,
    paddingRight:20,
    paddingTop:5,
    paddingBottom:5,
    marginBottom:20,
    width:'100%'
  },
  text: {
    width:'100%',
    color: Colors.DarkRed,
    fontSize: Constants.FontSize.small,
    marginTop: 5,
    marginLeft: 0,
    marginBottom: 5,
    justifyContent: 'center',
    textAlign:'left'
  },
});

export default styles;
