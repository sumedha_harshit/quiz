import React, { Component } from 'react';
import { Text, View } from 'react-native';
import styles from './styles';

 /*========================================================
     * class Name: validationMessage
     * class Purpose: display error message
     * class Parameters: error message text
     * class ReturnType: message
     * class Description: set few time show error message and auto clear error message
     *=====================================================*/

class validationMessage extends Component {
  componentDidMount() {
    this.startTimeout();
  }

  componentDidUpdate() {
    this.startTimeout();
  }

  componentWillUnmount() {
    const { handleTimeout = () => {} } = this.props;
    clearTimeout(this.hideTimeout);
    handleTimeout();
  }

  startTimeout() {
    const { timeout = 5000, handleTimeout = () => {} } = this.props;
    clearTimeout(this.hideTimeout);
    this.hideTimeout = setTimeout(handleTimeout, timeout);
  }

  render() {
    const { message = '', icon = null } = this.props;
    return (
      <View style={styles.container}>
        {icon}
        <Text style={styles.text}>{message}</Text>
      </View>
    );
  }
}

export default validationMessage;
