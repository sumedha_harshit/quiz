import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';
import styles from './style';
import Constants from '../../app/common/Constants';

export default class CarouselSlider extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };

    get image() {
        const { data: { ImagePath }, parallax, parallaxProps, even } = this.props;

        return parallax ? (
            <ParallaxImage
                source={{ uri: ImagePath }}
                containerStyle={styles.imageContainer}
                style={styles.image}
                parallaxFactor={0.35}
                showSpinner={true}
                spinnerColor={even ? 'rgba(255, 255, 255, 0.4)' : 'rgba(0, 0, 0, 0.25)'}
                {...parallaxProps}
            />
        ) : (
                <Image
                    source={{ uri: ImagePath }}
                    style={styles.image}
                />
            );
    }
    schemeDescription = (SchemeId) => {
        //alert("schemeDescription");
        if (SchemeId > 0 && SchemeId != "") {
            // alert(SchemeId);
            this.props.handleImageClick(SchemeId);
            // this.props.navigation.navigate(Constants.Screen.SchemeDescription, {"Schemeid" : SchemeId});
        }
    }



    render() {

        const { data: { Heading, Data, ImagePath, SchemeId }, even } = this.props;
        const { handleImageClick } = this.props;

        const uppercaseTitle = Heading ? (
            <Text
                style={[styles.title, even ? styles.titleEven : {}]}
                numberOfLines={2}
            >
                {Heading.toUpperCase()}
            </Text>
        ) : false;

        return (
            <TouchableOpacity
                activeOpacity={1}
                style={styles.slideInnerContainer}

                onPress={(event) => this.schemeDescription(SchemeId)}
            >
                <View style={styles.shadow} />

                <View style={styles.imageContainer}>
                    <Image
                        source={{ uri: ImagePath }}
                        style={styles.image}
                    />
                    <View style={styles.overlayTextView}>

                        <View><Text style={styles.titleText}>{Heading}</Text></View>
                        <View><Text style={styles.descriptionText}>{Data}</Text></View>

                    </View>
                </View>
            </TouchableOpacity>
        );
    }
}
