import { StyleSheet, Dimensions, Platform } from 'react-native';
import { Colors, Constants } from '@common';

const IS_IOS = Platform.OS === 'ios';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp(percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

const entryBorderRadius = 8;

export default StyleSheet.create({
    slideInnerContainer: {
        width: '100%',
        height: 240
    },
    shadow: {
        position: 'absolute',
        top: 0,
        bottom: 18,
        shadowColor: Colors.Black,
        shadowOpacity: 0.25,
        shadowOffset: { width: 0, height: 10 },
        shadowRadius: 0,
    },
    imageContainer: {
        flex: 1,
        marginBottom: IS_IOS ? 0 : -1, // Prevent a random Android rendering issue
        backgroundColor: 'white',
        justifyContent: 'flex-start',
        alignItems: 'flex-end',
        height: 180,
    },
    imageContainerEven: {
        backgroundColor: Colors.Black
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
        height: 180,
    },
    radiusMask: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        height: entryBorderRadius,
        backgroundColor: 'white'
    },
    radiusMaskEven: {
        backgroundColor: Colors.Black
    },
    textContainer: {
        justifyContent: 'center',
        paddingTop: 20 - entryBorderRadius,
        paddingBottom: 20,
        paddingHorizontal: 16,
        backgroundColor: 'white',
        borderBottomLeftRadius: entryBorderRadius,
        borderBottomRightRadius: entryBorderRadius
    },
    textContainerEven: {
        backgroundColor: Colors.Black
    },
    title: {
        color: Colors.Black,
        fontSize: 13,
        fontWeight: 'bold',
        letterSpacing: 0.5
    },
    titleEven: {
        color: 'white'
    },
    subtitle: {
        marginTop: 6,
        color: Colors.Gray,
        fontSize: 12,
        fontStyle: 'italic'
    },
    subtitleEven: {
        color: 'rgba(255, 255, 255, 0.7)'
    },
    overlayTextView: {
        position: 'absolute',
        width: '70%',
        left: '15%',
        height: 130,
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        textAlign: 'left',
        padding: 15,
        borderWidth: 1,
        borderColor: Colors.GrayDark,
        bottom: 10,
        backgroundColor: Colors.White,
        borderRadius: 10

    },
    titleText: {
        fontSize: Constants.FontSize.medium,
        color: Colors.Black,
        fontWeight: "500",
        paddingBottom: 2

    },
    descriptionText: {
        fontSize: Constants.FontSize.small,
        color: Colors.Black,
    }
});
