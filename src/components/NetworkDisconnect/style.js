import { StyleSheet } from 'react-native';
import { Utils, Constants } from '@common';

const size = Utils.deviceSize();

export default StyleSheet.create({
    container: {
        flex: 1, 
        justifyContent: 'center', 
        alignItems: 'center',
        backgroundColor: 'white'
    },
    noInternetImage: {
        height: size == 'medium' ? 200 : 300,
        width: size == 'medium' ? 200 : 300,
    },
    noInternetText: {
        textAlign: 'center', 
        marginTop: 20,
        fontSize: size == 'medium' ? Constants.FontSize.medium : Constants.FontSize.big,
    },
})