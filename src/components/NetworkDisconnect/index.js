import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import { Languages, Icons, Svgs, CommonStyle } from '@common';
import styles from './style';

class NetworkDisconnect extends Component {
    render() {
        const { container, noInternetImage, noInternetText } = styles;
        return (
            <View style={CommonStyle.NoInternetView}>
                {Svgs.NoInternetConnection}
                <Text style={[CommonStyle.fs18, CommonStyle.NoDataHeading]}>Mobile data is off </Text>
                <Text style={[CommonStyle.fs14, CommonStyle.NoDataHeading]}>No data connection. Consider truning on mobile data or turning on Wi-Fi.</Text>
            </View>
        );
    }
}

export default NetworkDisconnect;