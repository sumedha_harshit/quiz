import React from 'react';
import { View, StyleSheet, Text, Platform } from 'react-native';
import SpinKit from 'react-native-spinkit';
import { Colors } from '@common';

/*========================================================
     * class Name: Spinner component 
     * class Purpose: display spinner view
     * class ReturnType: spinner view
     * class Description: common component of spinner and display spinner view
     *=====================================================*/

{/* types: 'CircleFlip', 'Bounce', 'Wave', 'WanderingCubes', 'Pulse', 
'ChasingDots', 'ThreeBounce', 'Circle', '9CubeGrid', 'WordPress', 'FadingCircle',
'FadingCircleAlt', 'Arc', 'ArcAlt' */}

export const Spinner = () => {
    return (
        <View style={styles.loading}>
            {
                Platform.OS == 'windows'
                    ?
                    <Text>Loading...</Text>
                    : <SpinKit type="Circle" size={35} color={Colors.ThemeBlue} />
            }
        </View>
    );
};

const styles = StyleSheet.create({
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

