import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logout } from '../redux/actions';
import { TouchableOpacity, Image } from 'react-native';
import { Constants } from '@common';
import PropTypes from 'prop-types';

 /*========================================================
     * class Name: UserLogOutComponent
     * class Purpose: display logout icon and logout.
     * function Parameters: icon, style of touchableopacity, imgStyle of image, navigation
     * class Description: display logout icon and logout method
     *=====================================================*/

class UserLogOutComponent extends Component {

    // calls logout method and navigates to logout screen
    onLogout() {
        this.props.logout();
        this.props.navigation.navigate(Constants.Screen.Logout);
    }

    render() {
        const { icon, style, imgStyle } = this.props;
        return(
            <TouchableOpacity style={style} onPress={() => this.onLogout()}>
                <Image
                    source={icon}
                    style={imgStyle} 
                />
            </TouchableOpacity>
        );
    }
}

UserLogOutComponent.propTypes = {
    logout: PropTypes.func.isRequired
};

const mapDispatchToProps = {
    logout
};

export default connect(
  null,
  mapDispatchToProps,
)(UserLogOutComponent);