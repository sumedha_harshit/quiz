import { StyleSheet, Platform } from 'react-native';
import { Constants, Colors } from '@common';

const styles = StyleSheet.create({
    HeaderTitle:{
        height: 50,
        width: '100%',
        elevation: 0,
        shadowOpacity: 0
    },
    breadcrumbsView:{
        width:'100%',
        marginTop:10,
        borderWidth:1,
        borderColor:'#000'
    },
    BreadcrumbText:{
        width:200,
        color:Colors.White,
        marginLeft:10,
        flexDirection:"row",
        borderWidth:1,
        borderColor:'#000',
    },
 /******** modal style********/
 modalView: {
    flexDirection: 'column',
    backgroundColor: Colors.White,
    
    paddingLeft : 25,
    paddingRight : 25,
    paddingTop : 20,
    paddingBottom : 25,
  },
  modalHeaderViewTimer:{
    width:"100%",
    flexDirection:"row"
  },
  headingModal:{
    width:"91%",
    flexDirection:"row",
    alignItems:'flex-start',
    
  },
  modalCloseViewTimer: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingTop: 7,
    textAlign:'right'
  },
  modalCloseIcon: {
    height: 20,
    width: 20,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  NotificationViewModal:{
    width:'95%',
    alignItems:'flex-start',
    justifyContent:'center',
    marginTop:10,
    // height:50,
    alignSelf:'center',
    flexDirection:'row',
    paddingLeft:10,
    paddingRight:10,
    // borderWidth:1
  },
  ModalSubHeaderText:{
    color:Colors.Black,
    fontSize:22,
    textAlign:'center',
    lineHeight:30
  },
  buttonStyleProfileBrowse:{

  },
  ProfilebuttonTitleStyle:{

  },
  modalContainerTimer:{

  },
  modalButtonView:{
      
  }
});