import React, { Component } from 'react';
import { View, Text, AsyncStorage, TouchableOpacity, Image } from 'react-native';
import { connect } from 'react-redux';
import CommonStyles from '../../app/common/CommonStyle';
import { challengeRequest } from '../../redux/actions/authActions';
import styles from './style';
import { Colors, Constants, Icons, Svgs, Languages } from '@common';
import {CustomButton} from "@components";
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import Modal from "react-native-modal";
import EventEmitter from "react-native-eventemitter";
import _ from "lodash";

class HeaderBackground extends Component {
  
    constructor(props){
       super(props);
       this.startAuthApp();
       this.state= {
           username : '',
           ModalVisibility: false,
           joinModalVisibility: false
       };
       
   }
   
   toggleModal = () => {
        let newRequestLength = this.props.requestedChallengeIds.length;
        AsyncStorage.setItem("prevRequestLength", newRequestLength.toString());
        this.setState({ ModalVisibility: false });
   };

   toggleModal1 = () => {
        let startingSoon = this.props.startingSoonChallengeList.length;
        AsyncStorage.setItem("startingSoonChallenge", startingSoon.toString());
        this.setState({ joinModalVisibility: false });
    };

   startAuthApp = async () => {

        // challenge Request 
        let newRequestLength = this.props.requestedChallengeIds.length;
        
        let prevRequestLength = await AsyncStorage.getItem("prevRequestLength");
        
        if(prevRequestLength == null || prevRequestLength == undefined){
          this.setState({ ModalVisibility: true });
        }
        else if(parseInt(newRequestLength) === parseInt(prevRequestLength)){
          this.setState({ ModalVisibility: false });
        }
        else if(parseInt(newRequestLength) > parseInt(prevRequestLength)){
          this.setState({ ModalVisibility: true });
        }
        else if(parseInt(newRequestLength) <= parseInt(prevRequestLength)){
            await AsyncStorage.setItem("prevRequestLength",newRequestLength.toString());
          }
        else{
          this.setState({ ModalVisibility: false });
        }

        // challenge starting soon
       
        let soonChallenge =_.filter(this.props.startingSoonChallengeList, { CreatedByUserId: global.UserId });
        let soonChallengeLen = soonChallenge.length;
        let startingSoonChallengeList = this.props.startingSoonChallengeList.length;
        
        let prevSoonChallenge = await AsyncStorage.getItem("startingSoonChallenge");
        if(soonChallengeLen == 0){
        if((prevSoonChallenge == null || prevSoonChallenge == undefined ) && soonChallenge.length == 0){
            this.setState({ joinModalVisibility: true });
          }
          else if(parseInt(prevSoonChallenge) === parseInt(startingSoonChallengeList)){
            this.setState({ joinModalVisibility: false });
          }
          else if(parseInt(startingSoonChallengeList) > parseInt(prevSoonChallenge)){
            this.setState({ joinModalVisibility: true });
          }
          else if(parseInt(startingSoonChallengeList) <= parseInt(prevSoonChallenge)){
            AsyncStorage.setItem("startingSoonChallenge", startingSoonChallengeList.toString());
          }
          else{
            this.setState({ joinModalVisibility: false });
          }
        }
        
    //   //get challnege request and starting soon challnege list
    //   this.didBlurSubscription = this.props.navigation.addListener(
    //     'didFocus',
    //     payload => {
    //         this.props.challengeRequest();
    //     }
    // );
   }

   onCheckRequest= (link) =>{
        this.setState({ ModalVisibility: false });
        let newRequestLength = this.props.requestedChallengeIds.length;
        AsyncStorage.setItem("prevRequestLength", newRequestLength.toString());
        EventEmitter.emit(Constants.EventEmitterName.OpenPage, link);
   }

   onJoinChallenge= (link) =>{
    this.setState({ joinModalVisibility: false });
    let startingSoon = this.props.startingSoonChallengeList.length;
    AsyncStorage.setItem("startingSoonChallenge", startingSoon.toString());
    EventEmitter.emit(Constants.EventEmitterName.OpenPage, link);
}

    render() {
        const { title, requestedChallengeIds, startingSoonChallengeList} = this.props;
        let soonChallenge =_.filter(startingSoonChallengeList, { CreatedByUserId: global.UserId });
        return (
           
            <LinearGradient colors={[Colors.ThemePurple, Colors.ThemeBlue]} style={[CommonStyles.HeaderTitle,{height:60,alignItems:'center',justifyContent:'center'}]}>
                    <View style={CommonStyles.headersection}>
                        <Text style={CommonStyles.headerTitle}>{title} </Text>{/*title == "Home" ? this.state.username.length > 20 ? this.state.username.substring(0, 20)+"..." : this.state.username.substring(0, 20) : title.length > 20 ? title.substring(0, 20)+"..." : title.substring(0, 20)*/} 
                        {/* <View style={CommonStyles.borderView}></View> */}
                       
                    </View>
                    {
                    requestedChallengeIds && requestedChallengeIds.length > 0 ?
                    <Modal style={CommonStyles.modalContainerTimer} isVisible={this.state.ModalVisibility}>
                        <View style={CommonStyles.modalView}>
                            <View style={CommonStyles.modalHeaderViewTimer}>
                                <View style={CommonStyles.headingModal}><Text style={CommonStyles.modalHeaderTitleProfile}></Text></View>
                                <View style={CommonStyles.closeIconModal}>
                                    <TouchableOpacity style={CommonStyles.modalCloseViewTimer} onPress={() => this.toggleModal()}>
                                        <Image source={Icons.CloseModalImage} style={CommonStyles.modalCloseIcon} resizeMode={'stretch'} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={CommonStyles.modalSubViewTimer}>
                                <View style={CommonStyles.NotificationViewModal}>
                                    <Text style={CommonStyles.ModalSubHeaderBellIcon}>{Svgs.BellIcon}</Text><Text style={CommonStyles.ModalSubHeaderText}> You have received new challenge request.</Text>
                                </View>
                                <View style={CommonStyles.modalButtonView}>
                                    <CustomButton
                                        buttonStyle={CommonStyles.buttonStyleProfileBrowse}
                                        titleStyle={CommonStyles.ProfilebuttonTitleStyle}
                                        title="Check Request"
                                        onPress={() => { this.onCheckRequest(Constants.Screen.RequestedChallenge) }}
                                    />
                                    <CustomButton
                                        buttonStyle={CommonStyles.buttonStyleProfileBrowse}
                                        titleStyle={CommonStyles.ProfilebuttonTitleStyle}
                                        title="Close"
                                        onPress={() => { this.toggleModal() }}
                                    />
                                </View>
                            </View>
                        </View>
                    </Modal>
                    :
                    <View />
                    }
                    {
                    startingSoonChallengeList && startingSoonChallengeList.length > 0 && soonChallenge.length == 0 && this.state.ModalVisibility == false?
                    <Modal style={CommonStyles.modalContainerTimer} isVisible={this.state.joinModalVisibility}>
                        <View style={CommonStyles.modalView}>
                            <View style={CommonStyles.modalHeaderViewTimer}>
                                <View style={CommonStyles.headingModal}><Text style={CommonStyles.modalHeaderTitleProfile}></Text></View>
                                <View style={CommonStyles.closeIconModal}>
                                    <TouchableOpacity style={CommonStyles.modalCloseViewTimer} onPress={() => this.toggleModal1()}>
                                        <Image source={Icons.CloseModalImage} style={CommonStyles.modalCloseIcon} resizeMode={'stretch'} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <View style={CommonStyles.modalSubViewTimer}>
                                <View style={CommonStyles.NotificationViewModal}>
                                    <Text style={CommonStyles.ModalSubHeaderBellIcon}>{Svgs.BellIcon}</Text><Text style={CommonStyles.ModalSubHeaderText}>{startingSoonChallengeList.length} Challenge(s) going to start within 2 minutes.</Text>
                                </View>
                                <View style={CommonStyles.modalButtonView}>
                                    <CustomButton
                                        buttonStyle={CommonStyles.buttonStyleProfileBrowse}
                                        titleStyle={CommonStyles.ProfilebuttonTitleStyle}
                                        title="Join Challenge"
                                        onPress={() => { this.onJoinChallenge(Constants.Screen.StartSoonChallenge) }}
                                    />
                                    <CustomButton
                                        buttonStyle={CommonStyles.buttonStyleProfileBrowse}
                                        titleStyle={CommonStyles.ProfilebuttonTitleStyle}
                                        title="Close"
                                        onPress={() => { this.toggleModal1() }}
                                    />
                                </View>
                            </View>
                        </View>
                    </Modal>
                     :
                    <View />
                    }
            </LinearGradient>
        );
    }
}

HeaderBackground.propTypes = {
    challengeRequest: PropTypes.func.isRequired,
    startingSoonChallengeList: PropTypes.array.isRequired
};

function mapStateToProps({ auth }) {
    return {
        currentUser: auth.currentUser,
        requestedChallengeIds: auth.requestedChallengeIds,
        startingSoonChallengeList: auth.startingSoonChallengeList,

    }
}

const mapDispatchToProps = {
    challengeRequest,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HeaderBackground);