import React, { Component } from 'react';
import { TouchableOpacity, Image, Text, View } from 'react-native';
import { Constants, Icons, Svgs, Colors, Global } from '@common';
import CommonStyles from '../../app/common/CommonStyle';
import { HeaderBackButton } from 'react-navigation-stack';
import EventEmitter from "react-native-eventemitter";
/*========================================================
    * class Name: HeaderLeft
    * class Purpose: display Open Sidebar and Back Button.
    * function Parameters: icon, style of touchableopacity, imgStyle of image, navigation
    * class Description: display logout icon and logout method
    *=====================================================*/

class HeaderLeft extends Component {

    //calls toggle drawer method
    openDrawer() {
        EventEmitter.emit(Constants.EventEmitterName.OpenDrawer);
        //Global.EventEmitter.emit(Constants.EventEmitterName.OpenDrawer);
    }

    render() {
        const { sidemenu, fromhome, hideback } = this.props;
        if (sidemenu) {
            return (
                <View style={[CommonStyles.heightStyle, { marginTop:15,  } ]}>
                   {
                    fromhome ?
                    <TouchableOpacity onPress={() => this.openDrawer()} style={{ paddingTop:10, paddingBottom:10, paddingRight:10}}>
                        <View style={ fromhome ? CommonStyles.headerLeftIconStyleBack : CommonStyles.headerLeftIconStyle}>
                            {Svgs.MenuIcon}
                        </View>
                    </TouchableOpacity>
                    :
                    <View />
                    }
                    {
                        !fromhome  ?
                            <HeaderBackButton
                                onPress={() => this.props.navigation.goBack(null)}
                                tintColor={Colors.White}
                            />
                            : <View />
                    }
                </View >
            );
        }
        else if(hideback){
            return (
                <View />
            );
        }
        else {
            return (
                <View style={CommonStyles.heightStyle}>
                    <HeaderBackButton
                        onPress={() => this.props.navigation.goBack(null)}
                        tintColor={Colors.White}
                    />
                </View>
            );
        }

    }
}

export default HeaderLeft;