import { StyleSheet } from 'react-native';
import { Constants, Colors } from '@common';

export default StyleSheet.create({
  
   imageOfBrand : {
      width : 42,
      height : 42,
      
   },
   imageOfBrandRadius : {
      borderRadius:21
   },
   SelectCarimageOfBrand : {
      width : "70%",
      height : "70%",
     
   },
   SelectCarimageOfBrandRadius : {
      borderRadius:55
   },
})