import React from 'react';
import { Image } from 'react-native';
import styles from './style'
export default class HandleImages extends React.Component {
  static defaultProps = {
    source: [],
    onError: () => {},
  }

  state = { current: 0 }

  onError = error => {
    this.props.onError(error);
    const next = this.state.current + 1;
    if (next < this.props.source.length) {
      this.setState({ current: next });
    }
  }

  render() {
    const { onError, source,imagestyle,imagestyleradius, ...rest } = this.props;
    return (
      <Image
        source={source[this.state.current]}
        onError={this.onError}
        style={[imagestyle, this.state.current==1? imagestyleradius : {} ]}
        {...rest}
        
      />
    );
  }
}