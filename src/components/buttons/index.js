import React, { Component } from 'react';
import { TouchableOpacity, Text } from 'react-native';

/*========================================================
     * class Name: TouchableOpacity
     * class Purpose: dynamic touchableOpacity button
     * class Parameters: onPress, title, titleStyle, buttonStyle
     * class ReturnType: button
     * class Description: common component touchableOpacity button 
     *=====================================================*/

class Button extends Component {
    render() {
        const { onPress, title, titleStyle, buttonStyle, btndisable } = this.props;
        return (
            <TouchableOpacity style={buttonStyle} onPress={onPress} disabled={btndisable}>
                <Text style={titleStyle}>{title}</Text>
            </TouchableOpacity>
        )
    }
}

export default Button;