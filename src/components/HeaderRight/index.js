import React, { Component } from 'react';
import { connect } from 'react-redux';
// import { logout } from '../../redux/actions/authActions';
import { ChallengeStartingSoon, challengeRequest } from '../../redux/actions/authActions';
import { TouchableOpacity, Image, Text, View, AsyncStorage } from 'react-native';
import { Constants, Icons, Svgs, Global, Utils } from '@common';
import PropTypes from 'prop-types';
import CommonStyles from '../../app/common/CommonStyle';
import _ from "lodash";

/*========================================================
    * class Name: HeaderRight
    * class Purpose: display logout icon and logout.
    * function Parameters: icon, style of touchableopacity, imgStyle of image, navigation
    * class Description: display logout icon and logout method
    *=====================================================*/

class HeaderRight extends Component {

    constructor(props) {
        super(props);
        this.onload();
        this.state = {
            count: 0,
        }  
    }

    onload = () => {
        //get challnege request and starting soon challnege list
        this.didBlurSubscription = this.props.navigation.addListener(
            'didBlur',
            payload => {
                this.props.challengeRequest();
            }
        );
    }

    onHome() {
        this.props.navigation.navigate(Constants.Screen.GameZone);
    }

    render() {
        const {orderPage, page, showhome, showNotification, startingSoonChallengeList} = this.props;
        // let soonChallenge =_.filter(startingSoonChallengeList, { CreatedByUserId: global.UserId });
        return (
            showhome?
            <View style={[CommonStyles.heightStyle]}>
                <TouchableOpacity onPress={() => this.onHome()}>
                    <View style={CommonStyles.headerRightIconStyle}>
                        {/* <Image source={Icons.homeIcon} style={{height:20, width:20, marginTop:7}}/> */}
                        {Svgs.HomeIcon}
                    </View>
                </TouchableOpacity>
            </View>
            :
            // showNotification ?
            // startingSoonChallengeList && startingSoonChallengeList.length > 0 && soonChallenge.length == 0 ?
            // <View style={[CommonStyles.heightBellStyle]}>
            //     <TouchableOpacity onPress={() => this.props.navigation.navigate(Constants.Screen.StartSoonChallenge)}>
            //         <View style={CommonStyles.headerRightBellIconStyle}>
            //             {Svgs.BellIcon1}
            //         </View>
            //         <View style={CommonStyles.NotificationCountView}>
            //             <Text style={CommonStyles.NotificationCount}>{startingSoonChallengeList.length}</Text>
            //         </View>
            //     </TouchableOpacity>
            //     <Text style={CommonStyles.NotificationText}>Challenge Starting Soon</Text>

            // </View>
            //  :
            <View />
        );
    }
}

HeaderRight.propTypes = {
    // logout: PropTypes.func.isRequired,
    // ChallengeStartingSoon: PropTypes.func.isRequired,
    // startingSoonChallengeList: PropTypes.array.isRequired
    challengeRequest: PropTypes.func.isRequired
};

const mapDispatchToProps = {
    // logout,
    // ChallengeStartingSoon
    challengeRequest
};

const mapStateToProps = state => ({
    // startingSoonChallengeList: state.auth.startingSoonChallengeList,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(HeaderRight);