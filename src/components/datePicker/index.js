import React, { Component } from 'react';
import { View, TouchableOpacity, Text, TextInput, Image } from 'react-native'
import styles from './style'
import { Colors } from "@common";
import DateTimePicker from 'react-native-modal-datetime-picker';

class DatePickers extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isDateTimePickerVisible: false,
            
        }
    }

    //Used to show startdate datetime picker
    showDateTimePicker() {
        if (this.props.disabled == undefined || this.props.disabled == null || this.props.disabled == false) {
            this.setState({ isDateTimePickerVisible: true });
        }
    }

    //Used to hide startdate datetime picker
    hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false });
    }

    //Called when startdate date picked
    handleDateTimePicked = (date) => {
        this.hideDateTimePicker();
        this.props.confirmPress(date);
    };

    render() {
        const { label, placeholder, placeholderTextColor, selectionColor, selectDate, mode, dateIcon, iconStyle, viewContainer, labelTextStyle,
            inputStyles, selectDateOrignal } = this.props;
        const { mainContainer } = styles;
        const { isDateTimePickerVisible } = this.state;
        return (
            <View style={mainContainer}>
                <Text style={labelTextStyle}>{label}</Text>
                <TouchableOpacity activeOpacity={0.8} onPress={() => this.showDateTimePicker()}>
                    <View style={viewContainer} pointerEvents='none'>
                        <TextInput
                            style={inputStyles}
                            autoCorrect={false}
                            blurOnSubmit={false}
                            underlineColorAndroid="transparent"
                            placeholder={placeholder}
                            placeholderTextColor={placeholderTextColor ? placeholderTextColor : Colors.Black}
                            selectionColor={selectionColor ? selectionColor : Colors.Black}
                            editable={false}
                            selectTextOnFocus={false}
                            value={selectDate}
                        />
                        <DateTimePicker
                            mode={mode}
                            isVisible={isDateTimePickerVisible}
                            onConfirm={this.handleDateTimePicked}
                            onCancel={this.hideDateTimePicker}
                            date={selectDateOrignal ? new Date(selectDateOrignal) : new Date()}
                        />
                        <Image style={iconStyle} source={dateIcon} />
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

export default DatePickers;