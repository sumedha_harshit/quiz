import React, { PureComponent } from 'react';
import { TextInput, Image, Text, View, TouchableOpacity, FlatList } from 'react-native';
import styles from './style';
import Modal from "react-native-modal";
import { Colors, Icons, Svgs } from '@common';

/*========================================================
    * class Name: pickerModalWithValue
    * class Purpose: display selection picker modal
    * class Description: design picker modal  
    *=====================================================*/

class pickerModalWithValue extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      pickerShow: false
    };
  }

  toggleModal() {
    this.setState({ pickerShow: !this.state.pickerShow });
  }

  onBackButton() {
    this.setState({ pickerShow: !this.state.pickerShow, selectValue: '' });
  }

  selectItems = (item) => {
    if (item != "" && item != null) {
      this.setState({ pickerShow: !this.state.pickerShow });
      this.props.handlePress(item);
    }
  }

  getSelectText(value) {
    if (value != "" && value != null) {
      var d = this.props.items.filter(a => a.value == value);
      return d != undefined && d.length > 0 ? (String(d[0].label).length > 12 ? String(d[0].label).substr(0, 11) + ".." : d[0].label) : value;
    }
  }

  renderItems = (item) => {
    const { itemContainer, itemName, itemNameSelected, filterSelectionView, filterSelectionImage } = styles;
    return (
      <TouchableOpacity style={itemContainer} onPress={() => this.selectItems(item)}>
        {
          this.props.selectText == item.value
            ?
            <View style={filterSelectionView}>
              <Text style={itemNameSelected}>{item.label}</Text>
              <Image source={Icons.SelectTrue} style={filterSelectionImage} />
            </View>
            :
            <Text style={itemName}>{item.label}</Text>
        }
      </TouchableOpacity>
    );
  }

  render() {
    const { label, placeholder, placeholderTextColor, selectionColor, selectText, items, viewContainer, style, labelTextStyle, iconColor } = this.props;
    const { mainContainer, itemName, inputRightDownIcon, modalContainer, itemContainer, headerView, defaultPlaceholderView, closeIconView, closeIcon } = styles;
    return (
      <View style={mainContainer}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => this.toggleModal()}
        >
          <View style={viewContainer}>
            <TextInput
              style={style}
              autoCorrect={false}
              blurOnSubmit={false}
              underlineColorAndroid="transparent"
              placeholder={placeholder}
              placeholderTextColor={placeholderTextColor ? placeholderTextColor : Colors.PlaceholderLightGray}
              selectionColor={selectionColor ? selectionColor : Colors.White}
              editable={false}
              selectTextOnFocus={false}
              value={this.getSelectText(selectText.toString())}
            />
            {Svgs.DownArrowWhite}
          </View>
        </TouchableOpacity>

        <Modal isVisible={this.state.pickerShow} onBackButtonPress={() => this.onBackButton()}>
          <View style={modalContainer}>
            <View style={headerView}>
              <TouchableOpacity style={[itemContainer, { flex: 1 }]} onPress={() => this.selectItems(null)}>
                <Text style={defaultPlaceholderView}>{placeholder}..</Text>
              </TouchableOpacity>
              <TouchableOpacity style={closeIconView} onPress={() => this.onBackButton()}>
                <Image source={Icons.closeIcon} style={closeIcon} />
              </TouchableOpacity>
            </View>
            <FlatList
              data={items}
              showsVerticalScrollIndicator={false}
              extraData={this.state}
              keyExtractor={(index, i) => i.toString()}
              renderItem={({ item }) => this.renderItems(item)}
            />
          </View>
        </Modal>

      </View>
    );
  }
}

export default pickerModalWithValue;