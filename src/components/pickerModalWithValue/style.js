import { StyleSheet } from 'react-native'
import { Constants, Colors } from '@common';
// import { ifIphoneX } from 'react-native-iphone-x-helper';

export default StyleSheet.create({
  labelText: {
    color: Colors.Black,
    fontSize: Constants.FontSize.small
  },
  mainContainer: {
    flexDirection: 'column',
    paddingTop: 0,
  },
  filterSelectionView: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  filterSelectionImage: {
    height: 22,
    width: 22,
    justifyContent: 'flex-end'
  },
  inputRightDownIcon: {
    borderLeftWidth: 0,
    borderLeftColor: 'transparent',
    borderTopWidth: 3,
    borderTopColor: Colors.SkyBlue,
    borderRightWidth: 3,
    borderRightColor: Colors.SkyBlue,
    width: 9,
    height: 9,
    right: 15,
    transform: [{ rotate: '135deg' }]
  },
  defaultPlaceholderView: {
    fontSize: Constants.FontSize.medium,
    color: Colors.PlaceholderLightGray
  },
  modalContainer: {
    backgroundColor: Colors.White,
    borderRadius: 5,
    // ...ifIphoneX(
    //   {
    //     marginTop: 50,
    //   },
    //   {
    //     marginTop: 20,
    //   },
    // ),
    marginBottom: 20
  },
  headerView: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  closeIconView: {
    padding: 10
  },
  closeIcon: {
    height: 25,
    width: 25
  },
  itemContainer: {
    paddingLeft: 15,
    paddingRight: 15,
    paddingTop: 12,
    paddingBottom: 12
  },
  itemName: {
    fontSize: 15,
    flex: 1,
    color: Colors.Black
  },
  itemNameSelected: {
    fontSize: 15,
    flex: 1,
    color: Colors.Black,
    fontWeight : "bold"
  }
});