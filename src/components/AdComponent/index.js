import React from 'react';
import {View, Text} from 'react-native';
import {
    AdIconView,
    MediaView,
    AdChoicesView,
    TriggerableView,
    withNativeAd
  } from 'react-native-fbads';
  class AdComponent extends React.Component {
    render() {
      console.log("hello");
      console.log(JSON.stringify(this.props.nativeAd ? this.props.nativeAd :''));
      return (
        <View>
          <AdChoicesView style={{ position: 'absolute', left: 0, top: 0 }} />
          <AdIconView style={{ width: 50, height: 50 }} />
          <MediaView style={{ width: 160, height: 90 }} />
          <TriggerableView>
            <Text>{this.props.nativeAd ? this.props.nativeAd.description :''}</Text>
          </TriggerableView>
        </View>
      );
    }
  }
  
  export default withNativeAd(AdComponent);