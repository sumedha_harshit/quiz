import React, { Component } from 'react';
import { Text, View, Image, TouchableOpacity, Alert } from 'react-native';
import { connect } from 'react-redux';
// import { logout } from '../../redux/actions/authActions';
import CommonStyle from '../../app/common/CommonStyle'
import { Constants, Colors, Icons, Svgs, Languages, Global } from '@common';
import PropTypes from 'prop-types';
import LinearGradient from 'react-native-linear-gradient';

import EventEmitter from "react-native-eventemitter";
import styles from '../../app/common/CommonStyle';

class LeftMenu extends Component {
    constructor(props) {
        super(props);
        this.state={
            ModalVisibility: false,
        }
    }

    async componentWillMount(){
        
    }

    toggleModal = () => {
        
        EventEmitter.emit(Constants.EventEmitterName.CloseDrawer);
        
        this.setState({ ModalVisibility: !this.state.ModalVisibility });
    };

    render() {
        const { onOpenPage, currentUser, requestedChallengeIds } = this.props;
        return (
            <LinearGradient colors={[Colors.ThemeBlue1, Colors.ThemePurple1]} style={CommonStyle.menuGradientView}>
                <View style={CommonStyle.menuMainView}>
                    <View style={CommonStyle.userDetails}>
                        <Text style={CommonStyle.userName}>{currentUser.UserName}</Text>
                        <Text style={CommonStyle.userMobilenumber}>{currentUser.MobileNumber}</Text>
                    </View>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.GameZone)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>{Languages.en.Home}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.News)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>News</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.Challenge)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>Challenge</Text> 
                                {
                                requestedChallengeIds && requestedChallengeIds.length > 0 ?
                                <Text style={CommonStyle.NotificatinCount}>{requestedChallengeIds.length}</Text>
                                 :
                                <View />
                                } 
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.MyWallet)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>{Languages.en.MyWallet}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.Withdraw)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>{Languages.en.Withdraw}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.Network)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>{Languages.en.Network}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.ReferFriend)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>{Languages.en.ReferFriendHeader}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.PrivacyPolicy)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>Privacy policy</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.MySetting)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>My Setting</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.ContactSupport)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>Contact Support</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    
                    {/* <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.UsersChallenge)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>Users Challenge</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onOpenPage(Constants.Screen.ChallengeWinner)}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>Challenge Winner</Text>
                            </View>
                        </View>
                    </TouchableOpacity> */}

                    <TouchableOpacity onPress={() => onOpenPage("logout")}>
                        <View style={CommonStyle.menuView}>
                            <View style={CommonStyle.menuTextView}>
                                <Text style={CommonStyle.menuText}>{Languages.en.Logout}</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            </LinearGradient>

        );
    }
}

LeftMenu.propTypes = {
    // logout: PropTypes.func.isRequired,
};

function mapStateToProps({ auth }) {
    return {
        currentUser: auth.currentUser,
        requestedChallengeIds: auth.requestedChallengeIds
    }
}

const mapDispatchToProps = {
//    logout,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(LeftMenu);