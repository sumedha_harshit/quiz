import React, { Component } from 'react';
import {
  View,
  Modal,
  StyleSheet,
  Text, Platform
} from 'react-native';
import SpinKit from 'react-native-spinkit';
import { Colors } from '@common';

/*========================================================
     * class Name: Loader component 
     * class Purpose: dynamic loader component using spinner
     * class Parameters: animationType, modalVisible
     * class ReturnType: spinner dialog
     * class Description: common component Loader and show spinner dialog
     *=====================================================*/

export default class Loader extends Component {
  render() {
    const { animationType, modalVisible } = this.props;
    return (
      <Modal
        animationType={animationType}
        transparent
        visible={modalVisible}
        onRequestClose={() => { }}
      >
        <View style={styles.wrapper}>
          <View style={styles.loaderContainer}>
            {
              Platform.OS == 'windows'
                ?
                <Text>Loading...</Text>
                : <SpinKit type="ThreeBounce" size={50} color={Colors.White} />
            }
          </View>
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    zIndex: 9,
    backgroundColor: Colors.TransparentBlack,
    position: 'absolute',
    width: '100%',
    height: '100%',
    top: 0,
    left: 0,
  },
  loaderContainer: {
    width: 90,
    height: 90,
    backgroundColor: Colors.ThemeBlue,
    borderRadius: 15,
    position: 'absolute',
    left: '50%',
    top: '50%',
    marginLeft: -45,
    marginTop: -45,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor : Colors.ThemeBlue,
    borderWidth : 1
  },
  loaderImage: {
    width: 70,
    height: 70,
    borderRadius: 15,
    position: 'relative',
    left: '50%',
    marginLeft: -35,
    top: '50%',
    marginTop: -35,
  },
});
