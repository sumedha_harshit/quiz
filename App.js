/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Colors, Constants} from './src/app/common';
import { LeftMenu } from './src/components';
// import { NetInfo  } from '@react-native-community/netinfo';
import AppNavigator from './src/AppNavigator';
import { Provider } from 'react-redux';
import store from './src/store';
import Drawer from 'react-native-drawer';
import EventEmitter from "react-native-eventemitter";
import NetworkDisconnect from './src/components/NetworkDisconnect';
import NetInfo from "@react-native-community/netinfo";
// import firebase from 'react-native-firebase';

import {
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded,
} from 'react-native-admob';

class App extends Component {

  constructor() {
    super();
    this.state = {
      show: false,
      internetConnection: true,

    };
  }

    async componentDidMount() {
    NetInfo.addEventListener(state => {
      this.setState({internetConnection: state.isConnected})
      // alert(state.isConnected.toString());
    });
    this.dawerOpenListener = EventEmitter.on(Constants.EventEmitterName.OpenDrawer, this.openDawer)
    this.dawerCloseListener = EventEmitter.on(Constants.EventEmitterName.CloseDrawer, this.onClose)
    // let url = await firebase.links().getInitialLink();
    // alert('incoming url', url);
  }

  
  componentWillUnmount() {
    // EventEmitter.removeListener(Constants.EventEmitterName.OpenDrawer);
    // EventEmitter.removeListener(Constants.EventEmitterName.CloseDrawer);
  }

  openDawer = () => {
    this.setState({ show: true })
  }

  onClose = () => {
    this.setState({ show: false })
  }

  onOpenPage = (link) => {
    this.setState({ show: false }, () => {
      EventEmitter.emit(Constants.EventEmitterName.OpenPage, link);
    })
  }
  
  render() {
    console.disableYellowBox = true;
    return (
      <Provider store={store}>
        {
          this.state.internetConnection == true
            ?
            <Drawer
                  open={this.state.show}
                  type="static"
                  content={<LeftMenu onOpenPage={this.onOpenPage} />}
                  tapToClose={true}
                  onClose={this.onClose}
                  openDrawerOffset={0.2}
                  panCloseMask={0.2}
                  closedDrawerOffset={-3}
                  styles={drawerStyles}
                  tweenHandler={(ratio) => ({
                    main: { opacity: (2 - ratio) / 2 }
                  })}
                >
                  <AppNavigator />
            </Drawer>
         :
         <NetworkDisconnect />
      }
      </Provider>
    );
  }
}

const drawerStyles = {
  drawer: { shadowColor: '#000', shadowOpacity: 0.8, shadowRadius: 3, backgroundColor: Colors.ThemeBlue },
  main: { paddingLeft: 3 }
}
export default App;
